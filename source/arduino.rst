.. _arduino:

The A-Z of Programming Languages: Arduino's Tom Igoe
====================================================
Computerworld's series on the most popular programming languages continues as we chat to Arduino's Tom Igoe

* Trevor Clarke(Computerworld)
* 12 October, 2009 12:14

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_20704.jpg

In the past we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8]and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`falcon <falcon>` [13]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [14], Walter Bright :ref:`d <d>` [15], Brendan Eich on :ref:`javascript <javascript>` [16], Anders Hejlsberg on :ref:`c# <c#>` [17], Guido van Rossum on :ref:`python <python>` [18], Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [19], John Ousterhout on :ref:`tcl <tcl>` [20], Joe Armstrong on Erlang and Rich Hickey on Clojure. We recently spoke to Martin Odersky about the darling of Web 2.0 start-ups and big corporates alike, Scala.

More recently, we heard from `Groovy <http://groovy.codehaus.org/>`_ [21]Project Manager, Guillaume Laforge. He told us the development story behind the language and why he thinks it is grooving its way into enterprises around the world.

And we :ref:`awk_ <awk_>` [22], who helped popularise C with his book, co-written with the creator Dennis Ritchie, *The C Programming Language*and contributed to the development of :ref:`awk <awk>` [4]and AMPL.

 *Want to see a programming icon interviewed?Email Computerworldor follow `@computerworldau <http://www.twitter.com/computerworldau>`_ [23]on Twitter and let us know.*----

 **What prompted the development of Arduino?**

 **Tom Igoe:**There were a handful of schools teaching microcontrollers to non-technologists using a method we called physical computing. We all needed tools to teach that were simpler than the engineering tools that were out there. The Basic Stamp, and later the BX-24 from NetMedia, were okay but they really didn't match up to the tools we were using to teach programming (Hypercard, Director, and later Processing). Then at Ivrea in 2002, they started to do something about it. They developed Programa2003, then Wiring, then Arduino.

 **The Arduino developer team comprised Massimo Banzi, David Cuartielles, Gianluca Martino, David Mellis, Nicholas Zambetti - who were the pioneers - and yourself. Who played what roles?**

Massimo developed the Programa2003 environment for PIC. It was a simple PIC programming tool on the Mac (most of the Ivrea students were Mac users). It made it easier to teach his class. That, combined with the Processing IDE served as an example for Hernando Barragán to develop the Wiring board and environment. Shortly thereafter, Massimo (faculty at Ivrea), David Cuatielles (researcher at Ivrea), and Gianluca Martino (local engineer, hired to develop hardware for students' projects) developed a smaller, less expensive board, the Arduino board. Working togetther with Mellis and Zambetti (students at Ivrea at the time), they improved on the Wiring model and came up with a board and an IDE that could be used by the outside world. I joined them in 2005, helping to beta test it with another school (ITP has a large student body relative to Ivrea, so we could give it a bigger test), and later, helping to develop documentation. I also introduced the team to some of the early US distributors so we could build a market here as well as in Europe.

Nowadays, Gianluca and Massimo do the bulk of the hardware design, Dave Mellis coordinates or writes most of the software, David Cuartielles works on software as well as testing on Linux and maintains the website, and I work on documentation as well as testing, to a lesser degree. We all work together on the direction of the project, manufacturer relations and new development. Gianluca manages all the distributors and his company, Smart Projects, is the main hardware manufacturer. Zambetti has left the core team, but is still an occasional contributor when his professional life allows.

 **Were you trying to solve a particular problem?**

We wanted a tool to teach physical computing, specifically microcontroller programming, to artists and designers, who we teach. The assumptions of those coming at it from a background other than computer science (CS) or electrical engineering (EE) are quite different, and we wanted tools that matched those assumptions.

-----

 **Where does the name Arduino come from?**

Arduino was the first king of the region in which Ivrea is situated. It was also the name of a local bar where students and faculty of Ivrea would congregate.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of Arduino?**

The biggest challenge hasn't really been a technical one so much as a cultural one. Most CS/EE people I've met have an assumption about how you learn about microcontrollers: first you learn Ohm's law and Thevenin's Law, etc. Then you learn about transistor circuits and op amps, then discrete integrated circuits (ICs). Somewhere in there you learn to use an oscilloscope, and a multimeter if you have to, but the scope's better. Then you're introduced to microcontrollers, starting with the internal structure and memory registers. Then you learn the assembly language, and by then, 'of course' you know C and the command line environment, so you're ready for, say, CCS C (on the PIC) or AVR Studio. And 90 per cent of this is done on Windows, because 90 per cent of the world runs Windows, so it makes sense to develop there.

A large number of people coming to code and microcontrollers nowadays don't come from that background. They grew up assuming that the computer's GUI was its primary interface. They assume you can learn by copying and modifying code, because that's what the browser affords with 'view source'. Most of them don't actually want to be programmers, they just want to use programming and circuits to get things done. That may mean making an art piece, or an automatic cat feeder, or a new occupational therapy device. These people are not formally trained engineers, but they want to build things. These are the students we teach. It's their way of thinking for which we designed Arduino.


 **Would you have done anything differently in the development of Arduino if you had the chance?**

I think the biggest change we might have made would have been to standardize the spacing between pins 7 and 8! We've gotten a lot of grief for that mistake, but we've maintained the non-standard spacing to maintain backwards compatibility of the boards.

Mostly, though, I don't think there is an answer to "what would you do differently", because when we encounter something we'd do differently, we make a change. The changes are slower now that we have a larger user base to support, but they are still possible.

 **Why was 'Wiring' and 'Processing' (http://processing.org/) chosen as a basis for Arduino's programming language and environment?**

Because they were the tools in use at Ivrea (and ITP) at the time, and because they worked better for teaching to our students than the  
alternatives that were available at the time. Processing in particular had made a big change for art and design schools teaching programming,  
because the students "got it". It made sense to make a hardware development environment based on that.

Specifically, because *Processing* was in use at Ivrea and ITP at the time. Programa2003, Wiring, and Arduino all grew from Processing's roots in quick succession.

-----

 **How does the Arduino compare to BASIC Stamp, PICs, et. al.? What makes it a better choice?**

There are a couple of things we've tried to improve upon.

* The Arduino language is a set of methods in C/C++ that makes it easier to understand for beginners. Unlike the Stamp's PBASIC, it has all the powerful functions of C (parameter passing, local variables, and so forth) wrapped in a readable syntax. PBASIC was readable and easy for beginners, but it was so limited that even beginners quickly hit limits to what they could do.

* The user experience of Arduino is more like consumer-grade user experience. There's no need to learn about a hardware programmer (unlike the PIC environments), it plugs into the USB (unlike the Stamp). Compiling and uploading new code to your controller is one click. The method names are verbose, and closer in spirit to everyday language than C, assembly, or lower level languages. Ideally, the whole user experience is designed to minimize the time from idea to working device, while maintaining as much of the power and flexibility of the underlying components as possible.

* Arduino embodies what I call "glass box encapsulation".  That means that you don't have to look at the lower level code that comprises the libraries if you don't want to, but you can if you choose. The libraries are compiled only when you compile your final sketch. So if you want to modify them, you can. If you want to include non-Arduino-style C in your code, you can. If you want to include raw assembler code, you can. The encapsulation box is still there, but you can see through it if you choose. The higher level controllers like the Stamp don't include that. And the lower level environments don't abstract to the same level as we do.

* The board incorporates a serial bootloader on the controller, and a USB-to-serial chip, so you don't have to think about the supporting computer-to-controller circuit.  It's also got an on-board power jack and a regulator circuit that switches automatically from the USB to external power, again to simplify the shift from connected to the computer to standalone.

* The price tag for the board is reasonable (cheaper than a Stamp board) and the software's free. We want people to think about computing, rather than see their controller as one unit that they can't afford to duplicate.

* The whole thing is open source, so you can make your own version of it if you've a mind to. The number of clones tells us that this is useful to some of our users, and the continued sales of the official board tells us there's also value in the convenience for others.

* From the beginning, the software has been cross-platform. Teaching in schools where the students are 90 per cent mac users, it's a huge improvement for us. At ITP, we were able to free up a whole lab because we no longer needed to support the PCs that supported the Windows-only, proprietary software we were using for the PIC. Students like being able to use tools on whatever operating system they're familiar with.

 **Why did you decide to open source the hardware designs for the Arduino? What impact do you think this decision has had?**

We believe that openness is beneficial to innovation. The open source nature of it has had a huge impact on its spread, I think. There are tons of clones out there. Many of them aren't even looking for a customer base beyond their friends, students, etc. But there is great learning value in making your own version of a tool you use. I think a lot of people make a clone simply because they can, and they think it'll be fun. In the process, they learn something, and they get hooked on learning more. That wouldn't happen if the platform were closed.

-----

 **We have heard the developers have expressed a desire that the name "Arduino" (or derivatives thereof) be exclusive to the official product and not be used for derivative works without permission - is this correct and if so, why take this measure?**

This is true, we registered the trademark.  It's pretty common to do that in open source. If you look at Linux, MySQL, or Apache, or Ubuntu, for example, they're all trademarked, even though they are open source.  So those were our models.  There are a couple reasons why we chose to do this.

First off, names carry responsibility. While we're happy with people using the design files or the code we've generated, we feel that naming is something that should remain unique. When a person buys an Arduino board, she should be able to count on the manufacturer standing behind it. We do, for those manufacturers to whom we've licensed the name, because we work closely with them to ensure a standard of quality and ease of use that we are proud of.  If, on the other hand, someone buys a board called Arduino from a manufacturer with whom we have no contact, and then sends it to us or one of our manufacturers for repair or replacement, we (or they) can't be expected to service it. We can't guarantee the level of quality with someone we haven't been working with.

Second, product names work a lot like personal names: If I wrote an article and quoted Trevor Clarke, you'd probably be fine with it, but if I wrote it *as* Trevor Clarke, you probably wouldn't. You'd have no way of ensuring that the article was factually correct, or represented your views.  But there's your name on it. We feel the same way about boards and software. If you want to use the Arduino designs or source code to make your own board (basically, quoting the project) that's great. If you want to call it "Arduino-compatible" (citing the quote) that's fine too.  But we'd prefer you give your own board its own name.

Finally, there's a practical level why we retain the trademark. The hardware end of the business is commercially self-sustaining, but the software doesn't pay for itself.  We charge a license fee to the licensed manufacturers for each board they sell. That money goes to pay for maintenance and development of the software and the website. It allows each of us to take a couple hours a week off from our other jobs to maintain the parts of the Arduino system that don't pay for themselves.

You can make derivatives works without permission, it's just the name that is trademarked. Most of the clones did not seek our permission, nor do they need it, as long as they're not called "Arduino."  There are tons of \*duinos out there that are just fine, except for the fact that they bastardize the Italian language. But then again, so does Starbucks.

Patience. Persistence. And frequent showers.  I get my best problem solving done in the shower.(Tom Igoe's advice for up and coming hardware hackers)

 **What projects have you used Arduino for yourself?**

I use it all the time. The first use I ever made of it was with the rest of the team, developing prototypes for a lighting company in Italy. That project made me see how useful a platform it was. I also use it in my teaching. It's the first hardware platform I've used that I feel like I can teach beginners with, and also use in my professional work as well.

As for personal projects: I developed a new version of my email clock (a clock that ticks forward for each new email received) using Arduino. I made a cat bed that emails me when it's taken a picture of the cat; an air-quality meter; a blinking fan sign for my favorite roller derby team; and more. I use it in class just about every day.

 **Have you ever seen Arduino used in a way you never intended it to be deployed?**

Well, it was intended to be deployed in a wide variety of ways, so not really. I guess for me, the one thing I never intend it to be used for is to hurt people or to do damage, so I hope I never see it used for that.

 **Do you have any advice for up-and-coming hardware hackers?**

Patience. Persistence. And frequent showers.  I get my best problem solving done in the shower.

 **Finally, is there anything else you'd like to add?**

Thanks to everyone who's used Arduino! We've had a great time working on it, and it's incredibly rewarding to see people realise things they didn't think were possible because of something we made.


References

.. [1]  :ref:`perl <perl>` 

.. [2]  :ref:`f# <f#>` 

.. [3] :ref:`haskell <haskell>` 

.. [4] :ref:`awk <awk>` 

.. [5] :ref:`ada <ada>` 

.. [6] :ref:`asp <asp>` 

.. [7] :ref:`bash <bash>` 

.. [8] :ref:`c++ <c++>` 

.. [9] :ref:`forth <forth>` 

.. [10] :ref:`intercal <intercal>` 

.. [11] :ref:`yacc <yacc>` 

.. [12] `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

.. [13] :ref:`falcon <falcon>` 

.. [14] :ref:`modula-3 <modula-3>` 

.. [15] :ref:`d <d>` 

.. [16] :ref:`javascript <javascript>` 

.. [16] :ref:`c# <c#>` 

.. [17] :ref:`python <python>` 

.. [18] :ref:`lua <lua>` 

.. [19] :ref:`tcl <tcl>` 

.. [20] `http://groovy.codehaus.org/ <http://groovy.codehaus.org/>`_ 

.. [21] :ref:`awk_ <awk_>` 

.. [22] `http://www.twitter.com/computerworldau <http://www.twitter.com/computerworldau>`_ 

.. [23] `http://www.computerworld.com.au/user/login?mlkey=7051255049448 <http://www.computerworld.com.au/user/login?mlkey=7051255049448>`_ 

.. _clojure:

The A-Z of Programming Languages: Clojure
=========================================
Our series on the most popular programming languages continues as we chat to Clojure creator Rich Hickey
* Kathryn Edwards(Computerworld)

* 10 August, 2009 22:47

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_18669.jpg
   :alt: Clojure creator Rich Hickey

Clojure creator Rich Hickey

 *Computerworld*is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8], and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`tcl <tcl>` [13], :ref:`falcon <falcon>` [14]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [15], Walter Bright :ref:`d <d>` [16], Brendan Eich on :ref:`javascript <javascript>` [17], Anders Hejlsberg on :ref:`c# <c#>` [18], Guido van Rossum on :ref:`python <python>` [19]and Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [20]. We most recently spoke to Joe Armstrong, creator of :ref:`erlang <erlang>` [21].

In this interview, `Clojure <http://clojure.org/>`_ [22]creator, Rick Hickey, took some time to tell *Computerworld*about his choice to create another Lisp dialect, the challenges of getting Clojure to better compete with Java and C#, and his desire to to see Clojure become a 'go-to' language.

 *If you wish to submit any suggestions for programming languages or would like to see a particular language authors interviewed, please emailkathryn@computerworld.com.au*----

 **What prompted the creation of Clojure?**

After almost 20 years of programming in C++/Java/C#, I was tired of it. I had seen how powerful, dynamic and expressive Common Lisp was and wanted to have that same power in my commercial development work, which targeted the JVM/CLR. I had made a few attempts at bridging Lisp and Java, but none were satisfying. I needed something that could deploy in a standard way, on the standard platforms, with very tight integration with existing investments.

At the same time, throughout my career I have been doing multithreaded programming, things like broadcast automation systems, in these OO languages, and seen nothing but pain. As a self-defense and sanity-preserving measure, I had moved my `Java <http://www.computerworld.com.au/tag/Java>`_ [23]and `C# <http://www.computerworld.com.au/tag/C%2523>`_ [24]code to a non-OO, functional style, emphasising immutability. I found this worked quite well, if awkward and non-idiomatic.

So, I wanted a dynamic, expressive, functional language, native on the JVM/CLR, and found none.

See related tutorial: an `Introduction to Clojure <http://www.computerworld.com.au/article/314348/introduction_clojure/>`_ [25]----

 **Where does the name Clojure come from?**

It's a pun on the closure programming construct (and is pronounced identically). I wanted a name that involved C (CLR), L (Lisp) and J (JVM). There were no search hits and the domain was available - what's not to like?

 **Was there a particular problem the language aimed to solve?**

Clojure is designed to support writing robust programs that are simple and fast. We suffer from so much incidental complexity in traditional OO languages, both syntactic and semantic, that I don't think we even realise it anymore. I wanted to make 'doing the right thing' not a matter of convention and discipline, but the default. I wanted a solid concurrency story and great interoperability with existing Java libraries.

 **Why did you choose to create another Lisp dialect instead of extending an existing one?**

While Lisps are traditionally extremely extensible, I had made some design decisions, like immutability for the core data structures, that would have broken backward compatibility with existing Scheme and Common Lisp programs. Starting with a clean slate let me do many other things differently, which is important, since I didn't want Clojure to appeal only to existing Lispers. In the end Clojure is very different and more approachable to those having no Lisp background.

 **Why did you pick the JVM?**

I originally targeted both the JVM and CLR, but eventually decided I wanted to do twice as much, rather than everything twice. I chose the JVM because of the much larger open source ecosystem surrounding it and it has proved to be a good choice. That said, the CLR port has been revitalised by David Miller, is an official part of the Clojure project and is approaching feature-parity with the JVM version.
-

----- **Clojure-in-Clojure: self-hosting is usually a big milestone for programming languages - how is that going?**

It is going well. We are approaching the end of the foundation-laying phase. There were a few base capabilities of Java which I leveraged in the implementation of Clojure for which there was no analogy in Clojure itself. Now the last of these is coming into place. Then there will be nothing precluding the implementation of the Clojure compiler and the Clojure data structures in Clojure itself, with efficiency equivalent to the original Java implementation.

 **Did you run into any big problems while developing the language?**

One of the biggest challenges was getting the persistent data structures right, with sufficient performance such that Clojure could be a viable alternative to Java and C#. Without that, I wouldn't have gone forward.

 **We've all read * `The rise of 'Worse is Better' <http://www.jwz.org/doc/worse-is-better.html>`_ [26]*by Richard Gabriel. Do you feel that a project like Clojure can help reverse that attitude?**

The arguments made in *Worse is Better*are very nuanced and I'm not sure I understand them all, so Clojure tries to take both sides! It values simplicity of interface and of implementation. When there is a conflict, Clojure errs on the side of pragmatism. It is a tool, after all.

 **With multi-core CPUs becoming more common and a resurgence of hyperthreading, dealing with concurrent tasks is now more important. How does Clojure deal with this?**

Good support for concurrency is a central feature of Clojure. It starts with an emphasis on functional programming. All of the core data structures in Clojure are immutable, so right off the bat you are always working with data that can be freely shared between threads with no locking or other complexity whatsoever, and the core library functions are free of side-effects. But Clojure also recognises the need to manage values that differ over time. It supports that by placing values in references, which both call out their stateful nature and provide explicit concurrency semantics that are managed by the language.

For example, one set of references in Clojure are transactional, which lets you conduct database-like transactions with your in-memory data and, like a database, automatically ensures atomic/consistent/isolated integrity when multiple threads contend for the same data. In all cases, Clojure's reference types avoid the complications and deadlocks of manual locking.

 **What can you tell us about the support for parallelism and the upcoming Java ForkJoin framework?**

While concurrency focuses on coordinating multiple tasks, parallelism focuses on dividing up a single task to leverage these multi-cores to get the result faster. I didn't build any low-level infrastructure for parallelism into Clojure since the Java concurrency experts were already doing that in the form of the ForkJoin framework, a sophisticated thread pool and work-stealing system for parallel computation. As that framework is stabilising and moving towards inclusion in Java 7 (and usable with Java 6), I've started implementing parallel algorithms, like mapping a function across a vector by breaking it into subtasks, using ForkJoin. Clojure's data structures are well suited for this decomposition, so I expect to see a rich set of parallel functions on the existing data structures - ie, you won't have to use special 'parallel' data structures.

 **What about running on distributed systems? MapReduce did come from Lisp...**

I don't think distribution should be hardwired into a general purpose programming language. Clojure can tap into the many options for distribution on the JVM - JMS, Hadoop, Terracotta, AMQP, XMPP, JXTA, JINI, JGroups etc, and people are already leveraging many of those.

 **How did you choose the Eclipse License for Clojure?**

The EPL has the advantage of being reciprocal without impinging on non-derivative work with which it is combined. Thus, it is widely considered to be commercial-friendly and acceptable for businesses.

 **Web frameworks? I notice there's one called 'Compojure'. Do you see this as a direction in which Clojure could grow?**

Definitely, there are already interesting frameworks for Clojure in many areas. One of the nice things about libraries for Clojure is that they can leverage tried-and-true Java libraries for the low-level plumbing and focus on higher-level use and flexibility. **What books would you recommend for those wanting to learn Clojure?**

 *Programming Clojure*, by Stuart Halloway, published by Pragmatic Programmers is *the*book right now and it's quite good - concise and inspiring, I highly recommend it. I know of a couple of other books in the works. 
-

----- **What's the most interesting program(s) you've seen written with Clojure?**

There are a bunch of start-ups doing interesting things I'm not sure I can talk about. Clojure has been applied so diversely, given its youth - legal document processing, an R-like statistical language, and a message routing system in a veterinary hospital, for example.

 **You recently released Clojure 1.0. What features were you the most excited about?**

Version 1.0 was less about new features than it was about stability. For example, the feature base was sufficient that people weren't lacking anything major for doing production work and it could serve as a baseline for Stuart's book.

 **Has hosting the project on Github helped you increase the number of contributors and the community around Clojure?**

The contributor list has been growing steadily. I think being on GitHub makes it easier for people to work on contributions.

 **I understand you started working on Clojure during a sabbatical. How has the situation changed now?**

I'd like to continue to work on Clojure full-time but in order to do so I need to find an income model. I can't say I've figured that out yet but, as Clojure gets more widespread commercial adoption, I'm hoping for more opportunities.

 **Perl gurus are 'Perl Mongers', Python ones are 'Pythonistas'. We think Clojure needs something similar. Any suggestions?**

I think everyone has settled on Clojurians.

 **What is it with Lisp programmers and nested lists?**

Programming with data structures might be unfamiliar to some but it is neither confusing nor complex once you get over the familiarity hump. It is an important and valuable feature that can be difficult to appreciate until you've given it a try.

 **This question must be asked... What's the highest number of closing brackets you've seen in a row?!**

What brackets?! I don't see them anymore and neither do most Clojure developers after a short time. One advantage of piling them up is that the code ends up being denser vertically so you can see more of the logic in one screen, versus many lines of closing }'s (Java et al) or end's (Ruby).

 **Looking back, is there anything you would change in the language's development?**

I think it's quite important that a significant portion of Clojure's design was informed by use, and continues to be so. I'm happy with the process and the outcome.

 **Where do you envisage Clojure's future lying?**

Right now we're in the early adopter phase, with startups and ISVs using Clojure as a secret weapon and power tool. Clojure is a general purpose language and already being applied in a wide variety of domains. It's impossible to predict but I'd be very happy to see Clojure become a go-to language when you want the speed of dynamic development coupled with robustness, performance and platform compatibility.

 **What do you think will be Clojure's lasting legacy?**

I have no idea. It would be nice if Clojure played a role in popularising a functional style of programming.

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`tcl <tcl>` 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`erlang <erlang>` 

*  `http://clojure.org/ <http://clojure.org/>`_ 

*  `http://www.computerworld.com.au/tag/Java <http://www.computerworld.com.au/tag/Java>`_ 

*  `http://www.computerworld.com.au/tag/C%2523 <http://www.computerworld.com.au/tag/C%2523>`_ 

*  `http://www.computerworld.com.auhttp://www.computerworld.com.au/article/314348/introduction_clojure/ <http://www.computerworld.com.au/article/314348/introduction_clojure/>`_ 

*  `http://www.jwz.org/doc/worse-is-better.html <http://www.jwz.org/doc/worse-is-better.html>`_ 

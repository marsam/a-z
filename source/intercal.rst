.. _intercal:

The A-Z of Programming Languages: INTERCAL
==========================================
Computerworld's investigation into the history of programming languages takes a humourous turn as we examine INTERCAL
* Naomi Hamilton(Techworld Australia)

* 04 July, 2008 13:09

.. image:: http://cdn.techworld.com.au/dimg/415x415/dimg/m_img_6962.jpg
   :alt: Don Woods

Don Woods

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`ada <ada>` [2]on the Ada 1995 and 2005 revisions, Microsoft about its server-side script engine :ref:`asp <asp>` [3], :ref:`bash <bash>` [4]about his experience maintaining Bash, :ref:`c++ <c++>` [5]of C++ fame, and to :ref:`forth <forth>` [6]about the design and development of Forth, and :ref:`yacc <yacc>` [7]on YACC.

In this interview, Computerworld ventures down a less serious path and chats to Don Woods about the development and uses of INTERCAL.

Woods currently works at Google, following the company's recent acquisition of Postini, and he is best known for co-writing the original Adventure game with Will Crowther. He also co-authored The Hackers Dictionary. Here we chat to him about all things spoof and the virtues of tonsils as removable organs.

 **How did you and James Lyon get the urge to create such an involved spoof language?**

I'm not entirely sure.  As indicated in the preface to the original reference manual, we came up with the idea (and most of the initial design) in the wee hours of the morning.  We had just finished our -- let's see, it would have been our freshman year -- final exams and were more than a little giddy!

My recollection, though fuzzy after all these years, is that we and another friend had spent an earlier late-night bull session coming up with alternative names for spoken punctuation (spot, spark, spike, splat, wow, what, etc.) and that may have been a jumping off point in some way.

 **Why did you choose to spoof FORTRAN and COBOL in particular?**

We didn't.  (Even though Wikipedia seems to claim we did.)  We spoofed the languages of the time, or at least the ones we were familiar with.  (I've never actually learned COBOL myself, though I believe Jim Lyon knew the language.)  The manual even lists the languages we were comparing ourselves to.  And then we spoofed the reference manuals of the time, especially IBM documentation, again since that's what we were most familiar with.  Admittedly, the language resembles FORTRAN more than it does, say, SNOBOL or APL, but then so do most computer languages.

 **What prompted the name Compiler Language With No Pronounceable Acronym? And how on earth did you get INTERCAL out of this?**

I think we actually started with the name INTERCAL.  I'm not sure where it came from; probably it just sounded good.  (Sort of like FORTRAN is short for "Formula Translation", INTERCAL sounds like it should be short for something like "Interblah Calculation").  I don't remember any more specific etymology.  Then when we wanted to come up with an acronym, one of us thought of the paradoxical "Compiler Language With No Pronounceable Acronym."

 **How long did it take to develop INTERCAL? Did you come across any unforeseen problems during the initial development period?**

That depends on what you mean by "develop".  We designed the language without too much trouble.  Writing the manual took a while, especially for things like the circuit diagrams we included as nonsensical illustrations.  The compiler itself actually wasn't too much trouble, given that we weren't at all concerned with optimising the performance of either the compiler or the compiled code.

Our compiler converted the INTERCAL program to SNOBOL (actually SPITBOL, which is a compilable version of SNOBOL) and represented INTERCAL datatypes using character strings in which all the characters were "0"s and "1"s.

-

-----

 **Do you use either C-INTERCAL or CLC-INTERCAL currently?**

No, though I follow the alt.lang.intercal newsgroup and occasionally post there.

 **Have you ever actually tried to write anything useful in INTERCAL that actually works? Has anyone else?**

Me, no.  Others have done so.  I remember seeing a Web page that used INTERCAL (with some i/o extensions no doubt) to play the game "Bugs and Loops", in which players add rules to a Turing machine trying to make the machine run as long as possible without going off the end of its tape or going into an infinite loop.

 **How do you feel given that the language was created in 1972, and variations of it are still being maintained? Do you feel like you have your own dedicated following of spoof programmers now?**

I admit I'm surprised at its longevity.  Some of the jokes in the original work feel rather dated at this point.  It helps that the language provides a place where people can discuss oddball features missing from other languages, such as the "COME FROM" statement and operators that work in base 3.

And no, I don't feel like a have a "following", though every once in a while I do get caught off-guard by someone turning out to be an enthusiastic INTERCAL geek.  When I joined Google some months back, someone apparently noticed my arrival and took the opportunity to propose adding a style guide for INTERCAL to go alongside Google's guides for C++, Java and other languages.  (The proposal got shot down, but the proposed style guide is still available internally.)

 **Did you have a particular person in mind when you wrote the following statement in the reference manual: "It is a well-known and oft-demonstrated fact that a person whose work is incomprehensible is held in high esteem"?**

Oddly, I don't think we had anyone specific in mind.

 **Do you know of anyone who has been promoted because they demonstrated their superior technical knowledge by showing off an INTERCAL program?**

Heh, no.

 **The footnotes of the manual state: "4) Since all other reference manuals have Appendices, it was decided that the INTERCAL manual should contain some other type of removable organ." We understand why you'd want to remove the appendix, no one likes them and they serve no purpose, but tonsils seem to be much more useful. Do you regret your decision to pick the tonsil as the only removable organ?**

No, I never gave that much thought.  We were pleased to have come up with a second removable organ so that we could make the point of not including an appendix.  Besides, just because it's removable doesn't mean it's not useful to have it!

 **Did you struggle to make INTERCAL Turing-complete?**

Struggle?  No.  We did want to make sure the language was complete, but it wasn't all that hard to show that it was.

 **How do you respond to criticism of the language, such as this point from Wikipedia: "A Sieve of Eratosthenes benchmark, computing all prime numbers less than 65536, was tested on a Sun SPARCStation-1. In C, it took less than half a second; the same program in INTERCAL took over seventeen hours"?**

Excuse me?  That's not criticism, that's a boast!  Back in our original implementation on a high-end IBM mainframe (IBM 360/91), I would boast that a single 16-bit integer division took 30 seconds, and claimed it as a record!

-

-----

 **Would you do anything differently if you had the chance to develop INTERCAL again now?**

I'm sure there are fine points I'd change, and I'd include some of the more creative features that others have proposed (and sometimes implemented) over the years.  Also, some of the jokes and/or language features are a bit dated now, such as the XOR operator being a "V" overstruck with a "-", and our mention of what this turns out to be if the characters are overstruck on a punched card.

 **In your opinion, has INTERCAL contributed anything useful at all to computer development?**

Does entertainment count? :-)  I suppose there are also second-order effects such as giving some people (including Lyon and myself) a chance to learn about compilers and the like.

Perhaps more important, when you have to solve problems without using any of the usual tools, you can sometimes learn new things.  In 2003 I received a note from Knuth saying he had "just spent a week writing an INTERCAL program" that he was posting to his "news" web page, and while working on it he'd noticed that "the division routine of the standard INTERCAL library has a really cool hack that I hadn't seen before". He wanted to know if I could remember which of Lyon or myself had come up with it so he could give proper credit when he mentioned the trick in volume 4 of The Art of Computer Programming.  (I couldn't recall.)

 **Where do you envisage INTERCAL's future lying?**

I've no idea, seeing as how I didn't envisage it getting this far!

 **Has anyone ever accidentally taken INTERCAL to be a serious programming language?**

Heavens, I hope not!  (Though I was concerned YOU had done so when you first contacted me!)

 **Have you been impressed by any other programming languages, such as Brain****?**

I've looked at a few other such languages but never spent a lot of time on them.  Frankly, the ones that impress me more are the non-spoof languages that have amazingly powerful features (usually within limited domains), such as APL's multidimensional operations or SNOBOL's pattern matching.  (I'd be curious to go back and look at SNOBOL again now that there are other languages with powerful regular-expression operators.)

The closest I've come to being impressed by another "limited" programming language was a hypothetical computer described to me long ago by a co-worker who was a part-time professor at Northeastern University.  The computer's memory was 65536 *bits*, individually addressable using 16-bit addresses.  The computer had only one type of instruction; it consisted of 48 consecutive bits starting anywhere in memory.  The instruction was interpreted as three 16-bit addresses, X Y Z, and the operation was "copy the bit from location X to location Y, then to execute the instruction starting at location Z".  The students were first tasked with constructing a conditional branch (if bit A is set go to B, else go to C).  I think the next assignment was to build a 16-bit adder.  Now THAT'S minimalist!

 **Where do you see computer programming languages heading in the near future?**

An interesting question, but frankly it's not my field so I haven't spent any time pondering the matter.  I do expect we'll continue to see a growing dichotomy between general programming languages (Perl, Python, C++, Java, whatever) and "application-level" languages (suites for building Web-based tools and such). It seems that we currently have people who use the general programming languages, but don't have any understanding of what's going on down at the microcode or hardware levels.

 **Do you have any advice for up-and-coming spoof programmers?**

Try to find a niche that isn't already filled.  Hm, you know, SPOOF would be a fine name for a language.  It's even got OO in the name!

 **And finally, as already discussed with Bjarne Stroustrup, do you think that facial hair is related to the success of programming languages?**

I hadn't seen that theory before, but it's quite amusing.

(See `here <http://maplenewsnetwork.wordpress.com/2008/05/25/the-relation-between-beards-and-the-succes-of-programming-languages/>`_ [8])

I don't think I had any facial hair when we designed INTERCAL, but I've been acquiring more over the years.  Maybe that's why INTERCAL's still thriving?

 **Is there anything else you'd like to add?**

In some sense INTERCAL is the ultimate language for hackers, where I use "hacker" in the older, non-criminal sense, meaning someone who enjoys figuring out how to accomplish something despite the limitations of the available tools.  (One of the definitions in The Hacker's Dictionary is, "One who builds furniture using an axe.")  Much of the fun of INTERCAL comes from figuring out how it can be used to do something that would be trivial in other languages.  More fun is had by extending the language with weird new features and then figuring out what can be done by creative use of those features.

References
*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/216844/a-z_programming_languages_awk/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/216844/a-z_programming_languages_awk/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/223388/a-z_programming_languages_ada/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/223388/a-z_programming_languages_ada/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/224282/a-z_programming_languages_asp/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/224282/a-z_programming_languages_asp/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/222764/a-z_programming_languages_bash_bourne-again_shell/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/222764/a-z_programming_languages_bash_bourne-again_shell/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/250514/a-z_programming_languages_c_/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/250514/a-z_programming_languages_c_/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/250530/a-z_programming_languages_forth/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/250530/a-z_programming_languages_forth/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/252319/a-z_programming_languages_yacc/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/252319/a-z_programming_languages_yacc/>`_ 

*  `http://maplenewsnetwork.wordpress.com/2008/05/25/the-relation-between-beards-and-the-succes-of-programming-languages/ <http://maplenewsnetwork.wordpress.com/2008/05/25/the-relation-between-beards-and-the-succes-of-programming-languages/>`_ 

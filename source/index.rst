.. A-Z documentation master file, created by
   sphinx-quickstart on Thu Apr 11 01:52:42 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The A to Z of programming languages
===================================

Source: `The A to Z of programming languages <http://www.computerworld.com.au/article/344826/z_programming_languages/>`_. `Computerworld Australia <http://www.computerworld.com.au/>`_

Contents:

.. toctree::
   :maxdepth: 2

   asp.rst
   awk.rst
   awk_.rst
   ada.rst
   arduino.rst
   bash.rst
   c#.rst
   c++.rst
   clojure.rst
   coldfusion.rst
   d.rst
   erlang.rst
   f#.rst
   falcon.rst
   forth.rst
   groovy.rst
   haskell.rst
   intercal.rst
   javascript.rst
   lua.rst
   matlab.rst
   modula-3.rst
   objective-c.rst
   perl.rst
   python.rst
   scala.rst
   scala_.rst
   shakespeare.rst
   tcl.rst
   yacc.rst
   sh.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


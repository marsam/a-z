.. _perl:

The A-Z of Programming Languages: Perl
======================================
Culture and community go hand-in-hand with Perl programming
* Rodney Gedda(Techworld Australia)

* 11 December, 2008 16:02

.. image:: http://cdn.techworld.com.au/dimg/415x415/dimg/m_img_12879.jpg
   :alt: Perl creator Larry Wall

Perl creator Larry Wall

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Alfred v. Aho of :ref:`awk <awk>` [1], S. Tucker Taft on the :ref:`ada <ada>` [2], Microsoft about its server-side script engine :ref:`asp <asp>` [3], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [4], Bjarne Stroustrup of :ref:`c++ <c++>` [5]and to Charles H. Moore about the design and development of :ref:`forth <forth>` [6]. We've also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [7], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [8], Luca Cardelli on :ref:`modula-3 <modula-3>` [9], Walter Bright :ref:`d <d>` [10], and more recently, Simon Peyton-Jones on :ref:`haskell <haskell>` [11].

This time we chat with `Larry Wall <http://en.wikipedia.org/wiki/Larry_Wall>`_ [12], creator of the `Perl programming language <http://www.perl.org>`_ [13]and regarded as the father of modern scripting languages.

 **What prompted the development of Perl?**

I was scratching an itch, which is the usual story. I was trying to write reports based on text files and found the Unix tools were not quite up to it, so I decided I could do better. There was something missing in Unix culture — it was either C or a shell script, and people see them as opposites in one continuum. They were sort of orthogonal to each other and that is the niche Perl launched itself into — as a glue language. Unlike academic languages, which tend to be insular, I determined from the outset I was going to write Perl with interfaces.

Only later did it turn into a tool for something that was not anticipated. When the Web was invented they needed to generate text and use a glue language to talk to databases.

 **Was there a particular problem you were trying to solve?**

You can tell the other problem by the reaction Perl got from the die hards in the Unix community. They said tools should do one thing and do them well. But they didn't understand Perl was not envisioned as a tool so much as a machine shop for writing tools.

 **How did the name Perl come about?**

I came up with the name as I wanted something with positive connotations. The name originally had an "a" in it. There was another lab stats language called Pearl, so I added another backronym. The second one is Pathologically Eclectic Rubbish Lister.

 **Do you ever find yourself using the “backronym” Practical Extraction and Report Language at all?**

It is meant to indicate that there is more than way to do it, so we have multiple backronyms intentionally.

 **Were there any particularly hard/annoying problems you had to overcome in the development of the language?**

The annoying thing when you're coming up with a new language is you can't really design it without taking into account the cultural context. A new language that violates everyone's cultural expectations has a hard time being accepted. Perl borrowed many aspects out of C, shell and AWK which were occasionally difficult to reconcile. For example, the use of $ in a regular expression might mean match a string or interpret a variable.

-

-----

 **Would you have done anything differently in the development of Perl if you had the chance?**

Either nothing or everything. See Perl 6.

 **What is the most interesting program that you've seen written with Perl?**

I've seen an awful lot of interesting things written in Perl, maybe they are all weird. I know it's being used at the South Pole. The latest group to use it heavily are the biologists who do genetic analysis.

 **Have you ever seen the language used in a way that was not originally intended? If so, what was it? And did it work?**

When Clearcase (revision control systems) wrote its device driver in Perl to access the file system underneath the kernel. The first surprising thing is that it worked. And the second surprising thing is that it was 10 times faster than their C code. Generally you would not want to write device drivers in Perl. Perl 6 maybe, but not Perl 5.

 **Has the evolution and popularity of the language surprised you in any way?**

Yes and no. I definitely had experience prior to this with releasing open source software and finding that people liked it, so I already knew that if I wrote a language I liked other people would probably like it too.

I didn't anticipate the scale of the acceptance over time. Perl 5 opened up to community development, and the best thing about Perl is CPAN.

 **In what way do you think employing natural language principles in Perl has contributed to it's success?**

That's a subject of a PhD dissertation. We use natural language — most people think COBOL — and that's not how we think about it. Rather, the principles of natural language are that everything is context sensitive and there is more than one way to say it. You are free to learn it as you go.

We don't expect a five-year-old to speak with the same diction as a 50 year-old. The language is built to evolve over time by the participation of the whole community. Natural languages use inflection and pauses and tone to carry meanings. These carry over to punctuation in written language, so we’re not afraid to use punctuation either.

 **What are you proudest of in terms of the language's initial development and continuing use?**

It has to be the people involved. I think the Perl community paints a little picture in heaven. At the first Perl conference we met many in the Perl community for the first time and it was near my house so the family threw a party. The first thing we noticed is how pathologically helpful the people are and yet everyone was accepting of everyone's differences. The community counts diversity as a strength and that's what holds us together.

-

-----

 **How did a picture of a camel come to end up on Programming Perl and consequently end up as a symbol for the language? Were you involved with this at all?**

Yes. I picked the camel. When a writer writes a book for O'Reilly they ask them to suggest an animal. And then they say “no, you are going to use a mongoose instead”.

If I had asked for a left-brain cover I would have asked for an oyster. But I shared the vision of the cover designer. The right-brain meaning of a camel is an animal self-sufficient in a dry place, and there are vague biblical connotations of a caravan. Since that was more or less the Perl bible for many years, it kind of naturally became the mascot.

 **Do you agree with statements stating that Perl is practical rather than beautiful? Was this your intention starting out?**

Considering I wrote that into the first Perl manual page, yes. We are trying to make it more beautiful these days without loosing its usefulness. Maybe the next Perl book will have a camel with butterfly wings on it or something.

 **Many sources quote a main reference point of Perl being the C language. Was this deliberate?**

Definitely. C has never exactly been a portable language but it is ubiquitous. By writing complex shell scripts and many macros you can create a portable C and then write a portable language on top. So that made Perl able to be ported everywhere and that was important to us.

 **How do you feel about the Comprehensive Perl Archive Network (CPAN) carrying over 13,500 modules by over 6,500 authors? Why do you think that the archive network has been such a success?**

By it's very size it doesn't matter about Sturgeons Law — that 90 of everything is crud. 10 percent of a very large number is still a large number.

 **Do you agree with the following statement from Wikipedia: “The design of Perl can be understood as a response to three broad trends in the computer industry: falling hardware costs, rising labour costs, and improvements in compiler technology. Many earlier computer languages, such as Fortran and C, were designed to make efficient use of expensive computer hardware. In contrast, Perl is designed to make efficient use of expensive computer programmers.”**

That's accurate. In addition to C, I used :ref:`yacc <yacc>` [8]which was available. But I wrote my own lexer.

 **Do you agree that Perl is the “duct tape of the Internet”?**

It's one metaphor that we accept, but we like lots of metaphors.

 **Do you endorse the version of Perl written for Windows: win32.perl.org by Adam Kennedy?**

Yes, I've used it and it is a good port.

-

-----

 **You once listed the three virtues of a programmer as laziness, impatience and hubris. a) In what way do you think these virtues can be fostered by the way a language is designed, or are they merely a characteristic of a developer? b) How does the design of Perl encourage those virtues?**

If you are lazy you look for shortcuts. If you are impatient you want your program to be done now. And as for the hubris, that makes the programs easier to distribute. That will help programs be used universally and that has some ego value.

My own take on that personally is it's been a privilege not to just write a programming language but invent a new medium of art that other people can work in.

 **Why has no specification or standard for the language been created?**

There has for Perl 6. It's one of the things we decided to change. There will be multiple implementations of Perl 6, so it needs a standard and there will be a test suite.

We have a saying: all is fair if you pre-declare it. The idea with Perl 6 is you start with a standard language and you can mutate it. As long as you follow that refinement process there isn't the problem of ambiguity. There is the problem of multiple dialects, but that will always be a problem.

 **Have you ever played Perl Golf or written a Perl poem?**

I wrote the first Perl poem and played Perl Golf. But I'm more well know for my obfuscation of C rather than Perl. It's been a long time.

 **What new elements does Perl 5.10.0 bring to the language? In what way is it preparing for Perl 6?**

Perl 5.10.0 involves backporting some ideas from Perl 6, like switch statements and named pattern matches. One of the most popular things is the use of “say” instead of “print”.

This is an explicit programming design in Perl — easy things should be easy and hard things should be possible. It's optimised for the common case. Similar things should look similar but similar things should also look different, and how you trade those things off is an interesting design principle.

Huffman Coding is one of those principles that makes similar things look different.

 **And what about Perl 6? Do you have a release date for this yet? Are you able to talk about the most exciting/interesting new developments with this?**

Sure, It's Christmas Day — we just don't say which one. We've been working on it 8 years now and we would like to think we are a lot closer to the end than the beginning. We're certainly well into the second 80 percent.

-

-----

 **In your opinion, what lasting legacy has Perl brought to computer development?**

An increased awareness of the interplay between technology and culture. Ruby has borrowed a few ideas from Perl and so has PHP. I don't think PHP understands the use of signals, but all languages borrow from other languages, otherwise they risk being single-purpose languages. Competition is good.

It's interesting to see PHP follow along with the same mistakes Perl made over time and recover from them. But Perl 6 also borrows back from other languages too, like Ruby. My ego may be big, but it's not that big.

 **Where do you envisage Perl's future lying?**

My vision of Perl's future is that I hope I don't recognise it in 20 years.

 **Where do you see computer programming languages heading in the future, particularly in the next 5 to 20 years?**

Don't design everything you will need in the next 100 years, but design the ability to create things we will need in 20 or 100 years. The heart of the Perl 6 effort is the extensibility we have built into the parser and introduced language changes as non-destructively as possible.

 **Do you have any advice for up-and-coming programmers?**

We get Google Summer of Code people and various students are interested in the Perl 6 effort. We try to find a place for them to feel useful and we can always use people to write more tests.

In the future a lot of people will be getting into programming as a profession, but not calling it programming. They will call it writing spreadsheets or customising actions for their avatars. For many people it will be a means to an end, rather than an end in itself.

References
*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/216844/a-z_programming_languages_awk/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/216844/a-z_programming_languages_awk/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/223388/a-z_programming_languages_ada/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/223388/a-z_programming_languages_ada/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/224282/a-z_programming_languages_asp/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/224282/a-z_programming_languages_asp/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/222764/a-z_programming_languages_bash_bourne-again_shell/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/222764/a-z_programming_languages_bash_bourne-again_shell/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/250514/a-z_programming_languages_c_/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/250514/a-z_programming_languages_c_/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/250530/a-z_programming_languages_forth/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/250530/a-z_programming_languages_forth/>`_ 

*  `http://www.techworld.com.auhttp://www.techworld.com.au/article/251892/a-z_programming_languages_intercal/ <http://www.techworld.com.auhttp://www.techworld.com.au/article/251892/a-z_programming_languages_intercal/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/252319/a-z_programming_languages_yacc/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/252319/a-z_programming_languages_yacc/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/252531/a-z_programming_languages_modula-3/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/252531/a-z_programming_languages_modula-3/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/253741/a-z_programming_languages_d/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/253741/a-z_programming_languages_d/>`_ 

*  `http://www.techworld.com.auhttp://www.computerworld.com.au/article/261007/a-z_programming_languages_haskell/ <http://www.techworld.com.auhttp://www.computerworld.com.au/article/261007/a-z_programming_languages_haskell/>`_ 

*  `http://en.wikipedia.org/wiki/Larry_Wall <http://en.wikipedia.org/wiki/Larry_Wall>`_ 

*  `http://www.perl.org <http://www.perl.org>`_ 

.. _matlab:

The A-Z of Programming Languages: MATLAB
========================================
Our series on the most popular programming languages continues as we chat to MATLAB creator Cleve Moler
* Kathryn Edwards(Computerworld)

* 09 December, 2009 18:30

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_22432.jpg
   :alt: MATLAB creator Cleve Moler. Photo credit: The MathWorks.

MATLAB creator Cleve Moler. Photo credit: The MathWorks.

 *Computerworld*is undertaking a series of investigations into the most widely-used programming languages. Previously we spoke to Larry Wall, creator of :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8], and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`tcl <tcl>` [13], :ref:`falcon <falcon>` [14]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [15], Walter Bright :ref:`d <d>` [16], Brendan Eich on :ref:`javascript <javascript>` [17], Anders Hejlsberg on :ref:`c# <c#>` [18], Guido van Rossum on :ref:`python <python>` [19], Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [20]and Joe Armstrong, creator of :ref:`erlang <erlang>` [21]. We most recently spoke to :ref:`coldfusion <coldfusion>` [22]co-creator, Jeremy Allaire.

In this interview, which took place on the 25th anniversary of The MathWorks, `MATLAB <http://www.mathworks.com/products/matlab/>`_ [23]creator, Cleve Moler, took time to tell *Computerworld*about the unexpected popularity of the language, its influence on modern day maths, science and engineering and why today's computer science students should keep studying.

 *If you wish to submit any suggestions for programming languages or would like to see a particular language authors interviewed, please emailkathryn@computerworld.com.au*----

 **What prompted the development of MATLAB?**

It just so happens that December 7th is the 25th anniversary of MathWorks! But the development of MATLAB started about 10 years before that. At the time I was a professor of mathematics and computer science at the University of New Mexico and in the 1970s there were two Fortran software projects called LINPACK and EISPACK. LINPAC is today known as the benchmark, the basis for deciding the Top 500 supercomputers. But 30 years ago it was a software project involving matrices and I wanted students at the university to have access to LINPACK and EISPACK without writing Fortran programs. So I wrote the first version of MATLAB, in Fortran, 30 years ago, just as a program for my students to use.

 **Were you trying to solve a particular problem?**

It was problems involving computations with matrices and mathematics, which was very specialised with a very narrow focus. [I had] no idea that it would be a commercial product and no intention of starting a company.

 **Yourself, Jack Little and Steve Bangert were the original team behind MATLAB and MathWorks — what role did each person play in the program and company’s establishment?**

Little is an electrical engineer. In 1979 I visited Stanford University; I was on a sabbatical there and I taught a course and used MATLAB in the course. Engineering students at Stanford took the course and found it useful in engineering problems that I didn’t know anything about — topics called control theory and signal processing. Little had gone to Stanford and was working near the campus and he heard about MATLAB from the students, some friends that took my course. He got excited about it as something that could be used in engineering.

The mathematics that I was using was useful in these engineering subjects and I didn’t even realise it.

Bangert was a friend of Little’s and was our chief programmer for a number of years.

I’m the father of MATLAB and Little is the father of MathWorks the company. He’s the real heart and soul and the basis for the success of the company.

 **How has the evolution and popularity of MATLAB surprised you? Did you ever expect it to reach one million users?**

No, no. I had no idea, no thought in forming a commercial company, no idea of how far this [could go]. My first MATLAB was very primitive. It was hardly even a programming language, but Little turned it into a real programming language when he became involved in the early 1980s. And today there’s so many different kinds of uses of it.

 **Was there a moment when its popularity really hit you?**

We had started the company, I was living in California, the company was in Massachusetts, and I came back to visit Little. I saw we had an office with a conference table – a real conference table! Then we had this Christmas party, 25 years ago, and there were a lot of people at the Christmas party and I said: “Wow, we got a real company here!”

 **MATLAB is known for its great array and matrix handing. Do you think you have influenced many general purpose languages with that?**

Well, MATLAB itself has expanded to become a general purpose language. MATLAB stands for ‘matrix laboratory’, but it’s gone way beyond that, particularly with Simulink, our companion product, which lots of people are using for things that don’t even involve matrices.

Some competitors have been modelled after and made to compete with MATLAB and have gotten their inspiration from MATLAB. There are some open source MATLAB clones, there's the popular languages used in statistics called S and R. Those guys were very much influenced by MATLAB. There’s now an add-on to Python called Numerical Python, which very much looks like MATLAB.

 **Are you aware of any everyday products that use MATLAB as one of their tools for creation?**

Absolutely! One of the most interesting is hearing aids. There’s a famous Australian company called Cochlear that makes hearing aids. Several years ago my wife was looking for a hearing aid for her mother. She was on the web and she came across the Cochlear website. She said, “Hey Cleve there’s a MATLAB plot!” So it turns out my mother-in-law has a MATLAB designed hearing aid.

All the major automobile manufacturers use MATLAB in the design of the electronics in the car: The anti-lock brakes, the electronic ignition, motors running the windows. MATLAB doesn’t actually run in your car, but its electronics were most likely designed with MATLAB. The same is true of airplanes and cell phones.
-

----- **Can you tell us more about the graphics and plotting abilities of MATLAB?**

This has absolutely been one of the important aspects of its popularity. It was added very early on to accompany the matrices' [functionality] and make it easy to use plots. Today, they’re used throughout science and engineering. Whenever I read a scientific or engineering publication or journal article and there’s a plot in it I look to see if it’s made from MATLAB. It's sort of puzzle; they don’t say if it is a MATLAB plot — they don’t need to — but there are clues in the way the axes are labelled and so on that indicates a MATLAB plot.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of MATLAB?**

Early on, 20 years ago, it was important for us to run on all of the computers that were around. Unix workstations like Sun's were much more powerful than PCs and there were several Unix workstations: Sun, Apollo and so on. They’re not in business anymore because the PC has overtaken them, but in the early days it was important that we work on all these different architectures because our customers didn’t just use one machine, they had access to a number of different machines and the fact that they could move their MATLAB programs from one machine to another was an important aspect of preserving popularity. That was difficult to do, because there were a lot of operating systems and not a lot of standards.

 **Would you have done anything differently in the development of MATLAB if you had the chance?**

That’s a good question. MATLAB is a programming language and most users use it as this programming language. So it has evolved from something that was very primitive to a modern programming language, object oriented and so on. Its evolution, from a primitive calculator to a modern programming language, has been very difficult.

If I had started out at the beginning to design a programming language, MATLAB probably would have been something quite different. The original intention was that it would be easy to use and that it would have solid mathematics underlying it. I’m glad I did it the way I did, but if I knew what it was going to be today, I might not have done it that way.

 **Have you ever seen MATLAB used in a way in which you never intended it to be used?**

Yes I have, many times! One of the most remarkable was at Australia's `Synchrotron centre <http://www.synchrotron.org.au/>`_ [24]outside Melbourne. The software that controls the machine, the magnets, the timing and the operation of the machine was written in MATLAB. I saw a demonstration of that when I was in Australia two or three years ago. It’s not only used at that facility but they share that software with atom-smashers in other countries.

 **What’s your favourite feature of MATLAB, if you had to pick one?**

It’s the mathematics. The thing that I enjoy as a mathematician is finding out how mathematics underlies so many different disciplines. We didn't intend to do automobiles, anti-lock brakes or human genome or pricing of
derivatives in the finance market. We never set out to do any of that originaly, but mathematics is common to all of these. I really enjoy talking about how these different fields are unified by the underlying mathematics.

 **What are you proudest of in terms of MATLAB’s initial development and continuing use?**

The popularity, the fact that this is now used by probably one million people around the world and the fact that the science and engineering influences peoples lives. That’s not something that a research mathematician expects to see his work used in. That has been very gratifying.

 **Do you have any advice for today’s maths, science and engineering students?**

Stay in school. I’m serious, it’s very tempting for these guys [to leave early], particularly in the computer business. It’s so attractive and they get such good jobs. They can go out and be a web designer, they’re attracted by computer graphics, games, the film industry. That’s exciting, attractive work and these students leave school [university] to go get those good jobs. For the long term, they should stay in school and learn a little bit more math and a little more engineering before they succumb to all the attractive industries.

 **What do you wish would be taught more in universities?**

We’re on the intersection between mathematics, engineering and computer science. In many universities, those three disciplines just concentrate on their own little field. The mathematicians don’t want to dirty their hands with engineering, the engineers are afraid of mathematics, it’s the interdisciplinary, the combination of all three of those that students should have a chance to appreciate.

 **What do you envisage for MATLAB’s future?**

Biomedical areas, research medicine and research biology are areas where we’re just beginning to have an impact. Our biggest competitor is actually [Microsoft] Excel. A lot of technical people do calculations with a spreadsheet, but they’d be better off using MATLAB for it and that’s the audience we want to reach. Not a particular discipline, but all the scientists and engineers who haven’t gone to the trouble to learn more powerful methods to do the calculations they want to do.

 **What’s next for MathWorks?**

Stay on course. We’ve come through this world economic crisis in good shape. Some of our customers have been hit hard, but we survived well. We’ve got to continue to attract good people, good students out of the universities all around the world.

 **MathWorks is celebrating 25 years. Do you think there will be a 50th and, eventually, 100th anniversary?**

Some people are saying that! I’m getting on in years, I’m not sure I’ll be here for the 50th!

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`tcl <tcl>` 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`erlang <erlang>` 

*  :ref:`coldfusion <coldfusion>` 

*  `http://www.mathworks.com/products/matlab/ <http://www.mathworks.com/products/matlab/>`_ 

*  `http://www.synchrotron.org.au/ <http://www.synchrotron.org.au/>`_ 

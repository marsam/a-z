.. _c#:

The A-Z of Programming Languages: C# 
=====================================
Microsoft's Anders Hejlsberg reveals the history behind one of the most common programming languages, C#, and what the future holds for C#4.0.
* Naomi Hamilton(Computerworld)

* 01 October, 2008 13:00

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_10827.jpg
   :alt: Anders Hejlsberg

Anders Hejlsberg

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Alfred v. Aho of :ref:`awk <awk>` [1]fame, S. Tucker Taft on the :ref:`ada <ada>` [2], Microsoft about its server-side script engine :ref:`asp <asp>` [3], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [4], Bjarne Stroustrup of :ref:`c++ <c++>` [5], and Charles H. Moore about the design and development of :ref:`forth <forth>` [6].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [7], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [8], Luca Cardelli on :ref:`modula-3 <modula-3>` [9], Walter Bright :ref:`d <d>` [10], Brendan Eich on :ref:`javascript <javascript>` [11], Guido van Rossum on :ref:`python <python>` [12]and Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [13]. We most recently spoke to Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [14].

In this interview Microsoft's leader of C# development, `Anders Hejlsberg <http://en.wikipedia.org/wiki/Anders_Hejlsberg>`_ [15], took some time to tell *Computerworld*about the development of C#, his thoughts on future programming trends, and his experiences putting out fires. Hejlsberg is also responsible for writing the `Turbo Pascal <http://en.wikipedia.org/wiki/Turbo_Pascal>`_ [16]system, and was the lead architect on the team that developed `Delphi <http://en.wikipedia.org/wiki/Delphi_programming_language>`_ [17].

( *Please note that due to popular demand we are no longer following alphabetical order for this series. If you wish to submit any suggestions for programming languages or language authors you would like to see covered, please email naomi@computerworld.com.au.*)

 **What were the fundamental flaws in other languages that you believe drove the development of Common Language Runtime (CLR), and in turn, C#?**

I wouldn’t say that our primary motivation for CLR was fundamental flaws in other languages. But we certainly had some key goals in mind. Primarily, we wanted to build a unified and modern development platform for multiple programming languages and application models.

To put this aim in context, we need to look back to the inception of .NET, which was in the late nineties or early 2000s. At that time, Microsoft’s primary developer offerings were fairly fragmented. For native code we had C++ with MFC, and ATL and so forth. And then for rapid application development we had Visual Basic, and for web development we had IIS and ASP.

Each language was its own little silo with different solutions to all of the different programming problems. You couldn’t transfer your skills and your application model implicitly became your choice of programming language. We really wanted to unify these separate entities to better leverage our efforts.

We also wanted to introduce modern concepts, such as object orientation, type safety, garbage collection and structured exception handling directly into the platform. At the time, the underlying infrastructure we were running on was COM, which is a very low  level programming model that requires you to deal with the registry and reference counting and HRESULTs and all that stuff.

These factors were, at the time, the motivators for .NET. There was also a competitive angle with Sun and Java etc.

Now, to move on to C#, in a nutshell our aim was to create a first class modern language on this platform that would appeal to the curly braces crowd: the C++ programmers of the world at the time, and competitively, the Java programmers.

There were several elements that we considered key design goals, like support for the next level up from object oriented programming, to component based programming where properties and metadata attributes were all first class in the language. Also, a unified and extensible type system, which sort of gets into value types and boxing etc. Versioning was a big thing; making sure we designed the language so that it would version well, so that whenever we added new features to the language we would not break code in older applications. These were all values that were important to us.

Of course, at the end of the day, productivity has always been a driver for me in all of the projects I’ve worked on. It’s about making programmers more productive.

-

-----

 **Why was the language originally named Cool, and what promoted the change to C#?**

The code name was Cool, which stood for ‘C like Object Oriented Language’. We kind of liked that name: all of our files were called .cool and that was kind of cool! We looked seriously at keeping the name for the final product but it was just not feasible from a trademark perspective, as there were way too many cool things out there.

So the naming committee had to get to work and we sort of liked the notion of having an inherent reference to C in there, and a little word play on C++, as you can sort of view the sharp sign as four pluses, so it’s C++++. And the musical aspect was interesting too. So C# it was, and I’ve actually been really happy with that name. It’s served us well.

 **How has your experience designing Visual J++, Borland Delphi and Turbo Pascal impacted on C#?**

If you go back to the Turbo Pascal days, the really new element created by Turbo Pascal was that it was the first product ever to commercialize the integrated development environment, in a broad sense – the rapid turnaround cycle between compile, edit or edit, compile, debug. Any development tool today looks that same way, and that of course has always been a key thing.

[I also learnt to] design the language to be well-toolable. This does impact the language in subtle ways – you’ve got to make sure the syntax works well for having a background compiler, and statement completion. There are actually some languages, such as SQL, where it’s very hard to do meaningful statement completion as things sort of come in the wrong order. When you write your SELECT clause, you can’t tell what people are selecting from, or what they might select until after writing the FROM clause. There are things like that to keep in mind.

Each of the products I’ve worked on, I’d say, have taught me valuable lessons about what works and what doesn’t, and of course you end up applying that knowledge to subsequent products you work on. For example, Delphi was the first product I worked on to natively support properties, and then that got carried over to C# for example. We added a similar feature there.

 **Have you encountered any major problems in the development of C#? Any catastrophes?**

No, I wouldn’t say that there have been any catastrophes! But life is nothing but little missteps and corrections along the way, so there are always little fires you’re putting out, but I wouldn’t say we ever had total meltdowns. It’s been a lot of fun to work on and it’s been over 10 years now.

 **Can you give me an example of a little fire that you’ve had to put out?**

Every project is about not what you put in, but what you don’t have time to put in! So it’s always about what we’re going to cut… so every project is like that. It’s so hard to single out anything in particular as we’re always putting out fires. New people leave the team and new people come in, it’s like every day you come to work and there’s something new to be dealt with.

-

-----

 **Would you do anything differently in developing C# if you had the chance?**

There are several things. First of all, when we shipped C# 1.0 we did not have generics in the language – that came in C# 2.0, and the minute we shipped generics we were able to put a lot of old code to bed as it was superfluous and not as strongly typed as generics. So a bunch of stuff got deprecated right out of the box in C#2.0. We knew generics were coming but it was one of those hard decisions: do you hold the platform longer or do you ship now and work on this and then ship it a couple of years later? I would have loved to have generics from the beginning as it would have left us with less obsolete stuff in the framework today.

With language design or with platform design 1.0 is always a unique opportunity to put down your core values, your core designs, and then with every version thereafter it’s much harder to fundamentally change the nature of the beast. And so, the things that you typically end up regretting later are the fundamentals that you didn’t quite get right. Because those you can’t change - you can always ship new libraries etc, but you can’t change the fundamental gestalt of the platform.

For example, in the type system we do not have separation between value and reference types and nullability of types. This may sound a little wonky or a little technical, but in C# reference types can be null, such as strings, but value types cannot be null. It sure would be nice to have had non-nullable reference types, so you could declare that ‘this string can never be null, and I want you compiler to check that I can never hit a null pointer here’.

50% of the bugs that people run into today, coding with C# in our platform, and the same is true of Java for that matter, are probably null reference exceptions. If we had had a stronger type system that would allow you to say that ‘this parameter may never be null, and you compiler please check that at every call, by doing static analysis of the code’. Then we could have stamped out classes of bugs.

But peppering that on after the fact once you’ve built a whole platform where this isn’t built in… it’s very hard to pepper on afterwards. Because if you start strengthening your APIs and saying that you can’t pass null here or null here or null here, then all of a sudden you’re starting to break a bunch of code. It may not be possible for the compiler to track it all properly.

Anyway, those are just things that are tough later. You sort of end up going, well ok, if we ever get another chance in umpteen years to build a new platform, we’ll definitely get this one right. Of course then we’ll go and make other mistakes! But we won’t make that one.

 **Why do you think C is such a popular language base, with many languages built on it such as C++ and C#?**

I think you have to take the historic view there first. If you go back to C itself, C was a very, very appropriate language for its time. It was really the language that lifted operating system builders out of assembly code and gave them higher level abstractions such as data types and so forth, yet was sufficiently close to the machine so that you could write efficient code. It was also very succinct: it was a very terse language, you can write very compact code which is something that programmers very much prefer. You compare a C program to a COBOL program and I can tell you where you’re going to see more characters.

So C was just an incredibly appropriate language for its time, and C++ was an incredibly appropriate evolution of C. Once you have huge language use, it is much easier to evolve and bring an existing base with you than it is to go create something brand new. If you look at the mechanics of new languages, when you design a new language you can either decide to evolve an existing language or start from scratch.

Evolving an existing language means you have an instantaneous big user base, and everything you add to the language is just gravy… there’s really no draw back as all of the old code still works. Start with a brand new language and you essentially start with minus 1,000 points. And now, you’ve got to win back your 1,000 points before we’re even talking. Lots of languages never get to more than minus 500. Yeah, they add value but they didn’t add enough value over what was there before. So C++ I think is a fantastic example of a very appropriate evolution of an existing language. It came right at the dawn of object oriented programming and pioneered that right into the core programming community, in a great way.

Of course by the time we started looking at C# as a new language, there was a huge, huge number of programmers out there that were very accustomed to programming with curly braces, like the C guys, C++ guys, Java guys etc etc. And so for us that was a very natural starting point: to make a language that would appeal to C++ programmers and to Java programmers. And that really meant build a language in the C heritage. And I think that has served us very, very well.

-

-----

 **What do you think of the upcoming language F#, which is touted as a fusion of a functional language and C#?**

I’m very enthusiastic about F# and the work that Don Syme from Microsoft Research in Cambridge is doing on this language. I wouldn’t say it’s a fusion of ML and C#. I mean, certainly its roots come from the ML base of functional programming languages, and it is closely related to Caml. I view it as a fusion of Caml and .NET, and a great impact of tooling experience.

 **Do you think that it’s ever going to become a large competitor to C#?**

I think they are both great and very complimentary. A competitor, yes, in the sense that VB is a competitor. But do you think of them as competitors? Or do you think of them as languages on a unified platform? I mean, I don’t personally: to me, the important thing is what’s built on top of .NET. Every language borrows from other languages, but that’s how we make progress in the industry and I’m interested in progress.

 **What do you think of functional programming in general?**

I think that functional programming is an incredibly interesting paradigm for us to look at, and certainly if you look at C# 3.0, functional programming has been a primary inspiration there, in all that we’ve done with LINQ and all of the primitive language features that it breaks down to. I think the time has finally come for functional programming to enter the mainstream. But, mainstream is different from taking over the world.

I definitely think that there is a space for functional programming today, and F# is unique in being the first industrial strength functional programming language with an industrial strength tooling language behind it, and an industrial strength platform underneath it. The thing that’s really unique about F# compared to all of the other functional programming languages is that it really offers first class support for object oriented programming as well, and first class interoperability with the .NET framework. Anything we have in the .NET framework is as easy to use from F# as it is from C# as it is from VB – it does not feel forced.

A lot of functional programming languages have lived in their own little world, and they’ve been pure and mathematical and so forth, but you couldn’t get to the big library that’s out there. If you look at languages today, they live and die by whether they have good framework support, as the frameworks are so big and so huge and so rich that you just cannot afford to ignore them anymore. And that’s why you’re seeing so many languages being built on top of .NET or on top of Java as opposed to being built in their own little worlds.

 **How do you feel about C# becoming standardized and adopted by Microsoft?**

If you’re asking from a personal perspective, I think it’s fantastic. I’ve been super fortunate to have Microsoft give me the opportunity to be the chief architect of a programming language and then have the company put its might behind it. That’s not an opportunity you get every day, and it’s been great.

With respect to standardization, I have always been a strong supporter of standardizing the language and I have always felt that you can’t have your cake and eat it too when it comes to expecting a language to be proprietary and also wanting community investment in the language. Be proprietary, but then just don’t expect people to build stuff on top of it. Or, you can open it up and people will feel more comfortable about investing.

Now, you can argue that we’re not obviously open source or anything, but the language is standardized, and the entire specification is available for anyone to go replicate. Mono has done so, and I think Mono is a fantastic thing. I don’t know [if] you’re familiar with Mono, but it’s an implementation of the C# standard and the CLI standard (which is effectively the .NET standard) on Linux, built as an open source project. And they’re doing great work and we talk to them a lot and I think it’s a super thing.

-

-----

 **And I guess they couldn’t have done that had you not put the specifications out there?**

Well, they could have but it would have been a heck of a lot harder and it would probably not be as good a product. You can go reverse engineer it… they have reverse engineered a lot of the .NET platform… but all of the core semantics of the language, they were part of the standardization process.

You know most recently we’ve created Silverlight, which is our browser hosted .NET runtime environment, and the Mono guys have built a project called Moonlight which is an implementation of Silverlight that is officially sanctioned by Microsoft that runs on Linux and other browsers. It’s a good thing.

 **So to focus more specifically on C#, why did you decide to introduce boxingunboxing into the language?**

I may have even touched on that a little bit earlier. What it boils down to is the fact that boxing allows you to unify the type system, and what I mean by unify is that when you are learning C# or approaching the language for the first time, you can make the simple statement that ‘in this language, everything is an object.’ Any piece of data you have you can treat as an object and assign it to a variable type object. The mechanism that makes that work is boxing and unboxing.

If you look at a similar language such as Java, it has a divided type system where everything is an object except ints and bools and characters etc which are not objects. So you have to sort of immediately dive in and describe the finer distinctions between these classes and types. Whereas when you have a unified type system you can just treat them as objects and then later, if you care, you can start diving into the deeper details about value types vs. reference types and what the mechanics are about and so forth.

We’ve seen this many times as people that teach the language have come back and said this is great as it allows us to have a very simple starting point. So from a pedagogical standpoint, it flows much better to first say that everything is an object and later we’ll teach you about the different kinds of objects that the system has.

 **Did you intend to make it easy to teach, or was that simply a side effect of the way the language was designed?**

I’d say we kept teachability in mind. It’s not just teachability that is an advantage of a unified type system, but it also allows your programs to have fewer special cases etc. I would say the motivator here was more conceptual simplicity. But conceptual simplicity is generally a great thing when it comes to teachability so the two kind of go hand in hand.

 **How do you feel about C#3.0? Were you happy with the release? When is the next release due out?**

Yes, I’m very happy with it, I think in some ways C# 3.0 was our first chance to truly do innovation and something brand new in the language. C# 1.0, if you think about it, was like ‘let’s go from zero to somewhere, so lets build all of the core things that a programming language has to have’. So, in a sense, ‘let’s build the 90% that is already known in the world out there’. C# 2.0 was about doing all of the things we wanted to do in C# 1.0 but we knew we weren’t going to have time to do. So C# 3.0 was the first chance of a green field: ok, what big problem are we going to attack here?

The problem we chose to attack was the mismatch between databases and general purpose programming languages, and the lack of queries and more declarative styles of programming in general purpose programming languages. It was a fantastic voyage, and it was so much fun to work on. The result has been quite unique and quite good really. LINQ is something that is a new thing.

 **Do you expect C#3.0 to become an ECMA and ISO standard, as previous versions have?**

We’re certainly open to that. There’s no ongoing work in the standards committee at the moment, but it’s really more a question of whether the community of industry partners out there would like to continue with that process. I should also say that the standards for C# explicitly do permit implementers to have extensions to the language, so though C# 3.0 is not standardized, it is certainly a complete implementation of the C# 2.0 standard. It is 100% backwards compatible, as all versions are.
-

-----

 **What functionality do you hope to add to C# in the future versions?**

There are many. I have a huge laundry list, or our team does, of features that people have requested over the years. If I had to name the 3 big trends that are going on in the industry that we take an interest in and get inspiration from, I would say the first is a move towards more declarative styles of programming, and you can sort of see LINQ as an example of that. All the talk we have about domain specific languages, that’s one form of declarative programming, and functional programming is another style of declarative programming. I think those are going to be quite important going forward and are certainly areas that we will invest in, in C#.

Dynamic programming is seeing a big resurgence these days, if you look at phenomena like Ruby and Ruby on Rails, these are all of a sudden very popular, and there are certain things you can do with dynamic programming languages that it would be great to also have in more classical languages like C#. So that’s something we’re also looking at.

Lastly, I would say that concurrency is the big thing that you can’t ignore these days because the mechanics of Moore’s law are such that it is no longer feasible to build more powerful processors. We can’t make them faster anymore because we can’t get rid of the heat, and so now all the acreage on the chips is being used to make more processors and all of a sudden it’s almost impossible to get a machine that doesn’t have multiple CPUs.

Right now you might have two cores but it’s only a matter of years before you have 4 or 8 or more than that, even in a standard desktop machine. In order for us to take advantage of that, we need much better programming models for concurrency. That’s a tough problem, it’s a problem that doesn’t just face us but the entire industry, and lots of people are thinking about it and we certainly are amongst those.

There’s no shortage of problems to solve!

 **Speaking of problems, how do you respond to criticism of C#, such as that the .NET platform only allows the language to run on Windows, as well as licensing and performance concerns?**

It is possible to build alternate implementations. We are not building .NET for Linux, because the value proposition that we can deliver to our customers is a complete unified and thoroughly tested package, from the OS framework to databases to Web servers etc. So .NET is part of a greater ecosystem, and all of these things work together. I think we are actually running on certain other platforms, such as Mono on Linux and other third party implementations. Silverlight now allows you to run .NET applications inside the browser and not just in our browser, but also in Safari on Macs for example.

As for performance concerns, I feel very comfortable about .NET performance compared to competitive platforms. I feel very good about it actually. There are performance issues here and there, as there is with anything, but I feel like we are always on a vigilant quest to make performance better and performance is pretty darn good. Performance is one of the key reasons that people choose .NET, certainly in the case studies I see and the customers I talk to (productivity being the other.)

 **What’s the most unusual/interesting program you’ve ever seen written in C#?**

Microsoft Research has this really cool application called `Worldwide Telescope <http://www.worldwidetelescope.org>`_ [18], which is written in C#. It’s effectively a beautiful interface on a catalogue of astronomical images (or images from astronomy) which allow you to do infinite zooming in on a planet and to see more and more detail. If you happen to choose planet Earth you can literally zoom in from galactic scale to your house, which is cool. I’ve been playing around with it with my kids and looking at other planets and they think it’s fun. It popularizes a thing that has traditionally been hard to get excited about.

-

-----

 **Do you always use the Visual C# compiler, or do you ever use versions developed by the Mono or DotGNU projects?**

I day to day use Visual Studio and Visual C# as that’s the environment I live in. I occasionally check out the Mono project or some of the other projects, but that’s more intellectual curiosity, rather than my day to day tool.

 **In your opinion, what lasting legacy has C# brought to Computer development?**

We all stand on the shoulders of giants here and every language builds on what went before it so we owe a lot to C, C++, Java, Delphi, all of these other things that came before us…we now hope to deliver our own incremental value.

I would say I’m very happy that C# definitely brought a big productivity boost to developers on the Windows platform and we continue to see that.

I think that C# is becoming one of the first widely adopted multi paradigm programming languages out there. With C# you can do object oriented programming, you can do procedural programming, now you can also do functional programming with a bunch of the extensions we’ve added in C# 3.0. We’re looking at C# 4.0 supporting dynamic programming and so we aim to harvest the best from all of these previously distinct language categories and deliver it all in a single language.

In terms of specific contributions, I think the work we’ve done in C# 3.0 on language integrated queries certainly seems to be inspiring lots of other languages out there. I’m very happy with that and I’m certainly hoping that in 10 years that there will be no languages where query isn’t just an automatic feature: it will be a feature that you must have. So I think we’ve certainly advanced the state of the art there.

 **Has the popularity of the language surprised you at all?**

It would have been presumptuous of me to say ‘so today we’re starting .NET and in 8 years we will own half of the world’s development’ or whatever. You can hope, but I have been pleasantly surprised.

Certainly we have labored hard to create a quality product, so it’s nice to see that we’re being rewarded with lots of usage. At the end of the day, that’s what keeps us going, knowing hundreds of thousands, if not millions of programmers use the stuff you work on day and you make their life better (hopefully!)

 **What are you working on now?**

I’m always working on the next release, so you can add one and deduce we’re working on C#4.0!

 **Do you have any idea when that release will be coming out?**

I don’t think we’re saying officially now, but we’re on a cadence of shipping every two years or so, or at least that’s what we hope to do. So 2010 sometime hopefully... there’s a set of features that we’re working on there that we’re actually going to talk about at the PDC (Professional Developers Conference) at the end of October. We’re giving some of the first presentations on what we’re doing.

 **Where do you think programming languages will be heading in the future, particularly in the next 5 to 20 years?**

I’ve been doing this now for 25 or almost 30 years, and I remember some early interviews that I gave after Turbo Pascal became very popular. People would always ask me where programming will be in 20 or so years (this is 1983 if you go back.) Of course, back then, the first thing out of one’s mouth was well ‘maybe we won’t even be programming at all and maybe we’ll actually just be telling computers what to do. If we’re doing any programming at all it’s likely to be visual and we’ll just be moving around software ICs and drawing lines and boxes’.

Lo and behold here we are 25 years later. We’re still programming in text and the programs look almost the same as they did 25 years ago. Yep, we’ve made a little bit of progress but it’s a lot slower than everyone expected.

-

-----

I’m going to be very cautious and not predict that we’re going to be telling computers what to do, but that it will look a lot like it does today, but that we’re going to be more productive, it’s hopefully going to be more succinct, we’re going to be able to say more with less code and we can be more declarative. We will hopefully have found good programming models for concurrency as that does seem to be an unavoidable trend.

Honestly, it’s anyone’s guess what it’s going to look like in the next 20 years, but certainly in the next 5 years those are the things that are going to be keeping us busy.

 **And do you have any advice for up-and-coming programmers?**I think it’s important to try to master the different paradigms of programs that are out there. The obvious object oriented programming is hopefully something that you will be taught in school. Hopefully school will also teach you functional programming, if not, that is a good thing to go look at.

Go look at dynamic languages and meta-programming: those are really interesting concepts. Once you get an understanding of these different kinds of programming and the philosophies that underlie them, you can get a much more coherent picture of what’s going on and the different styles of programming that might be more appropriate for you with what you’re doing right now.

Anyone programming today should check out functional programming and meta-programming as they are very important trends going forward.

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`haskell <haskell>` 

*  `http://en.wikipedia.org/wiki/Anders_Hejlsberg <http://en.wikipedia.org/wiki/Anders_Hejlsberg>`_ 

*  `http://en.wikipedia.org/wiki/Turbo_Pascal <http://en.wikipedia.org/wiki/Turbo_Pascal>`_ 

*  `http://en.wikipedia.org/wiki/Delphi_programming_language <http://en.wikipedia.org/wiki/Delphi_programming_language>`_ 

*  `http://www.worldwidetelescope.org <http://www.worldwidetelescope.org>`_ 

.. _d:

The A-Z of Programming Languages: D
===================================
Walter Bright talks about D and his desire to improve on systems programming languages.
* Naomi Hamilton(Computerworld)

* 22 July, 2008 08:07

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_7687.jpg
   :alt: Walter Bright

Walter Bright

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`ada <ada>` [2]on the Ada 1995 and 2005 revisions, Microsoft about its server-side script engine :ref:`asp <asp>` [3], :ref:`bash <bash>` [4]about his experience maintaining Bash, :ref:`c++ <c++>` [5]of C++ fame, to :ref:`forth <forth>` [6]about the design and development of Forth, a chat with the irreverent :ref:`intercal <intercal>` [7]about the development and uses of INTERCAL, Stephen C. Johnson on YACC and most recently Modula-3 design committee member, :ref:`modula-3 <modula-3>` [8].

In this interview, we chat to Walter Bright about D and his desire to improve on systems programming languages. According to his home page, Bright was trained as a mechanical engineer, and has worked for Boeing on the 757 stabilizer trim system. Ever since this experience however, he has been writing software, and has a particular interest in compilers.

Please note that we are no longer following exact alphabetical order for this series, due to popular demand. If you'd like to submit any suggestions for programming languages or language authors that you'd like to see covered, please emailnaomi@computerworld.com.au.

 **What prompted the development of D?**

Being a compiler developer, there's always at the back of my mind the impetus for applying what I know to design a better language. At my core I'm an engineer, and never can look at anything without thinking of ways to improve it.

The tipping point came in 1999 when I left Symantec and found myself at a crossroads. It was the perfect opportunity to put into practice what I'd been thinking about for many years.

 **Was there a particular problem you were trying to solve?**

There was no specific problem. I'd been writing code in C++ for 12 years, and had written a successful C++ compiler. This gave me a fairly intimate knowledge of how the language worked and where the problems were.

C++ was (and is) limited by the requirement of legacy compatibility, and I thought much could be done if that requirement was set aside. We could have the power of C++ with the hindsight to make it beautiful.

I had also been programming in other languages, which had a lot to contribute.

 **How did the name D come about?**

It started out as the Mars programming language (as the company name is Digital Mars). But my friends and colleagues kept calling it D, as it started out as a re-engineering of C++, and eventually the name stuck.

 **Why did you feel that C++ needed re-engineering?**

A lot has been learned about programming since C++ was developed. Much of this has been folded in C++ as layers on top of the existing structure, to maintain backwards compatibility. It's like a farmhouse that has been added on to by successive generations, each trying to modernize it and adapting it to their particular needs. At some point, with a redesign, you can achieve what is needed directly.

But D today has moved well beyond that. Many successful concepts from other languages like JavaScript, PERL, Ruby, Lisp, Ada, Erlang, Python, etc., have had a significant influence on D.

 **What elements of C++ have you kept, and what elements have you deliberately discarded?**

D keeps enough so that a C++ programmer would feel immediately comfortable programming in D. Obsolete technologies like the preprocessor have been replaced with modern systems, such as modules. Probably the central thing that has been kept is the idea that D is a systems programming language, and the language always respects that ultimately the programmer knows best.
-

-----

 **Would you do anything differently in the development of D if you had the chance?**

I'd be much quicker to turn more of the responsibility for the work over to the community. Letting go of things is hard for me and something I need to do a lot better job of.

 **What is the most interesting program that you've seen written with D?**

Don Clugston wrote a fascinating program that was actually able to generate floating point code and then execute it. He discusses it in this presentation: `http://video.google.com/videoplay?docid=1440222849043528221 <http://video.google.com/videoplay?docid=1440222849043528221&hl=en>`_ =en[9].

 **What sort of feedback have you received from the experimental version of D, or D 2.0, released in June 2007?**

D 1.0 was pretty straightforward stuff, being features that were adapted from well-trod experience in other languages. D 2.0 has ventured into unexplored territory that doesn't have a track record in other languages. Since these capabilities are unproven, they generate some healthy scepticism. Only time will tell.

 **Have you ever seen the language used in a way that was not originally intended? If so, what was it? And did it or didn't it work?**

There have been too many to list them all, but a couple examples are Don Clugston and Andrei Alexandrescu. They never cease to amaze me with how they use D. They often push the language beyond its limits, which turns into some powerful motivation to extend those limits and make it work. Don's presentation in the afore-mentioned video is a good example. You can see a glimpse of Andrei's work in, for example, the algorithms library at `http://www.digitalmars.com/d/2.0/phobos/std_algorithm.html <http://www.digitalmars.com/d/2.0/phobos/std_algorithm.html>`_ [10].

 **Do you still consider D to be a language "under development"?**

A language that is not under development is a language that is not being used. D is under development, and will stay that way as long as people use it. C++, Java, Python, PERL, etc., are also widely used and are still under development.

 **Are changes still being made to the language or are you focusing on removing bugs right now?**

I spend about half of my efforts fixing bugs and supporting existing releases, and the other half working on the future design of D 2.0.

 **Do you agree that the lack of support from many IDEs currently is a problem for the languages popularity right now?**

There are many editors and IDEs that support D now: `http://www.prowiki.org/wiki4d/wiki.cgi?EditorSupport <http://www.prowiki.org/wiki4d/wiki.cgi?EditorSupport>`_ [11]-

-----

 **How do you react to criticism such as the comment below, taken from Wikipedia? "The standard library in D is called Phobos. Some members of the D community think Phobos is too simplistic and that it has numerous quirks and other issues, and a replacement of the library called Tango was written. However, Tango and Phobos are at the moment incompatible due to different run-time libraries (the garbage collector, threading support, etc). The existence of two libraries, both widely in use, could lead to significant problems where some packages use Phobos and others use Tango."**

It's a valid criticism. We're working with the Tango team to erase the compatibility issues between them, so that a user can mix and match what they need from both libraries.

 **In your opinion, what lasting legacy has D brought to computer development?**

D demonstrates that it is possible to build a powerful programming language that is both easy to use and generates fast code. I expect we'll see a lot of D's pioneering features and feature combinations appearing in other languages.

 **Where do you envisage D's future lying?**

D will be the first choice of languages for systems and applications work that require high performance along with high programmer productivity.

 **Where do you see computer programming languages heading in the future, particularly in the next 5 to 20 years?**

The revolution coming is large numbers of cores available in the CPUs. That means programming languages will have to adapt to make it much easier to take advantage of those cores. Andrei's presentation  gives a taste of what's to come.

 **Do you have any advice for up-and-coming programmers?**

Ignore all the people who tell you it can't be done. Telling you it can't be done means you're on the right track.

 **Is there anything else you'd like to add?**

Yes. D isn't just my effort. An incredible community has grown up around it and contribute daily to it. Three books are out on D now and more are on the way. The community has created and released powerful libraries, debuggers, and IDEs for D. Another D compiler has been created to work with gcc, called gdc, and a third is being developed for use with LLVM. Proposals for new language features appear almost daily. D has an embarrassment of riches in the people contributing to it.

Oh, and I'm having a great time.

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`modula-3 <modula-3>` 

*  `http://video.google.com/videoplay?docid=1440222849043528221 <http://video.google.com/videoplay?docid=1440222849043528221&hl=en>`_ hl=en

*  `http://www.digitalmars.com/d/2.0/phobos/std_algorithm.html <http://www.digitalmars.com/d/2.0/phobos/std_algorithm.html>`_ 

*  `http://www.prowiki.org/wiki4d/wiki.cgi?EditorSupport <http://www.prowiki.org/wiki4d/wiki.cgi?EditorSupport>`_ 

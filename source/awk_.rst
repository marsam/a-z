.. _awk_:

An inteview with Brian Kernighan, co-developer of AWK and AMPL
==============================================================
Computerworld's series on the most popular programming languages continues as we chat to Brian Kernighan
* Trevor Clarke(Computerworld)

* 06 October, 2009 12:54
 *Computerworld*is undertaking a series of investigations into interesting programming languages. In the past we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8]and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`falcon <falcon>` [13]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [14], Walter Bright :ref:`d <d>` [15], Brendan Eich on :ref:`javascript <javascript>` [16], Anders Hejlsberg on :ref:`c# <c#>` [17], Guido van Rossum on :ref:`python <python>` [18], Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [19], John Ousterhout on :ref:`tcl <tcl>` [20], Joe Armstrong on Erlang and Rich Hickey on Clojure. We recently spoke to Martin Odersky about the darling of Web 2.0 start-ups and big corporates alike, Scala.

More recently, we heard from `Groovy <http://groovy.codehaus.org/>`_ [21]Project Manager, Guillaume Laforge. He told us the development story behind the language and why he thinks it is grooving its way into enterprises around the world.

This time we spoke with Brian Kernighan — a figure who helped popularise C with his book, co-written with the creator Dennis Ritchie, *The C Programming Language*and contributed to the development of :ref:`awk <awk>` [4]and AMPL. *Want to see a programming icon interviewed?Email Computerworldor follow `@computerworldau <http://www.twitter.com/computerworldau>`_ [22]on Twitter and let us know.*----

 **You maintain you had no part in the birth of C, but do you think the language would have been as successful as it has been without the book?**

 **Brian Kernighan:**The word is not ‘maintained’; it's ‘stated accurately’.  C is entirely Dennis Ritchie's work.  C would have done just fine on its own, since as a language it achieved a perfect balance among efficiency, expressiveness, and power. The book probably helped, though I think more in spreading the language early on than in its ultimate acceptance. Of course, it helped enormously to have Dennis as co-author, for his expertise and his writing.

 **In the ten years since you launched *The Practice of Programming*, a separate book written with Rob Pike, has the way programmers operate changed enough for you to consider amending any parts of the publication?**

Programming today depends more and more on combining large building blocks and less on detailed logic of little things, though there's certainly enough of that as well. A typical programmer today spends a lot of time just trying to figure out what methods to call from some giant package and probably needs some kind of IDE like Eclipse or XCode to fill in the gaps. There are more languages in regular use and programs are often distributed combinations of multiple languages. All of these facts complicate life, though it's possible to build quite amazing systems quickly when everything goes right. I think that the advice on detailed topics in *The Practice of Programming*is sound and will always be — one has to find the right algorithms and data structures, one has to test and debug and worry about performance, and there are general issues like good notation that will always make life much better. But it's not clear to me or to Rob that we have enough new good ideas for a new book, at least at the moment.

 **What advice do you have for young programmers starting out? Would you recommend a grounding in Cobol like you had, for example?**

Every language teaches you something, so learning a language is never wasted, especially if it's different in more than just syntactic trivia. One of Alan Perlis's many wise and witty epigrams says, "A language that doesn't affect the way you think about programming is not worth knowing". On the other hand, I would not suggest Cobol as a primary focus for most people today — I learned it as part of a summer job and long ago, not because it taught me something new (though it did that as well). No matter what, the way to learn to program is to write code, and rewrite it, and see it used, and rewrite again. Reading other people's code is invaluable as well. Of course all of these assume that the code is good; I don't see a lot of benefit in reading a lot of bad code, other than to learn what to avoid, and one should, of course, not write bad code oneself. That's easier said than done, which is why I stress rewriting.

-

-----

 **Who would you consider to be the icons of the programming world?**

For purely parochial reasons, I think of people who I know or whose work I know well.  Ken Thompson and Dennis Ritchie changed my life and yours; we would not be having this conversation without them. People who created major languages would also fall into that camp, for instance we all regularly use languages created by :ref:`c++ <c++>` [8], `James Gosling <http://www.computerworld.com.au/article/25910/interview_gosling_java_tools_his_move_mac/>`_ [23], :ref:`perl <perl>` [1], and :ref:`python <python>` [18]. And of course there are super-icons like Don Knuth and Fred Brooks. But this is a personal list; there are many others whose work has been influential, and your list would surely differ.

Every language teaches you something, so learning a language is never wasted, especially if it's different in more than just syntactic trivia.(Brian Kernighan)

 **Bell Labs has produced some of the most influential figures in the world as far as IT goes — does it still maintain its relevance in your view? What could it do to better its acclaimed past?**

Bell Labs was an astonishing place for many decades, though it fell on somewhat hard times during the telecom meltdown some years ago, as its corporate owner had to cope with shrinking markets. There are great people at Bell Labs but the operation is much smaller than it used to be, which reduces the chance of a big impact, though certainly it can still happen — all it takes is one or two people with a good idea.

 **What are you working on at the moment? Can we expect any new books or work on languages?**

I seem to get totally wrapped up in teaching and working with students during the school year. During the summer I try to spend time in the real world, writing code for therapy and perhaps for some useful purpose. This is fun but so far it hasn't led to any book, though ideas are percolating. I'm still very interested in domain-specific languages and, more generally, in tools that make it easier to write code. And it sometimes seems like some of the old Unix command line languages for special purposes might have a second life in web pages. So I play with these from time to time, or entice some student into exploring a half-baked idea for a semester.

-

-----

 **You've been around the development of some of the formative influences on the Internet such as UNIX, what do you see as the driving influences of contemporary computing and the way the world connects?**

For better or worse, the driving influence today seems to be to get something up and running and used via the Internet, as quickly as possible. A good idea, however simple in retrospect, can give one fame and fortune (witness Google, Facebook, Twitter, and any number of others). But this only works because there is infrastructure: open source software like Unix/Linux and GNU tools and web libraries, dirt-cheap hardware, and essentially free communications. We're seeing an increase in scalable systems as well, like Amazon's web services, where one can start very small and grow rapidly and without real limits as the need arises. It's starting to look like the Multics idea of an information utility.

 **AWK and AMPL languages are two you have been involved in developing. Are there any languages you would have liked to have helped develop?**

Well, it's always nice to have been part of a successful project, so naturally I would like to have helped with everything good. But I've been quite lucky in the handful that I was involved in. Most of that comes from having first-rate collaborators ( :ref:`awk <awk>` [4]and Peter Weinberger for AWK and Bob Fourer and Dave Gay for AMPL).

 **Which companies/individuals would you point to as doing great things for the society at present through computer sciences?**

I might single out Bill and Melinda Gates for their foundation, made possible by the great success of Microsoft. Their charitable work is aimed at tough but potentially solvable problems and operates on a scale that few others can approach. After that, one might name Google, which has made so much information so readily accessible; that access has changed the world greatly and is likely to continue to do so.

 **What are you views on the following languages: Perl, Java, and Ruby?**

I use Java some; it's the standard language for introductory computing at Princeton and lots of other places. I find it bulky and verbose but it flows pretty smoothly once I get going. I don't use Perl much at this point — it's been replaced by Python in my personal working set — but no other language matches the amount of computation that can be packed into so few characters. I have not written much Ruby; it clearly has a lot of appeal and some intriguing ideas, but so far when I have to write a program quickly some other more familiar language gets used just to get the job done. But one of these days, I'll add Ruby to the list.

 ** *Want to see a programming icon interviewed?Email Computerworldor follow `@computerworldau <http://www.twitter.com/computerworldau>`_ [22]on Twitter and let us know.***

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`tcl <tcl>` 

*  `http://groovy.codehaus.org/ <http://groovy.codehaus.org/>`_ 

*  `http://www.twitter.com/computerworldau <http://www.twitter.com/computerworldau>`_ 

*  `http://www.computerworld.com.auhttp://www.computerworld.com.au/article/25910/interview_gosling_java_tools_his_move_mac/ <http://www.computerworld.com.au/article/25910/interview_gosling_java_tools_his_move_mac/>`_ 

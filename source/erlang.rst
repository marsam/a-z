.. _erlang:

The A-Z of Programming Languages: Erlang
========================================
Our series on the most popular programming languages continues as we chat to Erlang creator Joe Armstrong
* Kathryn Edwards(Computerworld)

* 16 June, 2009 15:03

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_17628.jpg
   :alt: Joe Armstrong

Joe Armstrong

 *Computerworld* is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8], and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`falcon <falcon>` [13]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [14], Walter Bright :ref:`d <d>` [15], Brendan Eich on :ref:`javascript <javascript>` [16], Anders Hejlsberg on :ref:`c# <c#>` [17], Guido van Rossum on :ref:`python <python>` [18]and Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [19]. We most recently spoke to :ref:`tcl <tcl>` [20].

In this interview `Erlang <http://erlang.org/>`_ [21]creator Joe Armstrong, took some time to tell *Computerworld*about Erlang's development over the past 20 years, and what's in store for the language in the future.

 *If you wish to submit any suggestions for programming languages or language authors you would like to see covered, please emailkathryn@computerworld.com.au*----

 **What's behind the name Erlang?**

Either it's short for "Ericsson Language" or it's named after the Danish mathematician Agner Krarup Erlang.  We have never revealed which of these is true, so you'll have to keep guessing!

 **What prompted its creation?**

It was an accident. There was never a project "to create a new programming language". There was an `Ericsson <http://www.computerworld.com.au/tag/Sony%20Ericsson>`_ [22]research project "to find better ways of programming telephony applications" and  Erlang was the result.

 **Was there a particular problem the language aimed to solve?**

Yes, we wanted to write a control program for a small `telephone exchange <http://www.computerworld.com.au/tag/video%20telepresence>`_ [23]in the best possible manner. A lot of the properties of Erlang can be traced back to this problem. Telephone exchanges should never stop, so we have to be able to upgrade code without stopping the system.

The application should never fail disastrously so we need to developed sophisticated strategies for dealing with software and hardware errors during run-time.

 **Why was Erlang released as `open source <http://www.computerworld.com.au/tag/open%20source>`_ [24]? What's the current version of open source Erlang?**

To stimulate the spread of Erlang outside Ericsson. The current version is release 13 - so it's pretty mature. We release about two new versions per year.

 **What's the `Erlang eco-system <http://www.computerworld.com.au/index.php?search=erlang&op.x=40&op.y=10&op=SEARCH>`_ [25]like?**

There's a very active mailing list where we have a lot of discussions about architectures and applications and help solve beginners problems.

Currently there are several conferences which are dedicated to Erlang. The oldest is the Erlang User Conference that runs once a year in Stockholm. The ACM Functional Programming Conference has had an "Erlang day" for the last few years and last year the "Erlang Factory" started.

The "Erlang Factory" runs twice a year. The last conference was in Palo Alto and the next one will be in London. These conferences are explosions of enthusiasm. They are become the meeting place for people who want to build large scale systems that never stop.

It's difficult to get overall picture. Erlang is best suited for writing fault-tolerant servers. These are things that are not particularly visible to the end-user. If you have a desktop application it's pretty easy to find out how its been implement. But for a server this is much more difficult. The only way to talk to a server is through an agreed protocol, so you have no idea how the server has been implemented.
-

----- **What's the most interesting program(s) you've seen written with Erlang for business?**

That's difficult to answer, there are many good applications.

Possibly `Ejabberd <http://www.ejabberd.im/>`_ [26]which is an open-source Jabber/XMPP instant messaging server. Ejabberd appears to be the market leading XMPP server and things like `Google Wave <http://www.techworld.com.au/article/305043/aussie_devs_make_wave_google_web_toolkit/>`_ [27]which runs on top of XMPP will probably attract a lot of people into building applications on XMPP servers.

Another candidate might be `Rabbit MQ <http://www.rabbitmq.com/>`_ [28]which is an open-source implementation of the AMQP protocol. This provides reliable persistent messaging in a language-neutral manner. Building systems without shared memory and based on pure message passing is really the only way to make scalable and reliable systems. So AMQP fits nicely with the Erlang view of the world.

 **How flexible is the language, how does it stand aside the popular programming languages for general applications?**

Difficult to say. What we lose in sequential performance we win back in parallel performance. To fully utilize a multicore or `cloud infrastructure <http://www.computerworld.com.au/tag/cloud%20computing>`_ [29]your program must exploit parallelism. A sequential program just won't run faster on a multicore, in fact as time goes on it will run slower since clock speeds will drop in the future to save power. The trend is towards more and slower cores. The ease of writing parallel program is thus essential to performance.

In the Erlang world we have over twenty years of experience with designing and implementing parallel algorithms. What we lose in sequential processing speed we win back in parallel performance and fault-tolerance.

 **Have you ever seen the language used in a way that wasn't originally intended?**

Lots of times...

 **What limits does Erlang have?**

You have to distinguish the language from the implementation here. The implementation has various limits, like there is an upper limit on the maximum number of processes you can create, this is very large but is still a limit.

Somewhere in the next 10 to 20 years time we might have a million cores per chip and Petabyte memories and will discover that "hey - we can't address all this stuff" so we'll have to change the implementation - but the language will be the same.

We might discover that massive programs running in "the cloud" will need new as yet unthought of mechanism, so we might need to change the language.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of the language?**

Yes. An engineers job is to solve problems. That's why I'm an engineer. If the problems weren't difficult they would be no point in doing the job [but] 95 percent of the time the problems are in a state of "not being solved" which is frustrating. Frustration goes hand-in-hand with creativity - if you weren't frustrated with how things worked you would see no need to invent new things.

 **What is the third-party library availability like?**

Patchy. In some areas it's absolutely brilliant, in others on-existent. This is a chicken and egg situation. Without a lot of active developers there won't be many third-party libraries, and without a lot of libraries we won't attract the developers.

What's happening is that a lot of early-adopters are learning Erlang and using it for things that we hadn't imagined. So we're seeing things like `CouchDB <http://couchdb.apache.org/>`_ [30](a database) and `MochiWeb <http://code.google.com/p/mochiweb/>`_ [31](a Web server) which you can use to build applications.
-

----- **Programming languages are leveraging more and more threading due to multicore processors. Will this push the development of Erlang?**

Very much so. We've been doing parallel programming since 1986 and now we have real parallel hardware to run our programs on, so our theories are turning into reality. We know how to write parallel programs, we know how to deploy them on multicores. We know how to debug our parallel programs. We have a head start here.

What we don't know is the best way to get optimal performance from a multicore so we're doing a lot of tweaking behind the scenes.

The good news for the Erlang programmer is that they can more or less ignore most of the problems of multicore programming. They just write Erlang code and the Erlang run-time system will try and spread the execution over the available cores in an optimal manner.

As each new version of Erlang is released we hope to improve the mapping onto multicores. This is all highly dynamic, we don't know what multicore architectures will win in the future. Are we going to see small numbers of complex cores or large numbers of simple cores with a "network on chip" architecture (as in the Tilera chips, or the `Intel <http://www.computerworld.com.au/article/210888/intel_exec_programming_multicore_chips_challenge/>`_ [32]Polaris chip). We just don't know.

But whatever happens Erlang will be there adapting to the latest chipsets.

 **Did you see this trend coming in the early days of its development?**

No. We always said "one day everything will be parallel" - but the multi-core stuff sneaked up when we weren't watching. I guess the hardware guys knew about this in advance but the speed with which the change came was a bit of a surprise. Suddenly my laptop had a dual-core and a quad-core appeared on my desktop.

And wow - when the dual core came some of my Erlang program just went twice as fast with no changes. Other programs didn't go twice as fast. So the reasons why the program didn't go twice as fast suddenly became a really interesting problem.

 **What are the advantages of hot swapping?**

You're joking. In my world we want to build systems that are started once and thereafter never stop. They evolve with time. Stopping a system to upgrade the code is an admission of failure.

Erlang takes care of a lot of the the nitty-gritty details needed to hot-swap code in an application. It doesn't entirely solve the problem, since you have to be pretty careful if you change code as you run it, but the in-build mechanisms in Erlang make this a tractable problem.

 **Functional versus Imperative? What can you tell us?**

It's the next step in programming. Functional programs to a large extent behave like the maths we learned in school.

Functional programming is good in the sense that it eliminates whole classes of errors that can occur in imperative programs. In pure functional programs there is no mutable data and side effects are prohibited. Since you can't mutate data you don't need locks to lock the data while it is being mutated, since there is no mutation. This enables concurrency, all the arguments to any function can be evaluated in parallel if needed.

 **Interpreted versus compiled? Why those options?**

I think the distinction is artificial. Erlang is compiled to abstract machine code, which is then interpreted. The abstract machine code can be native code compiled if necessary. This is just same philosophy as used in the JVM and .Net.

Whether or not to interpret or compile the code is a pure engineering question. It depends upon the performance, memory-size, portability etc. requirements we have. As far as the user is concerned their is no difference. Sometimes compiled code is faster than interpreted code, other times it is slower.

 **Looking back, is there anything you would change in the language's development?**

Removing stuff turns out to be painfully difficult. It's really easy to add features to a language, but almost impossibly difficult to remove things. In the early days we would happily add things to the language and remove them if they were a bad idea. Now removing things is almost impossible.

The main problem here is testing, we have systems with literally millions of lines of code and testing them takes a long time, so we can only make backwards compatible changes.

Some things we added to the language were with hindsight not so brilliant. I'd happily remove macros, include files, and the way we handle records. I'd also add mechanism to allow the language itself to evolve.

We have mechanisms that allow the application software to evolve, but not the language and libraries itself. We need mechanisms for revision control as part of the language itself. But I don't know how to do this. I've been thinking about this for a long time.

Instead of having external revision control systems like Git or Subversion I'd like to see revision control and re-factoring built into the language itself with fine-grain mechanism for introspection and version control.

 **Will computer science students finally have to learn about dining philosophers?!**

Easy - give 'em more forks.

 **Finally, where do you envisage Erlang's future lying?**

I don't know. Erlang destiny seems to be to influence the design of future programming languages. Several new programming language have adopted the Erlang way of thinking about concurrency, but they haven't followed up on fault-tolerance and dynamic code-change mechanisms.

As we move into cloud computing and massively multicores life becomes interesting. How do we program large assemblies of parallel processes? Nobody really knows. Exactly what is a cloud? Again nobody knows.

I think as systems evolve Erlang will be there someplace as we figure out how to program massively fault tolerant systems.

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`tcl <tcl>` 

*  `http://erlang.org/ <http://erlang.org/>`_ 

*  `http://www.computerworld.com.au/tag/Sony%20Ericsson <http://www.computerworld.com.au/tag/Sony%20Ericsson>`_ 

*  `http://www.computerworld.com.au/tag/video%20telepresence <http://www.computerworld.com.au/tag/video%20telepresence>`_ 

*  `http://www.computerworld.com.au/tag/open%20source <http://www.computerworld.com.au/tag/open%20source>`_ 

*  `http://www.computerworld.com.au/index.php?search=erlang <http://www.computerworld.com.au/index.php?search=erlang&op.x=40&op.y=10&op=SEARCH>`_ op.x=40op.y=10op=SEARCH

*  `http://www.ejabberd.im/ <http://www.ejabberd.im/>`_ 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/305043/aussie_devs_make_wave_google_web_toolkit/ <http://www.techworld.com.au/article/305043/aussie_devs_make_wave_google_web_toolkit/>`_ 

*  `http://www.rabbitmq.com/ <http://www.rabbitmq.com/>`_ 

*  `http://www.computerworld.com.au/tag/cloud%20computing <http://www.computerworld.com.au/tag/cloud%20computing>`_ 

*  `http://couchdb.apache.org/ <http://couchdb.apache.org/>`_ 

*  `http://code.google.com/p/mochiweb/ <http://code.google.com/p/mochiweb/>`_ 

*  `http://www.computerworld.com.auhttp://www.computerworld.com.au/article/210888/intel_exec_programming_multicore_chips_challenge/ <http://www.computerworld.com.au/article/210888/intel_exec_programming_multicore_chips_challenge/>`_ 

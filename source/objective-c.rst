.. _objective-c:

The A to Z of programming languages: Objective-C
================================================
Our series on the most popular programming languages continues as we chat to Objective-C co-creator Brad Cox
* James Hutchinson(Computerworld)

* 17 June, 2010 10:34
 *Computerworld Australia*is undertaking a series of investigations into the most widely-used programming languages. We most recently spoke to :ref:`matlab <matlab>` [1]creator, Cleve Moler, on the 25th anniversary of The MathWorks. Check out `The A to Z of programming languages index <http://www.computerworld.com.au/article/344826/z_programming_languages/>`_ [2]to find them all.

In this interview, we take a look at one of the most in vogue programming languages at the moment: Objective-C. Acquired by Steve Jobs' company NeXT in 1995, the language now underpins both Apple's Mac OS X and the iOS platform. Thanks to the popularity of the iPhone, iPad and the App Store, the language has become an essential part of creating and delivering mobile apps to the masses.

Here, we talk to the language's co-creator, `Brad Cox <http://bradjcox.blogspot.com>`_ [3], on object-oriented programming, the difference with C++ and why the programming language ultimately doesn't matter.

 **Can you give us a brief rundown of your history, and programming experience, both pre- and post-Objective-C?**

After graduate school (mathematical biology), I realised I wasn’t cut out for academia and took two starter jobs building gold-plated newsroom automation systems (Toronto Star and Chicago Tribune). That got me into C and Unix. Then I joined the ITT advanced programming labs with Tom Love.

 **Can you provide a brief timeline of how and when Objective-C came about?**

I started Objective-C’s ancestor when at the ITT Research Laboratory just after the Byte Magazine Smalltalk-80 article came out [August 1981]. That was called *OOPC*: Object-oriented Preprocessor because it was originally a quick lash-up of ordinary Unix tools like sed, awk, C compilers, etc.

Soon afterwards Tom and I left to join Schlumberger Research Labs but left after about two years to found Productivity Products International; Stepstone’s ancestor. I started work immediately on a proper pre-compiler based on yacc/lex tools and got that working about six months later; as I recall around 1982.

About that time Bjarne [Stroustrup] heard about our work and invited me to speak at Bell Labs, which was when I learned he was working on C++. Entirely different notions of what object-oriented meant. He wanted a better C (silicon fab line). I wanted a better way of soldering together components originally fabricated in C to build larger-scale assemblies.

 **Objective-C has obviously become a major language thanks to popular Apple platforms and a thriving third-party developer community. Did you ever think it would become a widely known language in the sense that it has?**

Not really. I was never particularly focused on Objective-C as a language, just as circuit engineers aren’t particularly interested in soldering irons. My interest has always been in software components, not the tools for building them.

 **What caused you and your partner in crime, Tom Love, to invent the language in the first place? Was it a reaction to C++ or C?**

C++ didn’t exist when we started. It was a reaction to C not C++, and to limitations of C for building reusable components.

The only encapsulation tools C provides are macros and functions as building blocks for applications. Objective-C added objects originally and packages followed thereafter. I added lightweight threads (mini-applications) as a support library called Taskmaster.

It never occurred to us to add something comparable to SOA objects because networking was so new in those days. And we never thought of something comparable to OSGI since we tried (too hard I think now) to stay away from the C linker.

-

-----

 **Did you have any previous expertise in Smalltalk before inventing the language?**

No. Everything I know came from the Byte magazine article and interacting with its developers. I’d known Adele Goldberg at U. of Chicago.

 **What was the general feeling for object-oriented programming amongst developers at the time?**

Object-oriented programming was largely unknown at the time outside of research labs. I built Objective-C to take OOP “to the factory floor”.

 **Do you think you were successful in that?**

Certainly.

 **Do you have any regrets about maintaining the language i.e., any problems you didn't resolve in the language you wish you had?**

No. Lack of garbage collection was a known issue from the beginning, but an inescapable one without sacrificing the reasons people choose C in the first place.

 **Was there any apprehension to selling the rights to Objective-C to Steve Jobs' NeXT in 1995?**

Not really.

 **Do you think Objective-C would still be around today if it weren't for NeXT's acquisition?**

Probably not.

 **How do you feel Objective-C 2.0 added to your original language? Can you still call it yours in any sense?**

I’ve never thought of as “mine” in any sense, certainly not emotionally. Its a soldering gun. And lots of other people were involved in building it, not just me.

 **Have you played any significant part Objective-C's evolution since the acquisition?**

Not really. I’ve moved on to larger granularity components, particularly SOA and OSGI.

-

-----

 **Many have drawn similarities between Objective-C and later languages like Java and Flash's ActionScript. Have you seen any direct link between these languages, or can you attribute the similarities to something else?**

As I understand, Java history and interfaces were motivated by Objective-C protocols. But I didn’t invent those either, that was Steve Naroff’s contribution. And I didn’t invent the rest of it either; I took everything I could get from Smalltalk.

 **You say that you aim to make software a true engineering discipline. Some in the industry would say that it is already the case; what do you see as the reason why it might not be, and what would rectify this?**

Software is engineering in exactly the sense that primitive mud huts are. When everything is fabricated from whatever mud is around that particular construction site, there is nothing repeatable to build an engineering science around since everything is unique and nothing can be trusted.

Making software an engineering discipline involves adopting real brick construction, building by assembling trusted components. That’s precisely why I’ve been chasing components of various granularities; first objects with Objective-C, then SOA services, and most recently OSGI.

 **The divide between technical knowledge, one's programming work and the way they think about the society around the probably isn't one many programmers cross regularly. Would you attribute this to the granularity of inventing a programming language, or a general interest in these concepts to begin with?**

Programming *is*social and organisational work (apart from solitary hacking). Programmers produce components (of various granularities) for other people to use. My interests have never been in the tools for doing that (languages), but in incentive structures for encouraging that for a new kind of goods made of bits instead of atoms.

 **What do you see as the future of programming languages and the developer community? Are there any big holes to fill like object-oriented programming in modern languages, including Objective-C?**

Using programming languages is like mud brick architecture. The future of mud brick architecture isn’t better mud mixers (programming language). It is moving to real bricks, i.e. tested, certified, trusted components. That’s starting to happen, particularly at Apple, which is possibly why they were drawn to Objective-C in the first place. For example, iPhone development involves very little construction of new components, just assembling Apple components off-the-shelf. Similar trends are underway in DoD in connection with SOA components.

References
*  :ref:`matlab <matlab>` 

*  `http://www.computerworld.com.auhttp://www.computerworld.com.au/article/344826/z_programming_languages/ <http://www.computerworld.com.au/article/344826/z_programming_languages/>`_ 

*  `http://bradjcox.blogspot.com <http://bradjcox.blogspot.com>`_ 

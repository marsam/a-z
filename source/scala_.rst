.. _scala_:

The A-Z of programming languages: From Pizza to Scala
=====================================================
How Martin Odersky created the programming language behind Twitter
* Lisa Banks(Techworld Australia)

* 18 August, 2011 11:00
 *Techworld Australia*recently caught up with Martin Odersky to talk about the creation of Scala; the programming language created in a Web 1.0 world but ended up being used to run Twitter.

 *Techworld Australia*is undertaking a series of investigations into some of the most widely used programming languages — as well as some of the more obscure ones. Previously, we’ve spoken to Brad Cox, the man behind :ref:`objective-c <objective-c>` [1], Python creator :ref:`python <python>` [2], :ref:`awk <awk>` [3], Chet Ramey about his :ref:`bash <bash>` [4], :ref:`matlab <matlab>` [5], and :ref:`c++ <c++>` [6].

Be sure to check out all the interviews in our `A to Z of programming languages index <http://www.computerworld.com.au/tag/a-z_of_programming_languages/>`_ [7].

 **What led you to develop Scala?**

I was in programming languages almost all of my professional life. Since I was always fond of writing compilers, I did my PhD in Zurich with Niklaus Wirth, the designer of Pascal and Modula-2, in the 80s. I then got very interested in functional programming and thought it was the way forward.

When Java came out in 1995, it looked very promising to me and some of my colleagues because of its portable runtime and its garbage collector, which are key elements for supporting functional programming.

We decided to add some functional elements in a language extension of Java, which was called Pizza. This was a name we thought would appeal to hackers. We thought if Java is a hackers drink, then we want to have a name like hackers' food — and that was Pizza.

That way, I got involved a lot in Java, having co-designed the first version of generics and written the "javac" compiler. Then about 10 years ago I felt we’ve pushed it as far as we could, and it was very difficult to push it anywhere further because of the many constraints imposed on us by backwards compatibility and legacy code.

I thought maybe we could take a clean sheet approach and do the same thing once over and incorporate functional programming, but without being held back by the constraints of Java. This led us to develop Scala.

At first, this was an experiment, to answer the question whether we could achieve a tight integration between functional and object-oriented programming and whether this would lead to a useful programming model and language.

When we realised that we could achieve this integration and that it was useful, we continued working on it. Scala developed more and more into an open source environment and then a commercial language where we could have commercial support.

 **Why did you call the language Scala and did you consider any other names?**

It stands for scalable language. Before that, we considered a couple of other names. There was a previous language called Funnel but that was never intended to be practical and for an end user. At some point we considered to name it Clay. In fact I have now found out that name has been taken. The idea behind that was that things were supposed to be easily modelled in the language.

 **When did you start working on Scala and how long did it take?**

I started in 1999 when I joined EPFL (École polytechnique fédérale de Lausanne) after moving back to Europe from Adelaide, Australia. I then had the environment and the elbow room to do something a bit more long term.

The initial Scala design was done in 2001 and the language came out in 2003 — the first iteration of the language. At this stage the language was only being used by my students and a couple of external users.

I committed to teach a course in functional programming and I wanted to use the language from day one in that course. The first class I had were pretty much our "lab rats", where we tried out a couple of language variants and observed how the class worked with them.

 **What changes have you made to Scala since developing it?**

There were some redesigns in the 2005 timeframe when the Scala two series came out. There were some minor Syntax changes done after that, and there was a major feature called "extractors" that was added in 2006. For the last three years we’ve kept the language pretty stable and have focused mostly on the libraries.

 **What was the biggest challenge when you developed it?**

To come up with a statically-typed programming language that provided a fusion of object-oriented and functional programming. This turned out to be unknown terrain, certainly for me and also for other people in the industry. Then, having a language that’s different to Java but at the same time can work well with Java was also a big challenge that we faced.

 **Is Scala still used as the programming language behind Twitter? Did you intend for Scala to be used on social networking sites?**

Scala is used in more and more projects inside Twitter. Raffi Krikorian's keynote at OSCON Java gives a good overview.

Initially we didn’t target Scala to be a language for social, because social media was hardly known back in 2001; we were still at Web 1.0 at the time. It came as a surprise that Twitter were that early an adopter.

A lot of social media sites start off with a dynamic language of some sort, like in the case of Ruby, but some run into scalability problems. Scala keeps much of the "feel" of a dynamic language even though it is statically typed. It thus tends to appeal to people who come from dynamic languages — not all of them but some of them.

 **If you could go back in time, what would you change anything about the language?**

Apart from a few minor details, there is nothing major I would change. The one thing I’m not sure of, is whether I would keep XML literals — they are undoubtedly very very useful now, but they haven’t become as predominant as we thought they would become at the time we designed Scala.

 **Are any students studying programming languages being taught Scala?**

At EPFL, I’ve been teaching it for eight years now, in a number of courses, and other universities are now starting to teach it, too.

 **What advice would you have for up and coming programmers?**

I would say two things: Don’t be afraid to try new things because nothing is cast in stone, and its very good to go to the bottom of things to understand how they work.

 **What’s coming up next for you?**

There are a couple of big challenges. The obvious next challenge is to work out what to do with all of these cores in modern processors. We need to make concurrent and parallel programming accessible to many more people. That’s a big big challenge and something the whole industry will need to work at.

Another thing is to work out how to make a programs more reliable than they are now. And that’s very much an ongoing process, but we make steady advances and it will take a while to crack it — if we can ever crack it completely.

 *Follow Lisa Banks on Twitter: `@CapricaStar <http://twitter.com/CapricaStar>`_ [8]*

Follow Techworld Australia on Twitter: `@Techworld_AU <http://twitter.com/Techworld_AU>`_ [9]

References
*  :ref:`objective-c <objective-c>` 

*  :ref:`python <python>` 

*  :ref:`awk <awk>` 

*  :ref:`bash <bash>` 

*  :ref:`matlab <matlab>` 

*  :ref:`c++ <c++>` 

*  `http://www.computerworld.com.au/tag/a-z_of_programming_languages/ <http://www.computerworld.com.au/tag/a-z_of_programming_languages/>`_ 

*  `http://twitter.com/CapricaStar <http://twitter.com/CapricaStar>`_ 

*  `http://twitter.com/Techworld_AU <http://twitter.com/Techworld_AU>`_ 

.. _ada:

The A-Z of Programming Languages: Ada
=====================================
S. Tucker Taft, maintainer of Ada, speaks about the development of the language and its 1995 and 2005 revisions

* Naomi Hamilton (Computerworld)
* 04 June, 2008 14:49

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]_, and :ref:`bash <bash>` [2]_.

In this article we chat to S. Tucker Taft, Chairman and CTO of SofCheck. Taft has been heavily involved in the Ada 1995 and 2005 revisions, and still works with the language today as both a designer and user.

Computerworld spoke to Taft to learn more about the development and maintenance of Ada, and found a man deeply committed to language design and development.

 **How did you first become involved with Ada?**

After graduating in 1975, I worked for Harvard for four years as the "system mother" for the first Unix system outside of Bell Labs.  During that time I spent a lot of time with some of the computer science researchers, and became aware of the "DOD-1" language design competition.

I had been fascinated with programming language design for several years at that point, and thought it was quite exciting that there was a competition to design a standard language for mission-critical software.  I also had already developed some strong opinions about language design, so I had some complaints about *all* of the designs.

In September of 1980, a year after I left my job at Harvard, I returned to the Boston area and ended up taking a job at `Intermetrics <http://en.wikipedia.org/wiki/Intermetrics>`_ [3]_, the company responsible for the design of the "Red" language, one of the four semifinalists and one of the two finalists for DOD-1. By that time [the language was] renamed to `Ada <http://en.wikipedia.org/wiki/Ada_(programming_language)>`_ [4]_ in honor of Lady `Ada Lovelace <http://en.wikipedia.org/wiki/Ada_Lovelace>`_ [5]_, daughter of Lord Byron and associate of Charles Babbage.

Although Intermetrics had shortly before lost the competition to Honeywell-Bull-Inria, they were still quite involved with the overall process of completing the Ada standard, and were in the process of bidding on one of the two major Ada compiler acquisitions, this one for the Air Force.  After a 6-month design period and 12-month public evaluation, the Intermetrics design was chosen over two others and I became first the head of the "Ada Program Support Environment" part, and then ultimately of the Ada compiler itself.

One of the requirements of the Air Force "Ada Integrated Environment" contract was to write the entire compiler and environment in Ada itself, which created some interesting "bootstrap" problems.  In fact, we had to build a separate "boot" compiler in Pascal, before we could even compile the "real" compiler.  By the time we delivered, we had written almost a million lines of Ada code, and had seen Ada go from a preliminary standard to a Military standard (MIL-STD-1815), to an ANSI standard (Ada 83), and finally to an ISO standard (ISO 8652, Ada 87).  I also had to go through the personal progression of learning the language, griping about the language, and then finally accepting the language as it was, so I could use it productively.

However, in 1988 the US Department of Defense announced that they were beginning the process to revise the Ada standard to produce "Ada 9X" (where X was some digit between 0 and 9). I quickly resurrected all my old gripes and a few new ones, and helped to write a proposal for Intermetrics to become the "Ada 9X Mapping/Revision Team" (the government's nomenclature for the language design team).  This time the Intermetrics team won the competition over several other teams, including one that included Jean Ichbiah, the lead designer of the original Ada 83 standard.  I was the technical lead of the Intermetrics "MRT" team, with Christine Anderson of the Air Force as the manager of the overall Ada 9X project on the government side.

-----

 **What are the main differences between the original Ada and the 95 revision?**

The "big three" Ada 95 language revisions were Hierarchical Libraries, Protected Objects, and Object-Oriented Programming.  Hierarchical Libraries referred to the enhancement of the Ada module namespace to take it from Ada 83's simple "flat" namespace of "library units," where
each unit had a single unique identifier, to a hierarchical namespace of units, with visibility control between parent and child library unit.

Protected Objects referred to the new "passive," data-oriented synchronization construct that we defined to augment the existing "active" message/rendezvous-oriented "task" construct of Ada 83.

Object-Oriented Programming was provided in Ada 95 by enhancing an existing "derived type" capability of Ada 83, by supporting type extension as part of deriving from an existing type, as well as supporting run-time polymorphism with the equivalent of "virtual functions" and run-time type "tags."

 **What prompted the Ada revision in 95?**

ISO standards go through regular revision cycles.  Generally every five years a standard must be reviewed, and at least every ten years it must be revised. There were also some specific concerns about the language, though generally the language had turned out to be a reasonably good fit to the needs of mission-critical software development.

In particular, Ada's strong support for abstract data types in the guise of packages and private types had emerged as a significant step up in software engineering, and Ada's run-time checking for array bounds and null pointers had helped catch a large class of typical programming errors earlier in the life-cycle.

 **Was there a particular problem you were trying to solve?**

Object-oriented programming was growing in popularity at the time, though it was still not fully trusted by much of the mission-critical software development community.  In addition, the Ada 83 tasking model was considered elegant, but did not provide the level of efficiency or control that many real-time system developers would have preferred.

Once the Ada 9X revision process began, a "requirements" team was formed to solicit explicit comments from the Ada community about the language, both in terms of things to preserve and things to improve.

 **Have you faced any hard decisions in your revision of Ada?**

Every language-design decision was pretty hard, because there were many goals and requirements, some of which were potentially conflicting.  Perhaps the most difficult decisions were political ones, where I realized that to achieve consensus in the language revision process among the ISO delegations, we (the design team) would have to give up some of our personal "favourite" revision proposals.

 **Are you still working with the language now and in what context?**

Yes, I am still working with the language, both as a user and as a language designer.  As you may know the newest version of the language, known as Ada 2005, just recently achieved official standardization. The Ada 2005 design process was quite different from the Ada 95 process, because Ada 2005 had no Department of Defense supported design team, and instead had to rely on strictly voluntary contributions of time and energy.

Nevertheless, I am extremely proud of the accomplishments of the Ada 2005 design working group.  We managed to "round out" many of the capabilities of Ada 95 into a language that overall I believe is even better integrated, is more powerful and flexible, while also being even safer and more secure.

 **Would you have done anything differently in the development of Ada 95 or Ada 2005 if you had the chance?**

The few technical problems in the development of Ada 95 that emerged later during use were either remedied immediately, if minor, through the normal language maintenance activities ("we couldn't have meant *that*... we clearly meant to say *this*"). Or if more major, were largely addressed in the Ada 2005 process.  From a process point of view, however, I underestimated the effort required in building international consensus, and in retrospect I should have spent more time establishing the rationale for revision proposals before  "springing" them on the panel of "distinguished reviewers" and the ISO delegations.

-----

 **Are you aware of any of the Defence projects for which the language has been used?**

Ada was mandated for use by almost all significant Defense department software projects for approximately 10 years, from 1987 to 1997, and there were a large number of such projects.  In the early years there were real challenges because of the immaturity of the Ada compilers.  In the later years, in part because of the early difficulties, there were a number of projects that applied and received "waivers" to allow them to use other languages.  Nevertheless, in the "middle years" of 1989 to 1995 or so, there was a boom in the use of Ada, and much of it was quite successful.

As far as specific projects, the Apache helicopter and the Lockheed C-130J  (Hercules II Airlifter) are two well-known examples.  The Lockheed C-130J is particularly interesting because it was developed using a formal "correctness by construction" process using the SPARK Ada-based toolset from Praxis High Integrity Systems.  The experience with that process was that, compared to industry norms for developing safety-critical  avionics software, the C-130J development had a 10 times lower error rate, four times greater productivity, half as expensive a development 
process, and four times productivity increase in a subsequent project thanks to substantial reuse.  NASA has also used Ada extensively for satellite software, and documented significantly higher reuse than their prior non-Ada systems.

In general, in study after study, Ada emerged as the most cost effective way to achieve the desired level of quality, often having an order-of-magnitude lower error rates than comparable non-Ada systems after the same amount of testing.

 **Can you elaborate more on the development of the Static Interface Analysis Tool (SIAT) for Ada on behalf of the NASA Space Stations IV?**

The SIAT project was an early attempt to create a browser-based tool for navigating through a complex software system.  The particular use in this case was for analyzing the software designed for the large network of computers aboard the International Space Station.  It turns out that these systems have a large number of data interfaces, where one computer would monitor one part of the Space Station and report on its state to other computers, by what amounted to a large table of global variables.  The SIAT tool was designed to help ensure that the interfaces were consistent, and that data flowed between the computers and these global variable tables in an appropriate way.

 **Are you aware of why the Green proposal was chosen over the Red, Blue and Yellow proposals at the start of Ada's development?**

The `Green proposal <http://en.wikipedia.org/wiki/Ada_(programming_language)#History>`_ [6]_ reached a level of stability and completeness earlier than the other designs, and Jean Ichbiah did an excellent job of presenting its features in a way that the reviewers could understand and appreciate.  Although there were flashes of brilliance in the other designs, none of them achieved the polish and maturity of the Green design.

 **Did you ever work closely with Jean Ichbiah? If so, what was the working relationship like and what did you do together?**

I worked on and off with Jean during the final days of the Ada 83 design, and during some of the Ada "maintenance" activities prior to the start of the Ada 9X design process.

Jean was busy running his own company at the start of the Ada 9X process, but did end up joining the process as a reviewer for a period during 1992 and 1993.

As it turned out, Jean and I had quite different views on how to design the object-oriented features of the updated language, and he ultimately left the project when it was decided to follow the design team's recommended approach.

-----

 **In your opinion, what lasting legacy have Ada and Ada 95 brought to the Web?**

I believe Ada remains the benchmark against which all other languages are compared in the dimension of safety, security, multi-threading, and real-time control.  It has also been a source for many of the advanced features in other programming languages.  Ada was one of the first widely-used languages to have a language construct representing an abstraction (a package), an abstract data type (a private type), multi-threading (tasks), generic templates, exception handling, strongly-typed separate compilation, subprogram inlining, etc.  In some ways Ada was ahead of its time, and as such was perceived as overly complex.   Since its inception, however, its complexity has been easily surpassed by other languages, most notably C++, while its combination of safety, efficiency, and real-time control has not been equaled.

 **Where do you envisage Ada's future lying?**

As mentioned above, Ada remains the premier language for safety, security, multi-threading, and real-time control.  However, the pool of programmers knowing Ada has shrunk over the years due to its lack of success outside of its high-integrity "niche."  This means that Ada may remain in its niche, though that niche seems to be growing over time, as software becomes a bigger and bigger part of safety-critical and high-security systems.  In addition, the new growth of multi-core chips plays to Ada's strength in multithreading and real-time control.

I also think Ada will continue to play a role as a benchmark for other language design efforts, and as new languages emerge to address some of the growing challenges in widely distributed, massively parallel, safety- and security-critical systems, Ada should be both an inspiration and a model for their designers.

 **Where do you see computer programming languages heading in the future, particularly in the next 5 to 20 years?**

As mentioned above, systems are becoming ever more distributed, more parallel, and more critical.  I happen to believe that a well-designed programming language can help tame some of this growing complexity, by allowing programmers to structure it, abstract it and secure it.

Unfortunately, I have also seen a large number of new languages appearing on the scene recently, particularly in the form of "scripting" languages, and many of the designers of these languages seem to have ignored much of the history of programming language design, and hence are doomed to repeat many of the mistakes that have been made.

 **Do you have any advice for up-and-coming programmers?**

Learn several different programming languages, and actually try to use them before developing a "religious" affection or distaste for them.  Try Scheme, try Haskell, try Ada, try Icon, try Ruby, try CAML, try Python, try Prolog.  Don't let yourself fall  into a rut of using just one language, thinking that it defines what programming means.

Try to rise above the syntax and semantics of a single language to think about algorithms and data structures in the abstract.  And while you are at it, read articles or books by some of the language design pioneers,  like Hoare, Dijkstra, Wirth, Gries, Dahl, Brinch Hansen, Steele, Milner, and Meyer.

 **Is there anything else that you'd like to add?**

Don't believe anyone who says that we have reached the end of the evolution of programming languages.


References:

.. [1] :ref:`awk <awk>`

.. [2] :ref:`bash <bash>`

.. [3] `http://en.wikipedia.org/wiki/Intermetrics <http://en.wikipedia.org/wiki/Intermetrics>`_

.. [4] `http://en.wikipedia.org/wiki/Ada_(programming_language) <http://en.wikipedia.org/wiki/Ada_(programming_language)>`_

.. [5] `http://en.wikipedia.org/wiki/Ada_Lovelace <http://en.wikipedia.org/wiki/Ada_Lovelace>`_

.. [6] `http://en.wikipedia.org/wiki/Ada_(programming_language)#History <http://en.wikipedia.org/wiki/Ada_(programming_language)#History>`_

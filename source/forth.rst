.. _forth:

The A-Z of Programming Languages: Forth
=======================================
Charles H. Moore talks about the origins of Forth and how it shows that a computer language can be simple and powerful.
* Naomi Hamilton(Computerworld)

* 27 June, 2008 08:01

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_6595.jpg
   :alt: Charles H. Moore

Charles H. Moore

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`ada <ada>` [2]on the Ada 1995 and 2005 revisions, :ref:`asp <asp>` [3]about its server-side script engine ASP, :ref:`bash <bash>` [4]about his experience maintaining Bash, and :ref:`c++ <c++>` [5]of C++ fame.

In this article we chat to Charles H. Moore about the design and development of Forth. Moore invented Forth while at the US National Radio Astronomy Observatory to help control radio telescopes and data-collection/reduction systems.

Here he chats about why Forth was invented, as well as why he still works with Forth today.

 **How did Forth come into existence?**

Forth came about when I was faced with a just-released IBM 1130 minicomputer. Compiling a Fortran program was a cumbersome procedure involving multiple card decks. I used Fortran to develop the first Forth, which could use the disk and graphics display that Fortran couldn't. Because it was interactive, my programming was much faster and easier.

 **Was there a particular problem you were trying to solve?**

This computer was at Mohasco Industries. Their main product was carpet and the problem was to determine if the 1130 could help design carpets. Forth was great for programming the 2250 display, but ultimately the lack of color doomed the project.

 **Did you face any hard decisions in the development of the language?**

The hardest decision in developing Forth was whether to do it. Languages were not casually designed. It was reputed to require a brilliant team and man-years of effort. I had my eye on something quick and simple.I write Forth code every day. It is a joy to write a few simple words and solve a problem. As brain exercise it far surpasses cards, crosswords or Sudoku; and is useful.

 **How did Forth get its name?**

I decided to call it Fourth, as in 4th-generation computer language. But the 1130 file system limited names to 5 characters, so I shortened it to Forth. A fortuitous choice, since Forth has many positive associations.

 **I have read that Forth was developed from your own personal programming system, which you began to develop in 1958. Can you tell us a little more about this?**

My personal programming system was a deck of punch cards, [now] sadly lost. It had a number of Fortran subroutines that did unformatted input/output, arithmetic algorithms and a simple interpreter. It let me customize a program via its input at a time when recompiling was slow and difficult.

 **Why did you incorporate Reverse Polish notation into the language?**

Reverse Polish notation is the simplest way to describe arithmetic expressions. That's how you learn arithmetic in grade school, before advancing to infix notation with Algebra. I've always favored simplicity in the interest of getting the job done.
 -

----- **Was the language developed particularly for your work at the National Radio Astronomy Observatory?**

I did most of my work at NRAO in Forth: controlling several radio telescopes and data-collection/reduction systems, with the reluctant approval of the administration. The only reason this was permitted was that it worked: projects took weeks instead of years, with unparalleled performance and capabilities.

 **If you had the chance to re-design the language now, would you do anything differently?**

Would I do anything differently? No. It worked out better than I dreamed. The thing about Forth is that if I wanted a change, I made it. That's still true today. Forth is really a language tool kit. You select and modify every time you encounter a new application.

 **Do you still use/work with Forth?**

Yes indeed, I write Forth code every day. It is a joy to write a few simple words and solve a problem. As brain exercise it far surpasses cards, crosswords or Sudoku; and is useful.

 **What is your reaction to comments such as the following from Wikipedia:**

'Forth is a simple yet extensible language; its modularity and extensibility permit the writing of high-level programs such as CAD systems. However, extensibility also helps poor programmers to write incomprehensible code, which has given Forth a reputation as a "write-only language"'?

All computer languages are write-only. From time to time I have to read C programs. They are almost incomprehensible. It's not just the syntax of the language. It's all the unstated assumptions. And the context of the operating system and library. I think Forth is less bad in this regard because it's compact; less verbiage to wade thru. I like the observation that Forth is an amplifier: a good programmer can write a great program; a bad programmer a terrible one. I feel no need to cater to bad programmers.

 **Do you know of many programs written using Forth, and if so, what's your favourite?**

Forth has been used in thousands of applications. I know of very few. The Forth Interest Group held conferences in which applications were described. It was amazing the variety. My current favorite is that Forth is orbiting Saturn on the Cassini spacecraft.

 **In your opinion, what lasting legacy do you think Forth has brought to the web?**

The various Web pages and forums about Forth make a powerful point: Forth is alive and well and offers simple solutions to hard problems. Forth is an existence proof. A lasting legacy to KISS (keep it simple, stupid).

 **What made you develop colorForth?**

I was driven away from Forth by the ANSI standard. It codified a view of Forth that I disliked: megaForth; large, unwieldy systems. I was finally faced with the need for VLSI chip design tools. And [I was also] blessed with some insight as to how Forth could be made faster, simpler and more versatile. Hence, colorForth. Sadly [it has been] ignored by most Forth programmers.
 -

----- **Are there many differences between Forth and colorForth?**

ColorForth adds a new 'time' to Forth. Forth is intrinsically interactive. The programmer must distinguish compile-time from run-time, and switch back-and-forth between them. Anything that can be done at compile-time will save run-time. In colorForth there is also edit-time, which can save compile-time. The colorForth editor pre-parses text into Shannon-coded strings that are factored into 32-bit words. Each word has a 4-bit tag the compiler uses to interpret it. Compilation is very fast. ColorForth also restricts its primitives so they can be efficiently executed by a Forth chip.Forth is an existence proof. It shows that a computer language can be simple and powerful.

 **Where do you envisage Forth's future lying?**

I'm betting that parallel computers will be the future, and Forth is an excellent parallel-programming language. But I expect that conventional languages will become more complex in order to describe parallel processes. Computer scientists must exercise their ingenuity and have something non-trivial to teach.

 **Do you have any advice for up-and-coming programmers?**

I think it behooves new programmers to sample all the languages available. Forth is the only one that's fun. The satisfaction of finding a neat representation cannot be equaled in Fortran, C or even Lisp. (And mentioning those languages surely dates me). Try it, you'll like it.

 **What are you working on now?**

Currently I'm working with OKAD, my colorForth CAD tools, to design multi-core computer chips. They're small, fast and low-power, just like Forth.

 **Would you like to add anything else?**

To reiterate: Forth is an existence proof. It shows that a computer language can be simple and powerful. It also shows that "The race is not to the swift". The best solution is not necessarily the popular one. But popularity is not a requirement. There are many applications where a good solution is more important than popular methodology.

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

.. _python:

The A-Z of Programming Languages: Python
========================================
Our series on the most popular programming languages continues as we chat to Van Rossum, the man behind Python.

* Naomi Hamilton(Computerworld)
* 05 August, 2008 22:33

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_8247.jpg
   :alt: Guido van Rossum, author of Python.

Guido van Rossum, author of Python.

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Alfred v. Aho of :ref:`awk <awk>` [1], S. Tucker Taft on the :ref:`ada <ada>` [2], Microsoft about its server-side script engine :ref:`asp <asp>` [3], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [4], Bjarne Stroustrup of :ref:`c++ <c++>` [5]and to Charles H. Moore about the design and development of :ref:`forth <forth>` [6]. We've also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [7], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [8], Luca Cardelli on :ref:`modula-3 <modula-3>` [9], Walter Bright :ref:`d <d>` [10], and most recently, Brendan Eich on :ref:`javascript <javascript>` [11].

This time we chat with Guido van Rossum, Monty Python and Hitchhikers Guide to the Galaxy fan. Van Rossum is best known as the author of Python, and currently works for Google, CA where he gets to spend at least half his time developing the language.

 *Please note that we are no longer following exact alphabetical order for this series, due to popular demand. If you'd like to submit any suggestions for programming languages or language authors that you'd like to see covered, please email **naomi@computerworld.com.au.***

 **What was the motivation behind the development of such a productive programming language?**

Long ago, around 1989, at CWI in Amsterdam, I was part of a group developing a novel operating system. We found that we needed to write a lot of applications to support users, and that writing these in C our productivity was atrocious. This made me want to use something like ABC, a language I had help implemented (also at CWI) earlier that decade.

ABC had much higher productivity than C, at the cost of a runtime penalty that was often acceptable for the kind of support applications we wanted to write: things that run only occasionally, for a short period of time, but possibly using complex logic. However, ABC had failed to gain popularity, for a variety of reasons, and was no longer being maintained (although you can still download it from http://homepages.cwi.nl/~steven/abc/). It also wasn't directly usable for our purpose -- ABC had been designed more as a teaching and data manipulation language, and its capabilities for interacting with the operating system (which we needed) were limited to non-existent by design.

Being youthful at the time I figured I could design and implement a language "almost, but not quite, entirely unlike" ABC, improving upon ABC's deficiencies, and solve our support applications problem, so around Christmas 1989, I started hacking. For various reasons, soon after Python was complete enough to be used, that particular project was no longer as relevant, but Python proved useful to other projects at CWI, and in early 1991 (i.e. a little over a year after I started) we did the first open source release (well before the term open source had even been invented).

 **Was there a particular problem you were trying to solve?**

Programmer productivity. My observation at the time was that computers were getting faster and cheaper at an incredible rate. Today this effect is of course known as Moore's law. At the same time, as long as the same programming languages were being used, the cost of the programmers to program them remained was not going down. So I set out to come up with a language that made programmers more productive, and if that meant that the programs would run a bit slower, well, that was an acceptable trade-off. Through my work on implementing ABC I had a lot of good ideas on how to do this.

 **Are you a Monty Python fan (given the name and other elements of the language derive from Monty Python's Flying Circus)?**

Yes, this is where I took the language's name. The association with the snake of the same name was forced upon me by publishers who didn't want to license Monty-Python artwork for their book covers. I'm also into the Hitchhiker's Guide to the Galaxy, though I'm not into much other staples of geek culture (e.g. no sci-fi or fantasy, no role playing games, and definitely no computer gaming).

-----

 **Given that the language was developed in the 1980s, what made you publish it in 1991?**

I actually didn't start until the very end of 1989. It took just a bit over a year to reach a publishable stage.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of the language?**

I can't remember anything particularly frustrating or difficult, certainly not during the first few years. Even management (usually the killer of all really interesting-but-out-of-left-field projects) indulged my spending an inordinary amount of my time on what was considered mostly a hobby project at the time.

 **Would you do anything differently if you had the chance?**

Perhaps I would pay more attention to quality of the standard library modules. Python has an amazingly rich and powerful standard library, containing modules or packages that handle such diverse tasks as downloading web pages, using low-level Internet protocols, accessing
databases, or writing graphical user interfaces. But there are also a lot of modules that aren't particularly well thought-out, or serve only a very small specialized audience, or don't work well with other modules.

We're cleaning up the worst excesses in Python 3.0, but for many reasons it's much harder to remove modules than to add new ones -- there's always *someone* who will miss it. I probably should have set the bar for standard library modules higher than I did (especially in the early days, when I accepted pretty much anything anyone was willing to contribute).

 **A lot of current software is about writing for the web, and there are many frameworks such as Django and Zope. What do you think about current web frameworks based on Python?**

For a few years there were definitely way too many web frameworks. While new web frameworks still occasionally crop up, the bar has been set much higher now, and many of the lesser-known frameworks are disappearing. There's also the merger between TurboGears and Pylons.

No matter what people say, Django is still my favorite -- not only is it a pretty darn good web framework that matches my style of developing, it is also an exemplary example of a good open source project, run by people who really understand community involvement.

 **What do you think about Ruby on Rails?**

I've never used it.  Obviously it's a very successful web framework, but I believe (based on what users have told me) that Django is a close match.

 **We've all heard about how Python is heavily used by Google currently. How do you feel about this? Has this exceeded your expectations for the language?**

I never had any specific expectations for Python, I've just always been happy to see the user community grow slowly but steadily.  It's been a great ride.

 **Why has the language not been formally specified?**

Very few open source languages have been formally specified.  Formal language specifications seem to be particularly attractive when there is a company that wants to exercise control over the a language (such as for Java and JavaScript), or when there are competing companies that worry about incompatible implementations (such as for C++ or SQL).

 **What's the most interesting program you've seen written with Python?**

In terms of creative use of the language in a new environment, I think that would be MobilLenin, an art projects for Nokia phones written by Jurgen Scheible.

 **Have you ever seen the language used in a way that wasn't originally intended?**

Well, *originally* I had a pretty narrow view on Python's niche, and a lot of what people were doing with it was completely unexpected.  I hadn't expected it to be used for writing expert systems, for example, and yet one of the early large examples was an expert system. I hadn't planned for it to be used to write high-volume network applications like Bittorrent either, and a few years back someone wrote a VOIP client in Python.

I also hadn't foreseen features like dynamic loading of extension modules, or using Python as an embedded programming language.  And while ABC was designed in part as the teaching language, that was not a goal for Python's design, and I was initially surprised at Python's
success in this area -- though looking back I really should have expected that.

-

-----

 **How do you feel about the title bestowed on you by the Python community: Benevolent Dictator for Life (BDFL)?**

It totally matches the whimsical outlook I try to maintain on Python. By the way, the original title (as I have recently rediscovered after digging through age-old email), invented in 1995, was First Interim Benevolent Dictator For Life.  At a meeting of Python developers and fans in Reston, Virginia, everyone present was bestowed with a jocular title, but mine was the only one that stuck.

 **Do you agree with the following statement taken from Wikipedia:** *"Python can also be used as an extension language for existing modules and applications that need a programmable interface. This design, of a small core language with a large standard library and an easily-extensible interpreter, was intended by Van Rossum from the very start, due to his frustrations with ABC, which espoused the opposite mindset"?*

Yeah, that nails it.  ABC was designed as a diamond -- perfect from the start, but impossible to change.  I realized that this had accidentally closed off many possible uses, such as interacting directly with the operating system: ABC's authors had a very low opinion of operating systems, and wanted to shield their users completely from all their many bizarre features (such as losing data when you removed a floppy disk at the wrong time).  I didn't have this same fear: after all, Python originated in an operating systems research group!  So instead I built extensibility into the language from the get-go.

 **Do you believe that the large standard library is one of Python's greatest strengths?**

Despite the misgivings about the quality of (parts of) the standard library that I expressed above, yes, very much so.  It has often been a convincing argument for deciding to use Python in a particular project when there were already standard library modules or packages to perform important tasks of the project at hand.  Of course, the many third party extensions also add to this argument, but often it helps to notice that a single install (or, on modern Unix systems, no install at all, since Python comes pre-installed) is all what's needed to get started.

 **Given that you launched the Computer Programming for Everybody (CP4E) initiative while working at the Corporation for National Research Initiatives (CNRI), and the clean syntax of Python, do you think that computer programming is an area that should be more accessible to the general public?**

I certainly believe that educators would be wise to teach more about computer use than how to write PowerPoint presentations and HTML (useful though those are).  Some people have been quite successful using Python for all kinds of educational purposes, at many different levels.  However education is an incredibly politicized subject and I've burned myself enough that I will refrain from further commentary on this subject.

 **How have Python Enhancement Proposals (PEPs) helped in the development of Python? Which is your favourite?**

PEPs have made a huge difference.  Before we started using PEPs, the was no specific process for getting something about the language or standard library changed: we had heated debates on the mailing list, but no clear way to make a decision, and no policy about what kinds of changes would need what kind of consensus.  Sometimes people "got lucky" by sending me a patch, and if I happened to like it, it went in-- even though perhaps it wasn't always a well-thought-out design.

Other times good ideas got stuck in endless "bikeshedding" (as it has now become known) about itty-bitty details.  The PEP process helped frame these debates: the goal of a discussion was to arrive at a PEP, and a PEP needed to have a motivation, a specification, a discussion of alternatives considered and rejected, and so on.  The PEP process (with slight variations) was also adopted by other open source projects. I'm sure this has helped generations of open source developers be more productive in their design discussions, and by having the whole PEP process written out (in PEP number 1) it also has served as education for new developers.

My favorite is PEP 666, which was written with the explicit objective to be rejected: it proposes a draconian attitude towards indentation, and its immediate rejection once and for all settled an argument that kept coming up (between tab-lovers and tab-haters).  It is a great example of the rule that negative results are useful too.

 **Do you have any idea how many Python Enhancement Proposals (PEP's) have been submitted over the language's history?**

Yes, 239.  While they aren't numbered consecutively, they are all kept under version control (at the same Subversion server we use for the Python source tree) so they are easy to count.  The highest-numbered one is 3141.  This isn't counting a number of proposals that were nipped in the bud -- occasionally someone drafts a PEP but before they can submit it for review it's already killed by the online discussion.

 **How has 3.01b been received since it's release in June this year? Does it vary greatly from the 3.01a release?**

It's been received well -- people are definitely downloading the successive betas (two so far with a third planned) and kicking the tyres.  Perhaps the most visible difference from the last of the alphas is the standard library reorganization -- that project hadn't really gotten started until the first beta.  Other than that the differences are mostly minor improvements and bugfixes, nothing spectacular.

-

-----

 **Do you currently use CPython?**

It's the only Python version I use regularly.  It's also embedded in Google App Engine, and I use that a lot of course.

 **How do you feel about the 3.0 release series breaking backward compatibility?**

It's the right thing to do.  There were a number of design problems in the language that just couldn't be fixed without breaking compatibility.  But beyond those and a few cleanups we're actually trying not to break compatibility that much -- many proposals to add new features that would introduce incompatibilities were rejected for that very reason, as long as an alternative was available that avoided the incompatibility.

 **Do you consider yourself a Pythonista?**

It's not a term I would use myself, but if someone writes an email starting with "Dear Pythonistas" I certainly will assume I'm included in that audience.  The Monty Python folks are sometimes referred to as Pythons; that's a term we never use.  Similarly, Pythonesque tends to refer to "in the style of Monty Python" while we use Pythonic meaning roughly "compatible with Python's philosophy".  Obviously that's a pretty vague term that is easily abused...

 **Where do you see Python going in the embedded space?**

I'm assuming you're referring to platforms like cell phones and custom hardware and such.  I think those platforms are ready for Python, with enough memory and speed to comfortably run an interpreted language like Python.  (Hey, the Python runtime is a lot smaller than the JVM!) Actual adoption differs -- there's the Nokia S60 platform which has adopted Python as its official scripting language, and embedded platforms running some form of Linux can in theory easily run Python.

 **In your opinion, what lasting legacy has Python brought to computer development?**

It has given dynamic languages a morale boost.  It has shown that there are more readable alternatives to curly braces.  And for many it has brought fun back to programming!

 **Where do you envisage Python's future lying?**

Sorry, my crystal ball is in the shop, and I only use my time machine to go back in time to add features that are only now being requested. (That I have a time machine and use it for that purpose is a standing joke in the Python community.)

 **Has the evolution and popularity of the language surprised you in anyway?**

I certainly hadn't expected anything of the sort when I got started. I don't know what I expected though -- I tend not to dwell too much on expectations and just like to fix today's problems.  It also has come very gradually, so any particular milestone was never a big surprise. But after nearly 19 years I'm certainly very happy with how far we've come!

 **What are you proudest of in terms of the language's initial development and continuing use?**

That Python is and has always been the #1 scripting language at Google, without contest.  Also, that the language has made it to the top 5 of dynamic languages on pretty much a zero PR budget. That's a tremendous achievement for a grassroots community.

-----

 **Where do you see computer programming languages heading in the near future?**

I hope that at some point computers will have sufficient power that we don't need separate functional, dynamic, and statically typed languages, and instead can use a single language that combines the benefits of all three paradigms.

 **Do you have any advice for up-and-coming programmers?**

Learn more than one language.  It's amazing how eye-opening it can be to compare and contrast two languages.

 **And finally, no interview on Python would be complete without the following questions:
a. How do you feel about the indentation in Python now?**

It's the right thing to do from a code readability point of view, and hence from a maintenance point of view.  And maintainability of code is what counts most: no program is perfect from the start, and if it is successful, it will be extended.  So maintenance is a fact of life, not a necessary evil.

 **b. Do you favour tabs or spaces?**

Definitely spaces.  Four to be precise (even though the Google style guide uses two).

 **Is there anything else you'd like to add?**

Hardly; you've been very thorough.  I'd like to say hi to my many Aussie fans, and I promise that one of these years I'll be visiting your country to give some talks and do a bit of snorkeling. :-)

References
*  :ref:`awk <awk>`

*  :ref:`ada <ada>`

*  :ref:`asp <asp>`

*  :ref:`bash <bash>`

*  :ref:`c++ <c++>`

*  :ref:`forth <forth>`

*  :ref:`intercal <intercal>`

*  :ref:`yacc <yacc>`

*  :ref:`modula-3 <modula-3>`

*  :ref:`d <d>`

*  :ref:`javascript <javascript>`

.. _modula-3:

The A-Z of Programming Languages: Modula-3
==========================================
Luca Cardelli on why Modula-3 played a major role in popularizing the notion of type-safe programming and why Modula-3 is still vastly superior to programs such as Java.
* Naomi Hamilton(Computerworld)

* 15 July, 2008 21:38

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_7132.jpg
   :alt: Luca Cardelli 

Luca Cardelli

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`ada <ada>` [2]on the Ada 1995 and 2005 revisions, Microsoft about its server-side script engine :ref:`asp <asp>` [3], :ref:`bash <bash>` [4]about his experience maintaining Bash, :ref:`c++ <c++>` [5]of C++ fame, to :ref:`forth <forth>` [6]about the design and development of Forth, a chat with the irreverent :ref:`intercal <intercal>` [7]about the development and uses of INTERCAL and most recently, Stephen C. Johnson on YACC.

In this interview, we chat with Luca Cardelli, a member of the Modula-3 design committee. Cardelli is a Principal Researcher and Head of the Programming Principles and Tools and Security groups at Microsoft Research in Cambridge, UK, and is an ACM Fellow. Here he chats to Computerworld about the origins of Modula-3, including how the most exciting Modula-3 design meeting ever, was abruptly interrupted by the San Francisco 7.1 earthquake.

Please note that we are no longer following exact alphabetical order for this series, due to popular demand. If you'd like to submit any suggestions for programming languages or language authors that you'd like to see covered, please emailnaomi@computerworld.com.au.

 **Why did you feel the need to develop Modula-3? Was it a reaction to a problem that needed solving?**

The "problem" was developing programming environments in a type-safe language. This meant that if I wrote a type-safe library, and my clients had a hard crash, I could say: "not my problem, somebody must be cheating somewhere" because the typechecker guaranteed that it wasn't my problem. You couldn't say that if you used C++.

 **Why was the name Modula-3 chosen?**

We wanted to show continuity of the basic philosophy of modularization of Modula-2, carried out into an object-oriented language. Klaus Wirth designed Modula-2 while (or shortly after) visiting Xerox PARC, so there was a common origin. We asked him to use the name Modula-3, and he agreed, and he also occasionally attended our meetings.

 **How did Modula-2+ influence the design of Modula-3?**

It was basically the same language, but with none of the dark corners.

Modula-2+ had been developing organically, and needed a cleanup and standardization. We also wanted to publicize the innovative features of Modula-2+ (which largely came from Cedar/Mesa at Xerox PARC), and make them available to a wider community.

 **Were there any particularly hard/annoying problems you had to overcome in the development of the language?**

Settling the type system was the hard part, not only for me, but I believe for everybody. A POPL paper discussed just that part.

 **Why was one of the language's aims to continue the tradition of type safety, while introducing new elements for practical real-world programming? Was there a real need for this in the 1980s?**

Yes, the idea to design type-safe operating systems was still in full swing. It started at Xerox with Cedar/Mesa, and continued at DEC with the Taos operating system. You might say it is still continuing with Microsoft's .NET, and we are not quite there yet.
 -

----- **What is the most interesting program that you've seen written with Modula-3?**

I'll just talk about my programs. I wrote the second program (after Next Computer's) direct-manipulation user interface editor. And I wrote the Obliq distributed programming language, which was heavily based on Modula-3's network objects.

 **Have you ever seen the language used in a way that was not originally intended? If so, what was it? And did it or didn't it work?**

Not really; we intended to support type-safe systems programming and that is what happened. It's possible that we missed some opportunities, however.

 **Why do you think that the language hasn't been widely adopted by industry, but is still influential in research circles?**

Basically, competition from Java. Java had all the same main features (objects, type safety, exceptions, threads), all of which also came from the same tradition (and I believe they read our tech reports carefully...). In addition, Java initially had innovations in bytecode verification and Web applets, and later had the full support of a large company, while we were only supporting Modula-3 form a research lab. I believe the module system in Modula-3 is still vastly superior to programs such as Java, and that may explain continued interest.

 **Do you still use Modula-3 today? Is the language still being contributed to and updated?**

While Modula-3 was my all-time favorite language, I stopped using it after leaving DEC. I used Java for a short period, and today I occasionally use C# and F#.

 **How do you feel about statements such as this in Wikipedia: "Modula-3 is now taught in universities only in comparative programming language courses, and its textbooks are out of print"?**

It's probably accurate!

 **According to Wikipedia, the Modula-3 "standard libraries [were] formally verified not to contain various types of bugs, including locking bugs." Why was this?**

Type safety gets rid of a lot of silly bugs, but the main class of bugs it does not prevent are concurrency bugs. The expectation for Modula-3 libraries was that they would have a complete description (in English), of exactly what each procedure did and what it required. There was social pressure at the time to make these descriptions very precise. Some were so precise that they were amenable to formal verification. This was considered important for some base libraries, particularly in terms of locking behavior, because locking bugs were not captured by the type system, and were the hardest to debug.

-

-----

 **In your opinion, what lasting legacy has Modula-3 brought to computer development?**

I think what's important is that Modula-3 played a major role in popularizing the notion of type-safe programming. Cedar/Mesa was tremendously innovative, but was always kept "secret" at Xerox (I doubt that even now you can get its manual). And ML (the other root language of type safety) was always an academic non-object-oriented language. Modula-3 was the stepping stone from Cedar/Mesa to Java; and today, type-safe programming is a given. I am personally very proud (as a former ML type-safe programmer) that I was able to hang-on to Modula-3 until Java came out, therefore avoiding the C++ era altogether!

 **What are you proudest of in terms of the language's development and use?**

The development of the type system, and the module system. In terms of use, we used it for over 10 years (including Modula-2+) to write all our software, from OS's to GUI's, for several million lines of code. One of the most amazing features of Modula-3 was the Network Objects, (but that was not my work), which was transferred directly to become Java RMI.

 **Where do you see computer programming languages heading in the future, particularly in the next 5 to 20 years?**

Functional programming is coming back. Even an object-oriented language like C# now is a full functional language, in the sense that it supports first-class nameless lambda abstractions with proper scope capture and type inference, and developers love it. Other proper functional languages (which do not include object-oriented features) like F# and Haskell are becoming more and more popular.

 **Do you have any advice for up-and-coming programmers?**

Read other people's code!

 **Is there anything else of interest that you'd like to add?**

Only that the most exciting Modula-3 design meeting ever, was abruptly interrupted by the San Francisco 7.1 earthquake.

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

.. _f#:

The A-Z of Programming Languages: F#
====================================
Microsoft researcher Don Syme talks about the development of F#, its simplicity when solving complex tasks, the thriving F# community and the future ahead for this fuctional programming language.
* Kathryn Edwards(Computerworld)

* 23 December, 2008 12:30

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_13480.jpg
   :alt: Don Syme, Senior Researcher, Microsoft Research Cambridge. Image credit: Microsoft Research.

Don Syme, Senior Researcher, Microsoft Research Cambridge. Image credit: Microsoft Research.

 *Computerworld*is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Alfred v. Aho of :ref:`awk <awk>` [1], S. Tucker Taft on the :ref:`ada <ada>` [2], Microsoft about its server-side script engine :ref:`asp <asp>` [3], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [4], Bjarne Stroustrup of :ref:`c++ <c++>` [5]and to Charles H. Moore about the design and development of :ref:`forth <forth>` [6]. We've also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [7], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [8], Luca Cardelli on :ref:`modula-3 <modula-3>` [9], Walter Bright :ref:`d <d>` [10], Simon Peyton-Jones on :ref:`haskell <haskell>` [11]and more recently, with Larry Wall, creator of the :ref:`perl <perl>` [12]programming language.

This time we chat with Don Syme, senior researcher at `Microsoft Research Cambridge <http://research.microsoft.com/en-us/labs/cambridge/>`_ [13], who developed F#.

 **What prompted the development of F#?**

From the beginning, the aim of F# has been to ensure that typed functional programming in the spirit of OCaml and :ref:`haskell <haskell>` [11], finds a high-quality expression on the .NET framework. These languages excel in tasks such as data transformations and parallel programming, as well as general purpose programming.

 **How did the name F# come about?**

In the F# team we say “F is for Fun.” Programming with F# really does make many routine programming tasks simpler and more enjoyable, and our users have consistently reported that they’ve found using the language enjoyable.

However, in truth the name comes from “F for Functional,” as well as a theoretical system called “System F.”

A good functional program is like a beautiful poem: you see the pieces of a ‘solution’ come togetherSyme

 **Were there any particular problems you had to overcome in the development of the language?**

Combining object-oriented and functional programming poses several challenges, from surface syntax to type inference to design techniques. I’m very proud of how we’ve addressed these problems.

F# also has a feature called “computation expressions,” and we’re particularly happy with the unity we’ve achieved here. **Would you have done anything differently in the development of F# if you had the chance?**

In a sense, we’re tackling this now. Some experimental features have been removed as we’re bringing F# up to product quality, and we’ve also made important cleanups to the language and library. These changes have been very welcomed by the F# community. 
-

----- **Was F# originally designed in the .NET framework?**

Yes, totally. F# is all about leveraging the benefits of both typed functional programming and .NET in unison.

 **What elements has F# borrowed from ML and OCaml?**

F# is heavily rooted in OCaml, and shares a core language that permits many programs to be cross-compiled. The type system and surface syntax are thus heavily influenced by OCaml.

 **What feedback did the F# September 2008 CTP release get?**

It’s been really great.  We’ve heard from existing F# developers who have been really happy to see all the improvements in the CTP release – in particular some of the improvements in the Visual Studio integration.  It’s also been great to see lots of new users coming to F# with this new release.

 **Do you have any idea how large the F# community currently is?**

It’s hard to tell. We’re getting an excellent and active community developing, mainly around `hubFS <http://www.hubfs.net/>`_ [14]and have seen consistent growth throughout the year.

 **You say on your blog that “one of the key things about F# is that it spans the spectrum from interactive, explorative scripting to component and large-scale software development.” Was this always a key part of the development of F#, or has it simply morphed into a language with these features over time?**

A key development for us was when we combined F# Interactive with Visual Studio. This allowed F# users to develop fast, accurate code using Visual Studio’s background type-checking and Intellisense, while interactively exploring a problem space using F# Interactive. We brought these tools together in late 2005, and that’s when the language really started hitting its niche.

 **What are you currently most excited about in the development of F#?**

This year we have really focused on ensuring that programming in F# is simple and intuitive. For example, I greatly enjoyed working with a high-school student who learned F#. After a few days she was accurately modifying a solar system simulator, despite the fact she’d never programmed before. You really learn a lot by watching a student at that stage.

 **How much influence has Haskell had on the development of F#?**

A lot! One of the key designers of Haskell, Simon Peyton-Jones, is just down the corridor from me at Microsoft Research Cambridge and has been a great help with F#, so I have a lot to thank him for. Simon gave a lot of feedback on the feature called “asynchronous workflows” in particular. The F# lightweight syntax was also inspired by Haskell and :ref:`python <python>` [15].
 
Over the last five years F# has seen a lot of idea sharing in the language community, at conferences such as Lang.NET. The .NET framework has played an important role in bringing the programming camps together.
-

----- **Have you always worked with functional languages? Do you have a particular affinity with them? What initially attracted you?**

I’ve worked with many languages, from BASIC to assembly code. One of the last check-ins I made when implementing generics for .NET, C# and VB had a lot of x86 assembly code. My first job was in Prolog. I think programmers should learn languages at all extremes.

Functional languages attract me because of their simplicity even when solving complex tasks. If you look through the code samples in a book such as *F# for Scientists*they are breathtaking in their elegance, given what they achieve. A good functional program is like a beautiful poem: you see the pieces of a ‘solution’ come together.

Of course, not all programs end up so beautiful. It’s very important that we tackle ‘programming in the large’ as well. That’s what the object-oriented features of F# are for.

 **Why did Microsoft decide to undertake the development of F# and how does F# fit into Microsoft’s overall strategy and philosophy?**

Microsoft’s decision to invest in further F# research is very much based on the fact that F# adds huge value to the .NET platform. F# really enables the .NET platform to reach out to new classes of developers, and appeal to domains where .NET is not heavily used.  This is especially true in data exploration and technical computing. We’re also exploiting functional techniques in parallel programming.

 **What is the most interesting program you’ve seen written in F#?**

That’s a good question! I’ll give several answers. I’ve mentioned the samples from *F# for Scientists*, which are very compelling. But for sheer F# elegance, I like Dustin Campbell’s series of Project Euler solutions.

However, some of the most intriguing to me are the ones that integrate F# into existing data-oriented tools such as AutoCAD and ArcGIS. These domains are, in theory, well suited to functional programming, but no functional language has ever interoperated with these tools before. Through the magic of .NET interoperability, you can now use F# with the .NET APIs for these tools, which opens up many possibilities.

 **Why do you think a programmer would choose to write apps in F# rather than C#?**

Many programmers choose to explore a problem in F# because it lets them focus more on the problem domain and less on programming itself. That’s a big benefit in some of the data exploration, algorithmic and technical computing domains, and so we’ve seen a lot of interest in using F# here, where C# may not have been an obvious choice.
-

----- **Do you think that F# and C# are complimentary languages, or will one become more dominant than the other?**

C# and VB.NET are clearly the elder statesmen of.NET languages and it’s hard to imagine a really major .NET project where these languages don’t play a significant role. So the approach we take with F# is that it’s definitely complementary to C#. We expect there will be many F# projects that incorporate C# components. For example, the designer tools we use with F# emit C# code, and you then call your F# code from those the event handlers. A working knowledge of C# is thus very useful for the F# programmer.

 **In your opinion, what lasting legacy will F# bring to computer development?**

Our aim with F# has been to make typed functional programming real and viable. The feedback we’ve received often shows that our users are thrilled to have a programming solution that fills this role. However, perhaps the greatest sign of success will be when people copy what we’ve done and reuse the ideas in other settings.

 **Have you received much criticism of the language so far? If so, what has this been focused on?**

We’ve received lots and lots of feedback – we’ve been in almost continual engagement with the F# community for the last three years. This has been extraordinary. People have been very helpful, and have come up with many great ideas and suggestions. However, we’re just as glad to get the “this is broken” emails as we are glowing praise – indeed even more glad – we want to know when things don’t add up, or don’t make sense.

Some programmers do have a hard time adjusting their mindset from imperative programming to OO, though most find the transition enjoyable. Learning new paradigms can sometimes be easier for beginners than experienced programmers. However, one of the great things about F# is that you can “change one variable at a time,” e.g. continue to use your OO design patterns, but use functional programming to implement portions of your code.

 **What are you proudest of in terms of the language's initial development and continuing use?**

I’m proud of the F# community, for being so helpful to beginners. For the language itself, I’m very happy with the way we’ve stayed true to functional programming while still integrating with .NET.

 **Where do you see computer programming languages heading in the future?**

People thirst for simplicity.  People need simple solutions to the problems that really matter: data access, code development, deployment, cloud computing, Web programming, and parallel programming, to name a few. One of the exciting things about working in the Visual Studio team is that there are world experts in all of these domains working in unison. We won’t cover all of these bases with the first release of F#, but over time we’ll be operating in all these domains.

At the language level, people say that languages are converging in the direction of mixed functional/OO programming. However, I expect this will enable many important developments on the base.  For example, I’m a proponent of language-integrated techniques that make it easier to express parallel and asynchronous architectures.

 **Do you have any advice for up-and-coming programmers?**

Learn F#, Python, Prolog, Haskell, C# and Erlang!

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`haskell <haskell>` 

*  :ref:`perl <perl>` 

*  `http://research.microsoft.com/en-us/labs/cambridge/ <http://research.microsoft.com/en-us/labs/cambridge/>`_ 

*  `http://www.hubfs.net/ <http://www.hubfs.net/>`_ 

*  :ref:`python <python>` 

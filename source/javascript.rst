.. _javascript:

The A-Z of Programming Languages: JavaScript
============================================
Brendan Eich created JavaScript in 1995 with the aim to provide a "glue language" for Web designers and part time programmers. It has grown to become one of the most widely used languages on the planet.
* Naomi Hamilton(Computerworld)

* 31 July, 2008 21:04

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_8072.jpg
   :alt: Brendan Eich

Brendan Eich

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`ada <ada>` [2]on the Ada 1995 and 2005 revisions, Microsoft about its server-side script engine :ref:`asp <asp>` [3], :ref:`bash <bash>` [4]about his experience maintaining Bash, :ref:`c++ <c++>` [5]of C++ fame, to :ref:`forth <forth>` [6]about the design and development of Forth, a chat with the irreverent :ref:`intercal <intercal>` [7]about the development and uses of INTERCAL, Stephen C. Johnson on YACC, Modula-3 design committee member, :ref:`modula-3 <modula-3>` [8]and :ref:`d <d>` [9]about D

In this interview we chat to **Brendan Eich**, creator of JavaScript and Chief Technology Officer of Mozilla Corporation. Eich details the development of JS from its inception at Netscape in 1995, and comments on its continued popularity, as well as what he believes will be the future of client-side scripting languages on the Web.

Please note that we are no longer following exact alphabetical order for this series, due to popular demand. If you'd like to submit any suggestions for programming languages or language authors that you'd like to see covered, please emailnaomi@computerworld.com.au.

 **What prompted the development of JavaScript?**

I've written about the early history on my blog: `http://Weblogs.mozillazine.org/roadmap/archives/2008/04/popularity.html <http://Weblogs.mozillazine.org/roadmap/archives/2008/04/popularity.html>`_ [10]

I joined Netscape on 4 April 1995, with the goal of embedding the Scheme programming language, or something like it, into Netscape's browser. But due to requisition scarcity, I was hired into the Netscape Server group, which was responsible for the Web server and proxy products. I worked for a month on next-generation HTTP design, but by May I switched back to the group I'd been recruited to join, the Client (browser) team, and I immediately started prototyping what became JavaScript.

The impetus was the belief on the part of at least Marc Andreessen and myself, along with Bill Joy of Sun, that HTML needed a "scripting language", a programming language that was easy to use by amateurs and novices, where the code could be written directly in source form as part of the Web page markup. We aimed to provide a "glue language" for the Web designers and part time programmers who were building Web content from components such as images, plugins, and Java applets. We saw Java as the "component language" used by higher-priced programmers, where the glue programmers -- the Web page designers --  would assemble components and automate their interactions using JS.

In this sense, JS was analogous to Visual Basic, and Java to C++, in Microsoft's programming language family used on Windows and in its applications. This division of labor across the programming pyramid fosters greater innovation than alternatives that require all  programmers to use the "real" programming language (Java or C++)  instead of the "little" scripting language.

 **So was there a particular problem you were trying to solve?**

The lack of programmability of Web pages made them static, text-heavy, with at best images in tables or floating on the right or left. With a scripting language like JS that could touch elements of the page, change their properties, and respond to events, we envisioned a much livelier Web consisting of pages that acted more like applications.

Indeed, some early adopters, even in late 1995 (Netscape 2's beta period), built advanced Web apps using JS and frames in framesets, prefiguring the "Ajax" or "Web 2.0" style of development. But machines were slower then, JS had a relatively impoverished initial set of browser APIs, and the means to communicate with servers generally involved reloading whole Web pages.

 **How did JavaScript get its name given that it's essentially unrelated to the Java programming language?**

See my blog post, linked above.

 **Why was JS originally named Mocha and then LiveScript?**

Mocha was Marc Andreessen's code name, but Netscape marketing saw potential trademark conflicts and did not prefer it on other grounds. They had a "live" meme going in their naming (LiveWire, LiveScript, etc.). But the Java momentum of the time (1995-1996) swept these before it.
-

----- **How does JavaScript differ from ECMAScript?**

ECMA-262 Edition 3 is the latest ECMAScript standard. Edition 1 was based on my work at Netscape, combined with Microsoft's reverse-engineering of it (called JScript) in IE, along with a few other workalikes from Borland and a few other companies.

The 3rd Edition explicitly allows many kinds of extensions in its Chapter 16, and so JavaScript means more than just what is in the  standard, and the language is evolving ahead of the standard in  implementations such as Mozilla's SpiderMonkey and Rhino engines  (SpiderMonkey is the JS engine in Firefox).

The Ecma standard codifies just the core language, not the DOM, and many people think of the DOM as "JavaScript".

 **Do you believe that the terms JavaScript and JScript can or should be used interchangeably?**

JScript is not used, much or at all in cross-browser documentation and books, to refer to the language. JavaScript (JS for short) is what all the books use in their titles, what all the developer docs and conferences use, etc. It's the true name, for better and worse.

 **Were there any particularly hard/annoying problems you had to overcome in the development of the language?**

Yes, mainly the incredibly short development cycle to prove the concept, after which the language design was frozen by necessity. I spent about ten days in May 1995 developing the interpreter, including the built-in objects except for the Date class (Ken Smith of Netscape helped write that by translating Java's java.util.Date class to C, unintentionally inheriting java.util.Date's Y2K bugs in  the process!)

I spent the rest of 1995 embedding this engine in the Netscape browser and creating what has become known as the "DOM" (Document Object Model), specifically the "DOM level 0": APIs from JS to control windows, documents, forms, links, images, etc., and to respond to events and run code from timers.

I was the lone JS developer at Netscape until mid-1996.

 **What is the most interesting program that you've seen written with JavaScript?**

 `TIBET <http://www.technicalpursuit.com/>`_ [11]was an early, ambitious framework modeled on `Smalltalk <http://www.technicalpursuit.com>`_ [12].

There are amazing things in JS nowadays, including HotRuby (see `http://ejohn.org/blog/ruby-vm-in-javascript/ <http://ejohn.org/blog/ruby-vm-in-javascript/>`_ [13]for more -- this runs Ruby bytecode entirely in JS in the browser)  and a Java VM (Orto, see `http://ejohn.org/blog/running-java-in-javascript/ <http://ejohn.org/blog/running-java-in-javascript/>`_ [14]but beware: I'm not sure how much of the Java VM is implemented in JS  -- still, it's by all accounts an impressive feat.)

We are seeing more games, both new and ported from other implementations, as well:

 `http://blog.nihilogic.dk/2008/04/super-mario-in-14kb-javascript.html <http://blog.nihilogic.dk/2008/04/super-mario-in-14kb-javascript.html>`_ [15] `http://canvex.lazyilluminati.com/83/play.xhtml <http://canvex.lazyilluminati.com/83/play.xhtml>`_ [16]

And John Resig's port of the `Processing <http://ejohn.org/blog/processingjs/>`_ [17]visualization language takes the cake.

 **And what's the worst?**

I couldn't possibly pick one single worst JS program. I'll simply say that in the old days, JS was mainly used for annoyances such as pop-up windows, status bar scrolling text, etc. Good thing browsers such as Firefox evolved user controls, with sane defaults, for these pests. Netscape should have had such options in the first place.
-

-----

 **Have you ever seen the language used in a way that was not originally intended? If so, what was it? And did it or didn't it work?**

The Java VM (Orto) mentioned above is one example. I did not intend JS to be a "target" language for compilers such as Google Web Toolkit (GWT) or (before GWT) HaXe and similar such code generators, which take a different source language and produce JS as the "object" or  
"target" executable language.

The code generator approach uses JS as a "safe" mid-level intermediate language between a high-level source language written on  the server side, and the optimized C or C++ code in the browser that implements JS. This stresses different performance paths in the JS engine code, and potentially causes people to push for features in the Ecma standard that are not appropriate for most human coders.

JS code generation by compilers and runtimes that use a different source language does seem to be "working", in the sense that JS performance is good enough and getting better, and everyone wants to  maximize "reach" by targeting JS in the browser. But most JS is hand-coded, and I expect it will remain so for a long time.

 **It seems that many cross-site scripting exploits involve JavaScript. How do you feel about this? Are there plans to solve some of these problems?**

Yes, we have plans to address these, both through the standards bodies including the W3C, and through content restrictions that Web developers can impose at a fine grain. See the document:

 `http://www.gerv.net/security/content-restrictions/ <http://www.gerv.net/security/content-restrictions/>`_ [18]

and the Mozilla bug tracking work to implement these restrictions:

 `https://bugzilla.mozilla.org/show_bug.cgi?id=390910 <https://bugzilla.mozilla.org/show_bug.cgi?id=390910>`_ [19]

 **When do you expect the next version of JavaScript to be released? Do you have in mind any improvements that will be incorporated?**

I expect the 3.1 edition of the ECMA-262 standard will be done by the middle of 2009, and I hope that a harmonized 4th edition will follow within a year. It's more important to me (and I believe to almost everyone on the committee) that new editions of the specification be proven by multiple interoperating prototype implementations, than the specs be rushed to de-jure approval by a certain date. But the 3.1 effort seems achievable in the near term, and a harmonized major 4th edition should be achievable as a compatible successor in a year or two.

The improvements in the 3.1 effort focus on bug fixes, de-facto standards developed in engines such as SpiderMonkey (e.g. getters and setters) and reverse-engineered in other browsers, and affordances for defining objects and properties with greater integrity (objects that can't be extended, properties that can't be overwritten, etc.).

The improvements for the harmonized major edition following 3.1 simply build on the 3.1 additions and focus on usability (including new syntax), modularity, further integrity features, and in general, solutions to "programming in the large" problems in the current language.
-

-----

 **How do you feel about the place of JavaScript in Web 2.0?**

It's clear JS was essential to the "Ajax" or "Web 2.0" revolution. I would say Firefox, Safari, and renewed browser competition, and the renewed Web standards activities they spawned, were also important.

Real programs run in browsers too, and they are written in JS

But JS had to be sufficiently capable as a precondition for all of this progress to occur, even in the older Internet Explorer browser  versions (IE5.5, IE6), which were barely maintained by Microsoft for  the first five years of the new millennium. So JS was the tap root.

 **How do you feel about all the negative vibes expressed towards JavaScript over the years?**

These "vibes" seem to me to be a mix of:

* Early objections to the idea of a scripting language embedded in HTML.

* Appropriate rejection of the annoyance features JS enabled (and lack of sane controls, e.g. over pop-ups, until browsers such as Firefox came along).

* Confusion of DOM incompatibilities among browsers, which caused developer pain, with the generally more compatible JS implementations, which caused much less (but non-zero) pain.

* And of course, some people still feel negatively about the Netscape  marketing scam of naming the language JavaScript, implying a connection with Java, if not intentionally sowing confusion between JS and Java (for the record, I don't believe anyone at Netscape intended to sow such confusion).

These negative vibes are understandable. JS is the only example of a programming language that must interoperate at Web scale (wider than any other platform), on multiple operating systems and in many competing browsers. Other programming languages supported by browser  
plugins come from single vendors, who can control interoperation better by single-sourcing the implementation. Therefore JS and the DOM it controls have been a rough interoperation ride for Web developers.

It did not help that Netscape and Microsoft fought a browser war that forced premature standardization after a furious period of innovation, and which ended with way too many years of neglect of JS  and other Web standards under the IE monopoly.

On the up side, many developers profess to like programming in JS, and it has experienced a true renaissance since 2004 and the advent of "Web 2.0" or "Ajax" programming.

 **What do you think the future impact of JavaScript and other client-side scripting languages will be on the Web?**

I think JavaScript will be the default, and only obligatory, programming language in browsers for a while yet. But other languages will be supported, at first in one or another browser, eventually in cross-browser standard forms. Mozilla's browsers, including Firefox, optionally support C-Python integration, but you have to build it yourself and make sure your users have the C-Python runtime. We are working on better ways to support popular languages safely, compatibly, and with automated download of up-to-date runtime code.

It's clear the client side of the Web standards deserves programmability, as Marc Andreessen and I envisioned in 1995. The desktop and mobile computers of the world have plenty of cycles and storage to do useful tasks (more now than ever), without having to restrict their automation capabilities to submitting forms or sending  messages to real programs running on Web servers. Real programs run in browsers too, and they are written in JS.

The impact of JS is only increasing, as it becomes the standard for scripting not only in the browser, but on the desktop and in devices such as the iPhone.
-

-----

 **How do you feel about the recent release of JavaScript frameworks like SproutCore and Objective-J/Cappuccino? What impact do you think these will have on the future of Web applications?**

The Apple hype machine has certainly made some folks treat these as the second coming of Ajax. To me they are in a continuum of evolving JS libraries and frameworks, including Google GWT and such popular libraries as Dojo, JQuery, YUI, and Prototype. I don't particularly expect any one winner to take all, at least not for years, and then only in parts of the Web. On certain devices, of course, you may have essentially no choice, but the Web is wider than any one device, however popular.

 **Do you think that we are likely to see the death of desktop applications?**

No, but I think you will see more desktop applications written using Web technologies, even if they are not hosted in a Web server. And of course Web apps will continue to proliferate. With the evolution of JS and other browser-based Web standards, we'll see Web apps capable of more interactions and performance feats that formerly could be done only by desktop apps. We are already seeing this with offline support, canvas 2D and 3D rendering, etc. in the latest generation of  browsers.

 **How do you see the increasing popularity of plugins like Flash affecting the popularity of JavaScript?**

Flash is doing its part to be a good "Ajax" citizen, to be scriptable from JS and addressable using URLs -- to be a component on the page along with other components, whether plugins, built-in objects such as images or tables, or purely JS objects. The open Web levels everything upward, and militates against single-vendor lock-in. You can see this in how Flash has evolved to play well in the Web 2.0  world, and Microsoft's Silverlight also aims to integrate well into the modern Web-standards world.

People fear a return to proprietary, single-vendor plugins controlling the entire Web page and user experience, but I doubt that will happen all over the Web.

First, Web standards in the cutting edge browsers are evolving to compete with Flash and Silverlight on video, animation, high-performance JS, and so on.

Second, no Web site will sacrifice "reach" for "bling", and plugins always lack reach compared to natively-implemented browser Web standards such as JS. Users do not always update their plugins, and  users reject plugins while continuing to trust and use browsers.

 **Where do you envisage JavaScript's future lying?**

Certainly in the browser, but also beyond it, in servers and as an end-to-end programming language (as well as in more conventional "desktop" or "operating system" scripting roles).

 **Do you still think that (as you once said): "ECMAScript was always an unwanted trade name that sounds like a skin disease"?**

I don't think about this much, but sure: it's not a desired name and it does sound a bit like eczema. 
-

-----

 **Do you still expect ECMA-262 to be ready by October 2008? Do you expect the new version to be backwards incompatible at all?**

If you mean the 4th Edition of ECMA-262, no: we do not expect that in 2008, and right now the technical committee responsible (Ecma TC39) is working together to harmonize proposals for both a near-term (Spring 2009) "3.1" edition of ECMAScript, and a more expansive (but  
not too big) follow-on edition, which we've been calling the 4th edition.

 **Has the evolution and popularity of JS surprised you in anyway?**

The popularity has surprised me. I was resigned for a long time to JS being unpopular due to those annoying popups, but more: due to its unconventional combination of functional and prototype-based object programming traditions. But it turns out that programmers, some who  started programming with JS, others seasoned in the functional and dynamic OOP languages of the past, actually like this unconventional mix.

 **What are you proudest of in JavasScript's initial development and continuing use?**

The combination of first-class functions and object prototypes. I would not say it's perfect, especially as standardized (mistakes were added, as well as amplified, by standardization). But apart from the  glitches and artifacts of rushing, the keystone concepts hang together pretty well after all these years.

 **Where do you see computer programming languages heading in the future, particularly in the next 5 to 20 years?**

There are two big problems facing all of us which require better programming languages:

* Multicore/massively-parallel computers that are upon us even now, on the desktop, and coming soon to mobile devices. Computer scientists are scrambling to make up for the lack of progress in  the last 15 years making parallel computing easier and more usable. JS has its role to play in addressing the multicore world, starting with relatively simple extensions such as Google Gears' worker pools --  "shared nothing" background threads with which browser JS communicates by sending and receiving messages.

* Security. A programming language cannot create or guarantee security by itself, since security is a set of "end to end" or  "system" properties, covering all levels of abstraction, including above and below the language. But a programming language can certainly give its users better or worse tools for building secure systems and proving facts about those security properties that can be  expressed in the language.

 **Do you have any advice for up-and-coming programmers?**

Study the classics: Knuth, Wirth, Hoare. Computer science is a wheel, which rotates every 10-20 years in terms of academic research focus.  Much that was discovered in the early days is still relevant. Of course great work has been done more recently, but from what I can tell, students get more exposure to the recent stuff, and almost none to the giants of the past.

 **Is there anything else you'd like to add?**

Not now, I'm out of time and have to get back to work!

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  `http://Weblogs.mozillazine.org/roadmap/archives/2008/04/popularity.html <http://Weblogs.mozillazine.org/roadmap/archives/2008/04/popularity.html>`_ 

*  `http://www.technicalpursuit.com/ <http://www.technicalpursuit.com/>`_ 

*  `http://www.technicalpursuit.com <http://www.technicalpursuit.com>`_ 

*  `http://ejohn.org/blog/ruby-vm-in-javascript/ <http://ejohn.org/blog/ruby-vm-in-javascript/>`_ 

*  `http://ejohn.org/blog/running-java-in-javascript/ <http://ejohn.org/blog/running-java-in-javascript/>`_ 

*  `http://blog.nihilogic.dk/2008/04/super-mario-in-14kb-javascript.html <http://blog.nihilogic.dk/2008/04/super-mario-in-14kb-javascript.html>`_ 

*  `http://canvex.lazyilluminati.com/83/play.xhtml <http://canvex.lazyilluminati.com/83/play.xhtml>`_ 

*  `http://ejohn.org/blog/processingjs/ <http://ejohn.org/blog/processingjs/>`_ 

*  `http://www.gerv.net/security/content-restrictions/ <http://www.gerv.net/security/content-restrictions/>`_ 

*  `https://bugzilla.mozilla.org/show_bug.cgi?id=390910 <https://bugzilla.mozilla.org/show_bug.cgi?id=390910>`_ 

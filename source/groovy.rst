.. _groovy:

The A-Z of programming languages: Groovy
========================================
Our series on the most popular programming languages continues as we chat to Groovy’s Project Manager, Guillaume Laforge
* Dahna McConnachie(Computerworld)

* 14 September, 2009 12:13

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_19880.jpg
   :alt: Groovy’s Project Manager, Guillaume Laforge

Groovy’s Project Manager, Guillaume Laforge

 *Computerworld* is undertaking a series of investigations into interesting programming languages. In the past we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8]and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`falcon <falcon>` [13]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [14], Walter Bright :ref:`d <d>` [15], Brendan Eich on :ref:`javascript <javascript>` [16], Anders Hejlsberg on :ref:`c# <c#>` [17], Guido van Rossum on :ref:`python <python>` [18], Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [19], John Ousterhout on :ref:`tcl <tcl>` [20], Joe Armstrong on Erlang and Rich Hickey on Clojure. We most recently spoke to Martin Odersky about the darling of Web 2.0 start-ups and big corporates alike, Scala.

In this interview `Groovy <http://groovy.codehaus.org/>`_ [21]Project Manager, Guillaume Laforge, tells the development story behind the language and why he thinks it is grooving its way into enterprises around the world. Groovy, he says, is ultimately a glue that makes life easier for developers — and it has nothing to do with Jazz.

 *If you wish to submit any suggestions for programming languages or language authors you would like to see covered, please emailkathryn@computerworld.com.au*----

 **How did you come up with the name Groovy? Is it a reference to counter culture or are you a jazz fan?**

There's a little known story about the invention of the name!

Back in the day, in 2003, after suffering with Java and loving the features available in dynamic languages like Ruby, Python and Smalltalk, lots of questions arose of the form of, "Wouldn't it be 'groovy' if Java had this or that feature and you could replace said feature with closures, properties, metaprogramming capabilities, relaxed Java syntax?” and more.

When it came to choosing a name, it was obvious that a new language with all those great features would have to be called ‘Groovy’! So it's not really a reference to counter culture, nor about jazz, but just about the dream of having a language close enough to Java, but more powerful and expressive. That's how Groovy came to life.

 **What are the main differences between Groovy and other well-known dynamic languages like Ruby, Python and Perl?**

The key differentiator is the seamless integration with the Java platform underneath. It's something no other languages provide, even alternative languages for the JVM (Java Virtual Machine), or at least not up to the level that Groovy does.

First of all, the grammar of the language is derived from the Java 5 grammar, so any Java developer is also a Groovy developer in the sense that the basic syntax is already something he would know by heart. But obviously Groovy provides various syntax sugar elements beyond the Java grammar. The nice aspect of this close relationship is that the learning curve for a Java developer is really minimal.

Even at the level of the APIs, aspects such as the object orientation and the security model are all just what you would be accustomed to with Java. There's really no impedance mismatch between Groovy and Java. That's why lots of projects integrate Groovy, or why companies adopt the Grails web framework.

 **What led you to develop Groovy — was it to solve a particular problem or carry out a particular function that you could not do in another language?**

Back in 2003, I was working on project that was a kind of application generator where there was a Swing designer User Interface (UI) to define a meta-model of the application you wanted to build, and you could define the tables, columns, and UI widgets to represent the data and layout. This meta-model was deployed on a web application that interpreted that model to render a live running application. It was a pretty powerful system.

The project also allowed some customised UI widgets to render certain fields, like autocomplete fields and such, and you could develop your own widgets. But those widgets had to be developed in Java, compiled into bytecode, archived in a JAR file, and — the biggest drawback of all — you then had to deploy a new version of the web application to take this new widget into account.

The obvious problem was that all the customers using those generated applications had to stop using them for a moment, for a maintenance window, so that only one customer could have access to that new widget he needed. It was at that point that I decided a kind of scripting language would be useful to develop those widgets, and have them stored in the meta-model of the applications, and interpreted live in the running server. 
-

-----

 **What was the primary design goal for the language?**

Groovy's design goal has always been to simplify the life of developers.

We borrowed interesting features from other languages to make Groovy more powerful, but have [always had a] strong focus on a total seamless integration with Java. Because of these goals, Groovy is often used as a superglue for binding, wiring, or configuring various application components together. When we created the language, this glue aspect was clearly one of the primary functions.

 **How is it most often used?**

Companies using Groovy usually don't write full applications in Groovy, but rather mix Groovy and Java together. So Groovy is often used as a glue language for binding parts of applications together, as a language for plugins or extension points, as a more expressive way to create unit and functional tests, or as a business language. It's very well suited for defining business rules in the form of a Domain-Specific Language.

 **How widely is Groovy being used and where?**

Groovy is very often the language of choice when people need to integrate and use an additional language in their applications, and we know of lots of mission-critical applications are relying on Groovy.

For instance, Groovy is often used in financial applications for its expressivity and readability for writing business rules, and also because if its usage of BigDecimal arithmetics by default which allows people to do exact calculations on big amounts of money without important rounding errors. For example, there is a big insurance company in the US that used Groovy for writing all its insurance policy risk calculation engine. There is also a European loan granting platform working with 10 per cent of all the European banks, dealing with one billion Euros worth of loans every month, which uses Groovy for the business rules for granting loans and as the glue for working with web services.

The financial sector is not the sole one: Groovy is also being used by biomedical and genetics researchers, by CAD software and more.

 **How many developers work on Groovy?**

We currently have two full-time persons working on Groovy, plus a handful of super-active committers. We've got a second-tier of casual committers who focus on particular areas of the project. Groovy is a very active project that has seen a long list of committers and contributors over the course of its development.

 **Can you tell us a bit more about Grails (formerly Groovy on Rails) and is it, in your opinion, a good example of what can be done with Groovy?**

Grails is a highly productive web development stack. More than a mere Web framework, it provides an advanced integration of the best-of-breed open source software (OSS) components, such as Spring and Hibernate, to provide a very nice experience for developers using it, while also taking care of various other aspects like the project build, the persistence, a rich view layer and an extensible plugin system. Clearly, Grails leverages Groovy heavily, to bring productivity boosts to developers at every step of the project. Grails' choice of Groovy and all the other components it uses makes it a very compelling platform for high-traffic and complex applications. 
-

----- **What are some other interesting open source applications using Groovy?**

Griffon is based on the Grails foundations and extends Groovy's own Swing builder system to let you create complex and rich desktop applications in Swing. `Griffon <http://docs.codehaus.org/display/GROOVY/Griffon>`_ [22]is really to Swing development what Grails is for web development.

In the testing space, `Easyb <http://www.easyb.org>`_ [23]brings developers a DSL for Behavior-Driven-Development testing, and `Spock <http://spockframework.org/>`_ [24]provides some advanced testing and mocking techniques to unit testing. Let me also mention `Gradle <http://www.gradle.org>`_ [25], which is a very nice and advanced build system.

 **What are the biggest tasks you are currently working on with the language development?**

We always have two ongoing efforts at the same time: maintaining and evolving the current stable branch, as well as working and innovating on the development branch.

For instance, we've just released a minor version of Groovy 1.6 which solves some bugs and has some minor enhancements, and we have also just released a preview of the upcoming Groovy 1.7 full of new features. 
  
Groovy 1.7 will make it easier for extending the language through compile-time metaprogramming capabilities. It will also provide better assertion messages for unit tests, the ability to use annotations in more places in your programs and lots more.

 **Why did you choose an Apache License over other free and /or open licences?**

We felt that the Apache License was a great and open licence to use, so that anybody is free to embed, reuse, or even fork the language in whatever they see fit for their own needs, and integrate it in their own applications. The choice was also easy with some of the original committers coming from the Apache Software Foundation.

 **As it is in some ways a superset of Java, it would be easy for Java developers to learn, but what is the learning curve for developers without a Java background?**

Of course Groovy is easy to learn for Java developers, but thanks to its "scripting" aspects, it's still rather easy to learn for users coming from a different background.

As long as you're somewhat familiar with a language with a C-like syntax, it's simple to comprehend. There are of course some APIs to learn, as with any language and platform, but you can learn them as you need them without any upfront cost of learning. So even without a Java background, the learning curve isn't that stiff.

 **What is your favourite Groovy feature?**

This is a tricky question! There are really many aspects of the language that I love!

I guess if I had to choose just one, that would be Groovy's support for closures. With closures, you can start thinking differently about how you solve your everyday problems, or create complex algorithms. Closures give you an additional layer of abstraction for encapsulating code and behaviour, and even data (thanks to Groovy builders). Also, with various helper methods added to Java collections, in combination with closures, you've got the power of functional languages at your disposal.

 **What has been the greatest challenge in developing Groovy and how did you work around this?**

I would say that the two main challenges have been about a total seamless interoperability and integration with Java, as well as performance.

The former has always been part of our design goals, so we've always done our best to take care of all the bugs and keep up with the pace of Java itself (for instance when Java 5 introduced annotations, enums, and generics).

For the latter, we made sure that Groovy would be the fastest dynamic language available (both in and outside of the JVM). We used various techniques, such as 'call site caches' and related techniques. We're also very enthusiastic and optimistic about the upcoming JSR-292 'invokedynamic' bytecode instructions coming soon into the Java Virtual Machine, which should bring very significant performance boosts.

 **Do developers in corporate environments have trouble using non-standadised and relatively new languages like Groovy in the workplace?**

It depends, [but this can happen] in some cases. Groovy is an easy sell, as after all it's just another library to put on the classpath, and in some other cases it's more problematic as certain companies are really strict and avoid adding any other dependency in their projects, trying to mitigate any additional risk. Usually though, the various advantages Groovy brings help sell it to more conservative organisations.

Until recently, the tooling wasn't ideal either, but JetBrains with their incredible Groovy and Grails support in IntelliJ IDEA paved the way. We also have great support in NetBeans, and thanks to the SpringSource Eclipse team, the Eclipse plugin for Groovy is going to progressively catch up with the competitors. Groovy is now a much easier sell than it was a few years ago and a lot of companies trust Groovy for their advanced needs.

 **A Slashdot reader has said in a post months ago that Groovy is poised to convert the enterprise crowd. Do you agree with this statement?**

More and more companies are relying on Groovy for doing business — even critical apps dealing with large amounts of money. So clearly, Groovy is now a key asset to such companies and businesses. And the fact Groovy is very easy to learn and use, and is so well integrated with Java, makes it a nice fit for bringing more agility and power in your applications.

 **Where do you see Groovy heading in the future?**

This is a very good question! After each major release, we're wondering whether we will be able to add some new innovative and useful features to the language. And in the end, we always find something!

As I mentioned already, there are areas where we continue to innovate, like our compile-time metaprogramming techniques and our extended annotation support.

We're also considering certain features we find interesting in other languages and their respective APIs, for instance Erlang's actor concurrency model, pattern matching like in functional languages such as OCaml, or parser combinators from Haskel.

We always try to find new features that bring real value and benefits to our users.

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`tcl <tcl>` 

*  `http://groovy.codehaus.org/ <http://groovy.codehaus.org/>`_ 

*  `http://docs.codehaus.org/display/GROOVY/Griffon <http://docs.codehaus.org/display/GROOVY/Griffon>`_ 

*  `http://www.easyb.org <http://www.easyb.org>`_ 

*  `http://spockframework.org/ <http://spockframework.org/>`_ 

*  `http://www.gradle.org <http://www.gradle.org>`_ 

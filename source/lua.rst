.. _lua:

The A-Z of Programming Languages: Lua
=====================================
Professor Roberto Ierusalimschy offers an in-depth examination of what he believes to be the most successful programming language not born in a developed country.
* Naomi Hamilton(Computerworld)

* 11 September, 2008 20:29
Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Alfred v. Aho of :ref:`awk <awk>` [1]fame, S. Tucker Taft on the :ref:`ada <ada>` [2], Microsoft about its server-side script engine :ref:`asp <asp>` [3], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [4], Bjarne Stroustrup of :ref:`c++ <c++>` [5], and Charles H. Moore about the design and development of :ref:`forth <forth>` [6]. We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [7], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [8], Luca Cardelli on :ref:`modula-3 <modula-3>` [9], Walter Bright :ref:`d <d>` [10], Brendan Eich on :ref:`javascript <javascript>` [11], and most recently to Guido van Rossum about :ref:`python <python>` [12].

This time we chat to Prof. Roberto Ierusalimschy about the design and development of Lua. Prof. Ierusalimschy is currently an Associate Professor in the `Pontifical Catholic University <http://www.puc-rio.br/>`_ [13]of Rio de Janeiro's Informatics Department where he undertakes research on programming languages, with particular focus on scripting and domain specific languages.
 
Prof. Ierusalimschy is currently supported by the Brazilian Council for the Development of Research and Technology as an independent researcher, and has a grant from Microsoft Research for the development of `Lua.Net <http://www.tecgraf.puc-rio.br/~rcerq/luadotnet>`_ [14]. He also has a grant from `Finep <http://www.finep.gov.br/>`_ [15]for the development of libraries for Lua.

 *Please note that due to popular demand we are no longer following alphabetical order for this series. If you wish to submit any suggestions for programming languages or language authors you would like to see covered, please emailnaomi@computerworld.com.au.*

 **What prompted the development of Lua? Was there a particular problem you were trying to solve?**

In `our paper <http://www.tecgraf.puc-rio.br/~lhf/ftp/doc/hopl.pdf>`_ [16]for the Third ACM History of Programming Languages Conference we outline the whole story about the origins of Lua.

To make a long story short, yes, we did develop Lua to solve a particular problem. Although we developed Lua in an academic institution, Lua was never an "academic language", that is, a language to write papers about. We needed an easy-to-use configuration language, and the only configuration language available at that time (1993) was `Tcl <http://en.wikipedia.org/wiki/Tcl>`_ [17]. Our users did not consider Tcl an easy-to-use language. So we created our own configuration language.

 **How did the name Lua come about?**

Before Lua I had created a language that I called SOL, which stood for "Simple Object Language" but also means "Sun" in Portuguese. That language was replaced by Lua (still nameless at that time). As we perceived Lua to be "smaller" than Sol, a friend suggested this name, which means "moon" in Portuguese.

 **Were there any particularly difficult problems you had to overcome in the development of the language?**

No. The first implementation was really simple, and it solved the problems at hand. Since then, we have had the luxury of avoiding hard/annoying problems. That is, there have been many problems along the way, but we never had to overcome them; we have always had the option to postpone a solution.

Some of them have waited several years before being solved. For instance, since Lua 2.2, released in 1995, we have wanted lexical scoping in Lua, but we didn’t know how to implement it efficiently within Lua's constraints. Nobody did. Only with Lua 5.0, released in 2003 did we solve the problem, with a novel algorithm.

 **What is the most interesting program that you've seen written with Lua and why?**

I have seen many interesting programs written in Lua, in many different ways. I think it would be unfair to single one out. As a category, I particularly like table-driven programs, that is, programs that are more generic than the particular problem at hand and that are configured for that particular problem via tables.

-

-----

 **Have you ever seen the language used in a way that was not originally intended? If so, what was it, and did it work?**

For me, one of the most unexpected uses of Lua is "inline::Lua", a "Perl extension for embedding Lua scripts into Perl code". I always thought that it was a weird thing to use Lua for scripting a scripting language. It does work, but I do not know how useful it really is.

In a broader sense, the whole use of Lua in games was unexpected for us. We did not create Lua for games, and we had never thought about this possibility before. Of course, with hindsight it looks an obvious application area, and we are very happy to be part of this community. And it seems to be working ;)

 **You've mentioned the usage of Lua in games already and it's used for scripting in some very famous games such as World of Warcraft (WoW). Have you played WoW or written scripts in it?**

No :) I have never played that kind of games (RPG). Actually, until recently I had no knowledge at all about WoW add ons (what they call their scripts). In the last Lua Workshop, Jim Whitehead gave a nice talk about WoW add ons; it was only then that I learned the little I currently know about them.

 **Do you think that the use of Lua in computer games has allowed people to discover the language who may not have done so otherwise?**

Sure. I guess more people have learned about Lua through games than through any other channel.

 **Do you think that computer games have a part to play in promoting programming languages?**

Certainly games are an important way to introduce people to programming. Many kids start using computers to play games, so it seems natural to use this same stimulus for more advanced uses of computers and for programming. However, we should always consider other routes to stimulate people to learn programming. Not everybody is interested in games. In particular, girls are much less motivated by games (or at least by most games) than boys.

 **Over 40 percent of Adobe Lightroom is believed to be written in Lua. How do you feel about this?**

Proud :)

 **Why do you think that Lua has been such a popular toolkit for programs like Adobe lightroom and various computer games?**

There is an important difference between Adobe Lightroom and games in general. For most games, I think the main reason for choosing Lua is its emphasis on scripting. Lightroom has made a different use of Lua, as a large part of the program is written in Lua. For Adobe, a strong reason for choosing Lua was its simplicity. In all cases, however, the easiness of interfacing with C/C++ is quite important, too.

 **In the Wikipedia article on Lua, it notes: *“…Lua’s creators also state that Lisp and Scheme with their single, ubiquitous data structure mechanism were a major influence on their decision to develop the table as the primary data structure of Lua.”*Is this true, and why was the list such a powerful influence?**

Scheme has been a major source of inspiration for us. This is a language I would love to have created. And it is amazing what we can do using only lists.  However, lists do not seem so appropriate for a language where the main paradigm is imperative, such as Lua. Associative arrays have proved to be quite a flexible mechanism.

-

-----

 **Do you think that the MIT license has allowed the language to grow in popularity?**

Sure. In the first year that we released Lua outside PUC, we adopted a more restricted license. Basically it was free for academic use but not for commercial use. It was only after we changed for a more liberal license that Lua started to spread. I guess that even a GPL-like license would hurt its spread. Most game companies are very secretive about their technologies. Sometimes, it is hard to know who is actually using Lua!

 **Do you think Lua has any significant flaws?**

It is difficult for us to point to any clear flaw, otherwise we would have corrected it. But, like with any other language, the design of Lua involves many compromises. For instance, several people complain that its syntax is too verbose, but that syntax is friendlier to non programmers (such as gamers). So, for some people the syntax is flawed, for others it is not. Similarly, for some programmers even the dynamic typing is a flaw.

 **How does or will 5.1.3 differ from previous versions of Lua?**

In Lua, 5.1.x are only bug-fix releases. So, 5.1.3 differs from 5.1 only in fixing the few bugs found in 5.1.2. The next "real" release, 5.2, is still somewhat far on the horizon. Lua evolved somewhat quickly until version 5 (some users would say too quickly), so now we would like to allow some time for its culture to stabilize. After all, each new version automatically outdates current books, extra documentation, and the like. So, we are currently not planning big changes for the
language in the near future.

 **Has corporate sponsorship of the language influenced the way Lua has developed in any way?**

The corporate sponsorship program is still very recent; it started in June 2008. But it has no influence whatsoever in the development of Lua. The program offers only visibility to the sponsor. Sponsors do not have any exclusive channel to express their wishes about the language, and we do not feel obliged in any way to accept their suggestions.

 **What impact do you feel the growth of open source has had on Lua?**

A huge impact! The development of Lua does not follow the typical development structure of open source projects however; apart from this Lua is a typical product of the open source era. We have a strong community and we get lots of feedback from this community. Lua would never achieve its popularity and its quality if it was not open source.

 **Why do you think Lua is a popular choice for providing a scripting interface within larger applications?**

Except for Tcl, Lua is the only language designed since day one for that specific purpose. As I said before, any language design generally has lots of compromises. Lua's compromises are directed at being good for scripting, that is, for controlling applications. Most other languages have different compromises, such as having more complete libraries, better integration with the operating system, or a more rigid object system.

 **One place where Lua seems to be used widely is in Sysadmin tools such as Snort. What impact do you think Sysadmins have on a language?**

On some languages Sysadmins may have a big impact. Perl, for instance, got very strongly influence from that area. But Lua has had very little impact from Sysadmins. That's because their usage and their goals are quite different.

For instance, consider an application like Snort or Wireshark.  The goal of Perl is to allow you to implement the entire application in Perl. For that, the language must provide all system primitives that those tools may ever need.  Lua, on the other hand, emphasizes multi-language development. The primitives specific for the application are provided by the application itself, not by Lua.

Also, Sysadmin support frequently conflicts with portability - a main goal in Lua. Again, a Sysadmin tool should provide access to all facilities of the system, no matter how idiosyncratic they are. Lua has some libraries to allow such access, but they are not built-in. And even those libraries try to present system facilities in a more standard, less idiosyncratic way.

-

-----

 **What languages do you currently work with?**

The language I work with most nowadays is C, both in the implementation of Lua and in some libraries. I also use Lua frequently, for tasks such as text processing and system automation. In the past I have worked with several different languages: I have substantial programming with Fortran, Mumps, SNOBOL, Smalltalk, Scheme, Pascal and C++, plus assemblers for various machines.

 **Is there a particular tool which you feel could really do with having Lua embedded in it?**

It is hard to think about a tool that would not benefit from an embedded scripting facility, and Lua is an obvious choice for that support.

 **In your opinion, what lasting legacy has Lua brought to computer development?**

I think it is far too early to talk about any "lasting" legacy from Lua. But I think Lua has had already some impact on language design. The notion of co-routines, as implemented in Lua, has brought some novelties to that area. Also the object model adopted by Lua, based in delegation, is often cited. In the implementation aspect, Lua was a big showcase for register-based virtual machines.

Lua is also a showcase for the idea that "small is beautiful", that software does not need to be bloated to be useful.

 **Where do you envisage Lua's future lying?**

Scripting. It is a pity that the term "scripting language" is becoming a synonym for "dynamic language". A scripting language, as its name implies, is a language that is mainly used for scripting. The origins of the name are the shell languages that have been used to script other programs. Tcl enlarged it for scripting a program, but later people started applying the term for languages like Perl or Python, which are not scripting languages (in that original meaning) at all. They are dynamic languages. For real scripting, Lua is becoming a dominant language.

 **What are you most proud of in terms of the language's initial development and continuing use?**

I am very proud that Lua achieved all this popularity given where it came from. From all languages ever to achieve some level of popularity, Lua is the only one not created in a developed country. Actually, besides Lua and Ruby, I guess all those languages were created in the US or Western Europe.

 **Where do you see computer programming languages heading in the next 5 to 20 years?**

The easy part of predicting the next 20 years is that it will take a long time to be proved wrong. But we may try the reverse: where were we 20 years back, in the 80's?

I am old enough to remember the Fifth Generation project. Many people claimed at that time that in the far future (which is now) we would all be programming in Prolog :) In the short term, Ada seemed set to become the dominant language in most areas. It is interesting that the seeds of relevant changes in programming languages were already there. Object-oriented programming was on the rise; OOPSLA was created in 1986. But at that time no one would have bet on C++ overcoming Ada.

So, I would say that the seeds for the next 20 years are already out there, but they are probably not what people think or expect.

 **Do you have any advice for up-and-coming programmers?**

Learn Lua :)

More seriously, I really subscribe to the idea that "if the only tool you have is a hammer, you treat everything like a nail". So, programmers should learn several languages and learn how to use the strengths of each one effectively. It is no use to learn several languages if you do not respect their differences.

References
*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`python <python>` 

*  `http://www.puc-rio.br/ <http://www.puc-rio.br/>`_ 

*  `http://www.tecgraf.puc-rio.br/~rcerq/luadotnet <http://www.tecgraf.puc-rio.br/~rcerq/luadotnet>`_ 

*  `http://www.finep.gov.br/ <http://www.finep.gov.br/>`_ 

*  `http://www.tecgraf.puc-rio.br/~lhf/ftp/doc/hopl.pdf <http://www.tecgraf.puc-rio.br/~lhf/ftp/doc/hopl.pdf>`_ 

*  `http://en.wikipedia.org/wiki/Tcl <http://en.wikipedia.org/wiki/Tcl>`_ 

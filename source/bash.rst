.. _bash:

The A-Z of Programming Languages: BASH/Bourne-Again Shell
=========================================================
When the Bourne Shell found its identity
* Naomi Hamilton(Computerworld)

* 30 May, 2008 09:43

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_5489.jpg
   :alt: Chet Ramey, primary maintainer of Bash, atop the dome of Santa Maria del Fiore in Florence

Chet Ramey, primary maintainer of Bash, atop the dome of Santa Maria del Fiore in Florence

Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we :ref:`awk <awk>` [1], and in this article we chat to Chet Ramey about his experience maintaining Bash.

Bash, or the Bourne-Again Shell is a Unix shell created in 1987 by Brian Fox. According to Wikipedia, the name is a pun on an earlier Unix shell by Stephen Bourne (called the Bourne shell), which was distributed with Version 7 Unix in 1978.

In 1990, Chet Ramey, Manager of the Network Engineering and Security Group in Technology Infrastructure Services at Case Western Reserve University, became the primary maintainer of the language.

Computerworld tracked down Ramey to find out more.

 **How did you first become involved with Bash?**

In 1989 or so, I was doing network services and server support for [Case Western Reserve] University (CWRU), and was not satisfied with the shells I had available for that work.  I wasn't really interested in using sh for programming and csh/tcsh for interactive use, so I began looking around for a version of sh with the interactive features I wanted (job control, line editing, command history, filename completion, and so on.)

I found a couple of versions of the SVR2 shell where those features had been added (credit to Doug Gwyn, Ron Natalie, and Arnold Robbins, who had done the work). These were available to CWRU because we were Unix source licensees, but I had trouble with them and couldn't extend them the way I wanted.  Ken Almquist was writing ASH, but that had not been released, and there was a clone of the 7th edition shell, which eventually became PDksh, but that did not have the features I wanted either.

Brian Fox had begun writing BASH and readline (which was not, at that time, a separate library) the year before, when he was an employee of the FSF. The story, as I recall it, was that a volunteer had come forward and offered to write a Bourne Shell clone. After some time, he had produced nothing, so Richard Stallman directed Brian to write a shell. Stallman said it should take only a couple of months.

I started looking again, and ended up finding a very early version of Bash.  I forget where I got it, but it was after Brian had sent a copy to Paul Placeway from Ohio State -- Paul had been the tcsh maintainer for many years, and Brian asked him to help with the line editing and redisplay code. I took that version, made job control work and fixed a number of other bugs, and sent my changes to Brian.  He was impressed enough to begin working with me, and we went on from there.

I fixed many of the bugs people reported in the first public versions of BASH and fed those fixes back to Brian.  We began working together as more or less co-maintainers, and when Brian moved on to other things, I still needed to support Bash for my local users, so I produced several
local releases.  Brian and I eventually merged those versions together, and when he moved away from Bash development, I took over.

-

-----

 **Did you work with Brian Fox before becoming the primary maintainer of the language?**

Brian and I worked together for several years before he moved on to other things.  The versions through BASH-1.13 were collaborative releases.

 **What is/was your working relationship with Brian like?**

Our working relationship was very good, especially considering we met in person only once, in 1990.  We were heavy users of UNIX `talk' and `ntalk', which allowed real-time two-way communication over the Internet back then, and made good use of email and the occasional long distance phone call.

We still stay in touch.

 **What prompted the making of Bash in the first place?**

When Richard Stallman decided to create a full replacement for the then-encumbered Unix systems, he knew that he would eventually have to have replacements for all of the common utilities, especially the standard shell, and those replacements would have to have acceptable licensing.

After a couple of false starts (as previously mentioned), he hired Brian Fox to write it.

They decided early on that they would implement the shell as defined by the Posix standard, and used that as a specification.

 **Was there a particular problem that the language aimed to solve?**

In BASH's case, the problem to be solved was a free software version of the Posix standard shell to be part of the GNU system.

The original version of the shell (Steve Bourne's version) was intended to overcome a number of the limitations of the Unix shell included in versions up to the sixth edition, originally written by Ken Thompson.

 **Why did you take over as the language's primary maintainer 3 years after Fox created the language?**

Brian wanted to move on to other things, and I was a developer willing to take it on and experienced with the code.  Brian and the FSF trusted me with the program's future.

 **What prompted the writing of the GNU Bash Reference Manual and the Bash Reference Manual?**

Any good heavily-used program needs good reference documentation, and BASH is no exception.  I originally wrote the documents to support my local users, and they were folded into "official" releases along the line.

-

-----

 **Is there a strong relationship between the original Bourne Shell and the Bourne-Again Shell?**

I'd say there is a linear relationship: the original Bourne Shell was very influential, the various System V shell releases preserved that heritage, and the Posix committee used those versions as the basis for the standard they developed.

Certainly the basic language syntax and built-in commands are direct descendants of the Bourne Shell's.  Bash's additional features and functionality build on what the Bourne shell provided.

As for source code and internal implementation, there's no relationship at all, of course.

 **What prompted the language's name: why was a pun created on the Bourne Shell?**

The FSF has a penchant for puns, and this one seemed appropriate, I suppose.  The name predates my involvement.

 **Have you faced any hard decisions in maintaining the language?**

The hardest decisions are the ones dealing with compatibility: how compatible to be with the versions of sh existing at various points throughout Bash's history; how compatible to be with the features from the Korn shell I considered valuable; where and how to differ from the Posix standard, and when to break backwards compatibility with previous Bash versions to correct mistakes I had made.

Some of the features implemented (and not implemented) required a lot of thought and consideration -- not how to implement them, but whether or not to invest the resources to do so.  Most of the Bash development over the past 15 years has been done by one person.

 **Are you still working with the language now?**

I am.  In fact, the next major release of Bash, Bash-4.0, should be out sometime this (Northern) summer.

 **What is the latest project you have used it for?**

I mostly use Bash for interactive work these days.  I use it to write some small system administration tools, but I don't do much system administration any more.

 **What is the most exciting piece of code (that you know of) ever written in Bash?**

That's hard to say.  Lots of interesting projects have been implemented as shell scripts, or sets of shell scripts.

I particularly like the various versions of the Bash debugger that were implemented completely as shell scripts.  That's pretty complex work.  I've seen entire web servers and other surprisingly substantial applications written as shell scripts.

-

-----

 **In your opinion, what lasting legacy has Bash brought to the web?**

I think Bash's legacy is as a solid piece of infrastructure, and the shell making millions of Linux, Mac OS X, and Solaris systems work every day.

As I recall, it was one of the first couple of programs Linus Torvalds made run on his early Linux kernels.

 **Where do you envisage Bash's future lying?**

Bash will continue to evolve as both an interactive environment and a programming language.  I'd like to add more features that allow interested users to extend the shell in novel ways.  The programmable completion system is an example of that kind of extension.

Bash's evolution has always been user-driven, so it will ultimately be up to the feature requests that come in.

 **Where do you see computer programming languages heading in the future, particularly in the next five to 20 years?**

I see increased dynamism, allowing programmers to do more and more complex things on the fly, especially over the Web.  The advances in hardware allow interpreted code to run faster today than compiled code on some systems available when I started work on Bash.

 **Do you have any advice for up-and-coming programmers?**

Find an area that interests you and get involved with an existing community.  There are free software projects in just about any area of programming.

The nuts-and-bolts -- which language you use, what programming environment you use, where you do your work -- are not as important as the passion and interest you bring to the work itself.

 **Is there anything else that you'd like to add?**

The free software community is still as vibrant today, maybe even more so, than when I first became involved.  There is still a lot of room for significant contributions; all it takes is an interested person with a good idea.

References
*  :ref:`awk <awk>` 

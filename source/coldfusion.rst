.. _coldfusion:

An interview with ColdFusion co-creator Jeremy Allaire
======================================================
In the latest installment of Computerworld's A-Z of programming languages we chat with ColdFusion co-creator, Jeremy Allaire
* Trevor Clarke(Computerworld)

* 05 November, 2009 15:32

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_21325.jpg

As part of our series of investigations into interesting programming languages *Computerworld*talks to one of the creators of web development tool, ColdFusion – Jeremy Allaire.

In the past we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8]and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], :ref:`falcon <falcon>` [13]creator Giancarlo Niccolai, Luca Cardelli on :ref:`modula-3 <modula-3>` [14], Walter Bright :ref:`d <d>` [15], Brendan Eich on :ref:`javascript <javascript>` [16], Anders Hejlsberg on :ref:`c# <c#>` [17], Guido van Rossum on :ref:`python <python>` [18], Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [19], John Ousterhout on :ref:`tcl <tcl>` [20], Joe Armstrong on Erlang and Rich Hickey on Clojure. We recently spoke to Martin Odersky about the darling of Web 2.0 start-ups and big corporates alike, Scala.

More recently, we heard from `Groovy <http://groovy.codehaus.org/>`_ [21]Project Manager, Guillaume Laforge. He told us the development story behind the language and why he thinks it is grooving its way into enterprises around the world.

And we :ref:`awk_ <awk_>` [22], who helped popularise C with his book, co-written with the creator Dennis Ritchie, *The C Programming Language*and contributed to the development of :ref:`awk <awk>` [4]and AMPL. After that it was :ref:`arduino <arduino>` [23]

But now its time for ColdFusion's Jeremy Allaire, who is also CEO of Brightcove and was the CTO at Macromedia.

 *Want to see a programming icon interviewed?Email Computerworldor follow `@computerworldau <http://www.twitter.com/computerworldau>`_ [24]on Twitter and let us know.*----



 **What prompted the development of ColdFusion?**

Back in 1994, I had started a web development consultancy, and was very focused on how the Web could be used for building interactive, community and media based online services.  I thought that the Web was an application platform and that you could build open and freely available online services using an open technology such as the Web. I had a lot of ideas for how to build online services, but I was not an engineer and found the existing technologies (Perl/CGI) to be really terrible and difficult. At the same time, my brother was becoming a more sophisticated software engineer, and also became interested in the Web, and he ended up designing the first version of ColdFusion based on the key requirements I had for an online service I was building.

 **Between you and your brother, J.J., who played what roles?**

We each played many different roles over the life-cycle of the company, but early on I guess you could say I was more of a "product manager", someone who was helping to shape the market vision and product requirements, and J.J. was the 'lead architect'.  Over time, I played a very significant role in both the shape of the product but also how we articulated our larger vision for the Web as an application platform.

 **Was there a particular problem you were trying to solve?**

Yes, I believed that you could build fully interactive applications through a browser, and that that would open up a wide range of new opportunities in media, communications and commerce. Initially, ColdFusion was built to make it easy to connect dynamic data to a web page, with both inputs and outputs, and in a way that would be easy to adopt for someone who was at least able to code HTML.


-----

**Where does the name ColdFusion come from?**

It's a long story, but basically, a good friend of ours was a creative type, and came up with the name, in part because it mapped to an existing logo that was built for an earlier name for ColdFusion (it was early on called Horizon and Prometheus, and even WebDB). The "prometheus" logo (hand and lightening bolt) worked well with the name. But we liked the brand - it represented the idea of "effortless power", which is how we wanted people to feel when using the product. It also sounds revolutionary, given the science concept. And it was about "fusing" the Web and data.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of ColdFusion?**

I think one of the most common and frustrating challenges we faced was the perception that ColdFusion was a 'toy' environment and programming language. Because we started with a pretty simple utility, and a simple language, and at first didn't target the most sophisticated enterprise applications, there was a "knock" on ColdFusion as not being scalable or a robust environment. I think it wasn't until ColdFusion 4.0 that we really shook that, and had a super robust server, really advanced features, and proved it was highly scalable.


 **Would you have done anything differently in the development of ColdFusion if you had the chance?**

I think we waited too long to embrace Java as a run-time platform for the ColdFusion environment. We had acquired JRun, and had planned to migrate to a J2EE-based architecture, but we delayed and it took longer than we had thought. I think that could have helped grow the momentum for ColdFusion during a critical time in the marketplace.

 **Are there many third party libraries?**

I haven't really kept track of where things stand now, but back in 2002, there was a massive range of 3rd party libraries and custom add-ons for ColdFusion, and a quick peak at the Adobe Developer's Exchange shows a still very impressive base of libraries available.

 **Has anyone re-implemented parts of the language into other frameworks?**

Sure, there are straight ports of CFML into other environments, like BlueDragon for ASP.NET, and of course most of the server-side scripting environments have adopted conventions that we invented in CFML.
 
-

----- **Why was the choice made to have HTML-like tags for the language, rather than something that looks visually different such as PHP, or ASP?**

We believed that a new breed of developer was emerging around the Web, and that they were first users of HTML, and that it was critical to have a language that fit within the architecture and syntax of the Web, which was tag-based. This had the inherent advantage of being human readable and human writable, with a more declarative style and syntax. This allowed CF to be the easiest to learn programming language for web applications. It's been really rewarding to see the ascendance of XML as a framework for languages and meta-data, it is really validation in the core idea of tag-based languages.

 **A lot of people seem to think that ColdFusion's days are over. How do you feel about that statement? Do many new projects still get created in ColdFusion?**

It's very far from the truth.  I happened to attend part of Adobe MAX this year, and learned from the leadership there that ColdFusion has had its strongest revenue and growth year since 2001-2002, and that for two straight years the ColdFusion developer community has grown. It's still the fastest and easiest way to build great web applications.I think one of the most common and frustrating challenges we faced was the perception that ColdFusion was a 'toy' environment and programming language.(Jeremy Allaire)

 **Allaire was acquired by Macromedia in 2001 - did you have any concerns about the deal?**

I was primarily responsible for this merger from the Allaire side of things, and was incredibly excited about it. We saw a really unique combined vision, bringing the world of content and design and media together with the world of applications, programming and developer tools. It was a great vision, and we executed on that vision, and it was tremendously successful.

 **How do you think being part of a bigger corporation helped the development?**

Well, as CTO of Macromedia I had to focus on a lot more than just ColdFusion, and in fact my primary focus was on the development of our broader integrated platform (Macromedia MX) and the evolution of Flash into being a real platform for applications on the Internet.
 
-

----- **You were also part of the Macromedia MX Flash development team - how easy was it to go from ColdFusion to that?**

My primary interest in the Internet, back in 1991-1994, was around its application in media and communications, and so the opportunity to work on a broader platform that could revolutionize our experience of media on the Internet was incredibly exciting.

 **In 2004 you founded Brightcove - how would you compare it to your background with Allaire and Macromedia?**

Brightcove has been tremendous, it has been a rocket-ship, we've grown incredibly fast and I'm having a fantastic time. We've grown at about the same rate as Allaire. This time, I'm a lot more seasoned and prepared for startup and growth mode, and have enjoyed being the company's CEO, leading the team and strategy and overall execution. It's also very different, in that Brightcove is both a development platform for video applications, and a suite of end-user and producer applications that are browser-based. It's allowed me to get closer to business issues in the media and marketing industries, which is a deep personal passion, and be less focused on pure infrastructure technology issues.

 **What's next for you and Brightcove?**

Well, we are in the middle of a major transformation of the Web. Video is becoming pervasive, and nearly every professional website is looking to publish video. So we see an enormous global opportunity to establish Brightcove as the dominant platform for professional video publishing online. That means making our technology accessible to all by making  it work really well in the environments that web designers and developers prefer. An OVP (Online Video Platform) is really a collection of web services and toolkits for developers to flexibly integrate video into their sites and applications. So a big focus, growing out of my historical passion and involvement with web development, is this next phase of Brightcove's maturation as a platform.

 **Do you have any advice for up-and-coming programmers?**

Focus on projects and ideas that you are passionate about; business, social, economic or organization problems that really excite and motivate you. I think it's the output that matters most, not the technology, and that's the best way to channel your skills and energy.

Oh yeah, also, learn how to program and integrate video into your sites and applications!  http://developer.brightcove.com

 **Finally, is there anything else you'd like to add?**

Nope.  Thanks for the opportunity to chat.

References

*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`falcon <falcon>` 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`tcl <tcl>` 

*  `http://groovy.codehaus.org/ <http://groovy.codehaus.org/>`_ 

*  :ref:`awk_ <awk_>` 

*  :ref:`arduino <arduino>` 

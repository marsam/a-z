.. _shakespeare:

The A-Z of Programming Languages: Shakespeare
=============================================
Jon Aslund and Karl Wiberg open up about the all-nighter they pulled to create their programming language, Shakespeare
* Chloe Herrick(Computerworld)

* 27 June, 2011 11:50

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_40752.jpg

 *Computerworld Australia*recently caught up with the Swedish creators of the `Shakespeare Programming Language <http://shakespearelang.sourceforge.net/>`_ [1]. Jon Aslund and Karl Wiberg, took the time to give us the low-down on what motivated them to create an esoteric programming language in the space of almost one night, the challenges they faced in its creation, and where they see programming languages heading in coming years.

Like other esoteric languages, such as Brainf**k (missing letters: c, u), Sorted!, Malbolge and :ref:`intercal <intercal>` [2], Shakespeare wasn’t created for mainstream use. However, both Aslund and Wiberg were excited with the popularity Shakespeare has experienced, especially as both were still at university when it was created.

The aim of the Shakespeare Programming Language (SPL) is to create a language with beautiful source code that resembles Shakespeare's plays — including a title, characters, acts and scenes, enter and exit directives, lines and so on.

[ *Computerworld Australia*is undertaking a series of investigations into some of the most widely used programming languages — as well as some of the more obscure ones. Previously, we’ve spoken to Brad Cox, the man behind :ref:`objective-c <objective-c>` [3], Python creator  , :ref:`awk <awk>` [4], Chet Ramey about his :ref:`bash <bash>` [5], :ref:`matlab <matlab>` [6], and :ref:`c++ <c++>` [7]. Be sure to check out all the interviews in our ` A to Z of programming languages index <http://www.computerworld.com.au/tag/a-z_of_programming_languages/>`_ [8].]

 **Can you tell me what motivated you to create a new programming language?**

Karl Wiberg (K): We took a syntax analysis course together at the Royal Institute of Technology in Stockholm, Sweden [kth.se]; for the fifth and final lab project of that course, there were a handful of options to choose from, one of which was to do whatever we liked to how show off what we'd learned. That sounded like fun. So while pulling an all-nighter to finish the fourth lab project (which wasn't as much fun), due to be demonstrated at 9am the following morning (even less fun), we wound up spending rather more time inventing the Shakespeare Programming Language (SPL) than meeting our deadline.

In other words, a genesis story not so different from that of other, successful programming languages. (Note: The comma between "other" and "successful" is intentional.)

 **When was it created and how long did the process take?**

K: In February 2001. Most of the language design was done that first night, but a lot of details (and the implementation) was hashed out over the following weeks. We finally made the 1.0 release on August 21, announcing it on the school and asking people to write and submit SPL programs. On August 31, SPL was featured on Slashdot.

Jon Aslund (J): I especially remember the days before the release, which I think was also the extended deadline we had been given for the final lab. That was when most of it was written and tested. We used all 24 hours of the day, coding at my place. I collapsed on my bed, Karl continued for a few hours, finished the grammar. I woke up, Karl went home to sleep and I started writing documentation and examples. I then went to his place, we had breakfast, polished the documentation, wrote more examples and continued like that until it was done. Fun times!

 **How did the name come about?**

K: Well, "SPL" (for "the Shakespeare Programming Language") seemed to be the obvious choice for a Shakespeare-based programming language.

Others have since started referring to it as "Shakespeare" instead, most likely because the language never got popular enough that the acronym became well known.

 **Do you remember any other names that you threw up there?**

I think "SPL" was pretty much our first and only candidate.

 **What made you settle on Shakespeare?**

We settled on a Shakespearean play style for the programs because it was a structured, reasonably well-defined form that no one would expect to be used for this purpose and one that would be easily recognisable to most people.

The name pretty much followed from that.

 **Did you feel there were holes in the already existing programming languages that you wanted to fill with Shakespeare?**

K: Not particularly, no. SPL belongs to a class of programming languages usually called "esoteric programming languages", these languages aren't meant to be seriously useful. One can think of them as clever toys, or sometimes parodies of existing languages — but an important point is that they are technically real programming languages, and in principle one could program anything in them — this is called Turing completeness — which is what computer scientists end up doing when attempting to have a good time.

SPL does seem to be less technical than many esoteric programming languages, in that one does not have to know that much about programming languages to appreciate it. The programs are, after all, written in the Bard's own plain English.

 **What challenges did you encounter in the process?**

K: Given our skills at the time, I'm glad to say there weren't many.

J: The course we took gave us the tools and tricks needed to write parsers, but after we had our nice syntax tree we didn't actually know how to run it, so we took the easy way out and made the compiler output c code, which you then had to compile to get something running.

-

-----

 **How have you contributed to Shakespeare since it was first written?**

K: Jon and I released a few bug-fix releases of our SPL compiler through the end of 2001, and then we were done. (If you aren't a programmer, it may be hard to appreciate just how impressive this is. Most programs aren't ever finished---just look at the endless stream of new versions of Microsoft Windows, for example, each new version released because the last one wasn't good enough. And Google is forever revising its search ranking algorithm.)

Others have built their own SPL compilers and interpreters, but we can't claim to have made significant contributions there without risking being accused of trying to take credit for other people's work, and we'd never do that.

 **What has the language been used for — has it found any rea-world applications?**

K: The thing I've seen that impressed me the most was an SPL program enacted by humans, a video of which featured in an introductory presentation at HOPL-III `ACM's third History of Programming Languages conference, San Diego 2007 <http://research.ihost.com/hopl/HOPL-III.html>`_ [9]by Guy Steele and Richard P. Gabriel.

But really, the answers are "not much" and "no"; being an esoteric language, it's basically just an interesting design experiment for people interested in programming languages. Its place isn't to be useful, but to encourage interest in programming language design, and to serve as a success story for others to try and emulate.

J: I remember that we got mail from Dr. David S. Touretzky who maintains a gallery of DeCSS ` implementations <http://www.cs.cmu.edu/~dst/DeCSS/Gallery>`_ [10]— software used to decrypt DVDs.

He asked us if we could write a Shakespeare Programming Language example that implemented DeCSS. A performance of that code would then be protected under free speech and could possibly be exported to other countries.

Being Turing complete, it would work, but it would be a very long and boring play. If our language had the concept of arrays and better loop constructs it could have been much simpler. We thought about adding it to a new version of SPL, but I think time got in the way and then we forgot about it.

 **Do you think that new modules and record formats will ever be added on to the language and how likely do you think this is to happen?**

K: Conceivably, someone playing with the language could add new features to their implementation, but I think that would classify as an improved derivative. At this point, the language spec is written in stone and can't be changed, kind of like how you can't change a published book — because people simply expect it to be static, so that even if you as the original author make a substantial revision, they'll keep using the old name to refer to the original, and call your new version something else.

 **So would you have done anything different in the development of Shakespeare if you had the chance?**

K: No, I don't think so.

 **Have many students learnt Shakespeare?**

K: I doubt it's in any curriculum, if that's what you mean. But I suspect students are well represented among those who've learnt Shakespeare.

 **Have you personally taught Shakespeare to students?**

K: No.

 **What are you proudest of in terms of the development of the language and its use?**

K: That people seem to appreciate our work!

-

-----

 **Where do you think computer languages will be heading in the next 5 – 20 years or so? Can you see any big trends forming?**

K: One possibility is that one or more SPL-inspired languages become dominant in the industry, radically changing the way software is written and lifting programmer improductivity (sic) to previously unthought-of levels. Expect a total economic collapse soon thereafter, and the end of US global cultural dominance as other countries ban English in self-defence.

Another possibility is a continuation of the current trend of mainstream languages slowly but surely adopting ideas that have been in Lisp for fifty years. Additionally, we keep adding layers of indirection between the programmer and the hardware; with web apps, the stack has grown impressively deep, and I can only assume it's going to grow deeper yet.

 **Given your experience, what advice do you have for students or up and coming programmers?**

K: It's a huge advantage to enjoy what you do for a living. Specifically, if you're going to do programming for a living, the fact that you enjoy it enough that inventing your own programming language still seems like fun after you're halfway through is a good sign.

 **Have you ever seen the language used in a way that wasn't originally intended?**

I don't think we ever envisioned that someone would make live performances of SPL programs.

 **Finally, where do you envisage Shakespeare's future lying?**

K: Let's just say I think he'll continue to be remembered as a playwright first, and as the inspiration for an esoteric programming language a close second.

 *Follow Chloe Herrick on Twitter: `@chloe_CW <http://twitter.com/chloe_CW>`_ [11]*

Follow Computerworld Australia on Twitter: `@ComputerworldAU <http://twitter.com/computerworldau>`_ [12]

References
*  `http://shakespearelang.sourceforge.net/ <http://shakespearelang.sourceforge.net/>`_ 

*  :ref:`intercal <intercal>` 

*  :ref:`objective-c <objective-c>` 

*  :ref:`awk <awk>` 

*  :ref:`bash <bash>` 

*  :ref:`matlab <matlab>` 

*  :ref:`c++ <c++>` 

*  `http://www.computerworld.com.au/tag/a-z_of_programming_languages/ <http://www.computerworld.com.au/tag/a-z_of_programming_languages/>`_ 

*  `http://research.ihost.com/hopl/HOPL-III.html <http://research.ihost.com/hopl/HOPL-III.html>`_ 

*  `http://www.cs.cmu.edu/~dst/DeCSS/Gallery <http://www.cs.cmu.edu/~dst/DeCSS/Gallery>`_ 

*  `http://twitter.com/chloe_CW <http://twitter.com/chloe_CW>`_ 

*  `http://twitter.com/computerworldau <http://twitter.com/computerworldau>`_ 

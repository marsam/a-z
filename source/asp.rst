.. _asp:

The A-Z of Programming Languages: ASP
=====================================
Microsoft on the development of ASP and the Web application framework ASP.NET
* Naomi Hamilton(Computerworld)

* 11 June, 2008 11:18
Computerworld is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to :ref:`awk <awk>` [1]of AWK fame, :ref:`bash <bash>` [2]about his experience maintaining Bash, and :ref:`ada <ada>` [3]on the Ada 1995 and 2005 revisions.

In this article we chat to Microsoft about its server-side script engine ASP and web application framework ASP.NET, used to build dynamic Web sites, applications and Web services.

 **Why was ASP created and what problem/s was it trying to solve?**

Active Server Pages (ASP) was initially created to address the challenge of building dynamic Web sites and Web-based business solutions.  It was first released with IIS 3.0 (Internet Information Server) in 1996.

Creating and updating static Web sites was a very time consuming task that was prone to human error.  In order to avoid mistakes, every page would require careful attention during changes.  Furthermore, the potential use of Web sites was very limited using HTML exclusively.  There needed to be an efficient way to change content quickly, in real time. ASP enabled the easy integration of databases as well as more advanced business and application logic that the Web is known for today.

 **Explain the early development of ASP.NET. Who was involved, and what difficult decisions had to be made?**

Scott Guthrie is one of the original creators of Microsoft's ASP.NET and, today, is the Corporate Vice President of the Microsoft Developer Division. The early development of ASP.NET focused on developer productivity and enabling powerful, Web-based solutions.  The key goal was to help make it easier for traditional developers who had never done Web development before to be successful in embracing this new development paradigm.

ASP.NET was a breakthrough technology that fundamentally changed the way developers approached and delivered Web sites - bringing it more in line with traditional software development.

Building a brand new Web application framework was a difficult decision to make, especially since many customers had already adopted ASP.  We felt it was the best approach, since it provided customers with one robust and consistent development platform to build software solutions.  A Web developer could now reuse his existing skill set to build desktop or mobile applications.

When we released ASP.NET, we did not want to force customers to upgrade.  As a result, we ensured that ASP would work in each subsequent release of IIS.  Today, we still continue to support the ASP runtime, which was included as part of IIS7 in Windows Server 2008.

 **What is the difference between ASP and ASP.NET and why would developers choose one over the other?**

ASP and ASP.NET are both server-side technologies and the similarities basically stop there.  If a developer is interested in writing less code, we would recommend ASP.NET. There are a myriad of other reasons too, including:


* Great tool support provided by the Visual Studio family and Expression Studio, which makes developers more productive and working with designers much easier
* ASP.NET AJAX integrated in the framework, which allows a better end-user experience
* Breadth of the .NET Framework, which provides a wealth of functionality to address both common scenarios and complex ones too




I would encourage a developer to visit `ASP.net <http://www.asp.net>`_ [4]to find out more.  A key thing to consider is that ASP.NET is the focus for Microsoft and we are not making any new investments in ASP.  I'd highly encourage anyone to use ASP.NET over ASP.

-

-----

 **Given a second chance, is there anything Microsoft could have done differently in the development of ASP.NET?**

ASP.NET was created to meet the needs of our customers building Web solutions.  As with any incubation or v1 product, the biggest change we would have made is to have more transparent and customer integrated product development - much like we have today.  Discussion with customers allows us to be better equipped to make decisions that affect them.  For example, ASP.NET MVC (Model-View-Controller) was a request from customers interested in test driven development.

The MVC design pattern is decades old, but the concept can still be applied to the design of today's Web applications. The product team released the first preview at the end of last year, which received a lot of positive feedback.  Developers interested in the release wanted more and couldn't wait to try the latest updates.  In March, the product team published the source code for ASP.NET MVC on Codeplex and decided to have interim, frequent releases.  This allows developers to access the latest bits and provide feedback that influences the first, official release.  The community can expect to see similar transparency with other features too.

 **Why was the decision made to target ASP.NET to IIS and Windows servers? Was this an architectural or business decision? Is it likely that we will ever see a free (possibly open source) official Apache module supporting ASP.NET?**

Microsoft is in the business of selling servers so our decision to focus on our products is obvious.  We believe that by doing so we can provide the most productive, scalable, secure, and reliable solution for delivering Web applications with deeply integrated features across the Web server, the database, the tools (Visual Studio), and the framework (.NET).  ASP.NET is part of the freely available .NET Framework today and we offer free tools like Visual Web Developer Express for anyone to easily get started.

 **What lasting legacy has ASP brought to the web?**

Never underestimate the value of getting the job done.  Even if there is a new Web application framework, we know that some customers are happy with what ASP already provides.  We recognize the choice to stay with ASP, and that is why we are continuing our support for the ASP runtime.  However, we do believe that continued investments in our new .NET based server platform will provide developers the best platform choice moving forward.

 *Read Computerworld's investigations into other widely used programming languages like The A-Z of Programming Languages: :ref:`awk <awk>` [1], :ref:`bash <bash>` [2]and :ref:`ada <ada>` [3].*

References
*  :ref:`awk <awk>` 

*  :ref:`bash <bash>` 

*  :ref:`ada <ada>` 

*  `http://www.asp.net <http://www.asp.net>`_ 

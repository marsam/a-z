.. _tcl:

The A-Z of Programming Languages: Tcl
=====================================
Our series on the most popular programming languages continues as we chat to Tcl creator John Ousterhout
* Kathryn Edwards(Computerworld)

* 08 May, 2009 13:22

.. image:: http://cdn.computerworld.com.au/dimg/415x415/dimg/m_img_16716.jpg
   :alt: Tcl creator John Ousterhout

Tcl creator John Ousterhout

 *Computerworld*is undertaking a series of investigations into the most widely-used programming languages. Previously we have spoken to Larry Wall, creator of the :ref:`perl <perl>` [1], Don Syme, senior researcher at Microsoft Research Cambridge, who developed :ref:`f# <f#>` [2], Simon Peyton-Jones on the development of :ref:`haskell <haskell>` [3], Alfred v. Aho of :ref:`awk <awk>` [4]fame, S. Tucker Taft on the :ref:`ada <ada>` [5], Microsoft about its server-side script engine :ref:`asp <asp>` [6], Chet Ramey about his experiences maintaining :ref:`bash <bash>` [7], Bjarne Stroustrup of :ref:`c++ <c++>` [8], and Charles H. Moore about the design and development of :ref:`forth <forth>` [9].

We’ve also had a chat with the irreverent Don Woods about the development and uses of :ref:`intercal <intercal>` [10], as well as Stephen C. Johnson on :ref:`yacc <yacc>` [11], Steve Bourne on `Bourne shell <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ [12], Luca Cardelli on :ref:`modula-3 <modula-3>` [13], Walter Bright :ref:`d <d>` [14], Brendan Eich on :ref:`javascript <javascript>` [15], Anders Hejlsberg on :ref:`c# <c#>` [16], Guido van Rossum on :ref:`python <python>` [17]and Prof. Roberto Ierusalimschy on :ref:`lua <lua>` [18]. We most recently spoke to :ref:`falcon <falcon>` [19]creator Giancarlo Niccolai.

In this interview `Tcl creator John Ousterhout <http://www.tcl.tk/>`_ [20], took some time to tell *Computerworld*about the extensibility of Tcl, its diverse eco-system and use in NASA's Mars Lander project.

 *If you wish to submit any suggestions for programming languages or language authors you would like to see covered, please emailkathryn@computerworld.com.au* **What prompted the creation of Tcl?**

In the early and mid-1980's my students and I created several interactive applications, such as editors and analysis tools for integrated circuits. In those days all interactive applications needed command-line interfaces, so we built a simple command language for each application.  Each application had a different command language, and they were all pretty weak (for example, they didn't support variables, looping, or macros). The idea for Tcl came to me as a solution to this problem: create a powerful command language, and implement it as a library package that can be incorporated into a variety of different applications to form the core of the applications' command languages.

 **Was there a particular problem the language aimed to solve?**

The original goal for Tcl was to make it easy to build applications with powerful command languages. At the time I didn't envision Tcl being used as a stand-alone programming language, though that is probably the way that most people have used it.

 **How does Tk fit into the picture?**

One of the key features of Tcl is extensibility: it is easy to create new features that appear as part of the language (this is the way that applications using Tcl can make their own functionality visible to users). At the same time that I was developing Tcl, graphical user interfaces were becoming popular, but the tools for creating GUI applications (such as the Motif toolkit for the X Window System) were complex, hard to use, and not very powerful. I had been thinking about graphical toolkits for several years, and it occurred to me that I could build a toolkit as an extension to Tcl.  This became Tk. The flexible, string-oriented nature of Tcl made it possible to build a toolkit that was simple to use yet very powerful.

 **What influence, if any, did Tcl have in the development of Java?**

As far as I know the Java language developed independently of Tcl. However, the AWT GUI toolkit for Java reflects a few features that appeared first in Tk, such as a grid-based geometry manager.
-

----- **What’s the Tcl eco-system like?**

The Tcl ecosystem is so diverse that it's hard to characterize it, but it divides roughly into two camps. On the one hand are the Tk enthusiasts who believe that the Tcl/Tk's main contribution is its powerful cross-platform GUI tools; they think of Tcl/Tk as a stand-alone programming platform, and are constantly pushing for more Tk features. On the other hand are the Tcl purists who believe the most unique thing about Tcl is that it can be embedded into applications. This group is most interested in the simplicity and power of the APIs for embedding. The Tcl purists worry that the Tk enthusiasts will bloat the system to the point where it will no longer be embeddable.

 **What is Tcl’s relevance in the web application world?**

One of my few disappointments in the development of Tcl is that it never became a major factor in Web application development. Other scripting languages, such as Javascript and Python, have played a much larger role than Tcl.

 **What was the flagship application made with Tcl?**

Tcl's strength has been the breadth of activities that it covers, rather than a single flagship application.  Most Tcl applications are probably small ones used by a single person or group.  At the same time, there are many large applications that have been built with Tcl, including the NBC broadcast control system, numerous applications in the Electronic Design Automation space, test harnesses for network routers and switches, Mars lander software, and the control system for oil platforms in the Gulf of Mexico.

Unfortunately I don't know very much about those projects, and the information I have is pretty old (I heard about both of those projects in the late 1990s). For the oil platform project, I believe that Tcl/Tk provided the central management system for observing the overall operation of the platform and controlling its functions. In the case of the Mars lander, I believe Tcl was used for pre-launch testing of the system hardware and software.

 **Have you ever seen the language used in a way that wasn't originally intended?**

The most surprising thing to me was that people built large programs with Tcl. I designed the language as a command-line tool and expected that it would be used only for very short programs: perhaps a few dozen lines at most.  When I went to the first Tcl workshop and heard that a multi-billion-dollar oil platform was being controlled by a half million lines of Tcl code I almost fell over.

 **Were there any particularly difficult or frustrating problems you had to overcome in the development of the language?**

One problem we worked on for many years was making Tcl and Tk run on platforms other than Unix. This was eventually successful, but the differences between Unix, Windows, and the Macintosh were large enough that it took a long time to get it all right. A second problem was language speed. Originally Tcl was completely interpreted: every command was reparsed from a string every time it was executed. Of course, this was fairly inefficient. Eventually, Brian Lewis created a bytecode compiler for Tcl that provided 5-10x speedups.

 **Can you attribute any of Tcl’s popularity to the Tk framework?**

Absolutely. As I mentioned earlier, there are many people who use Tcl exclusively for Tk.

 **Generally, more and more and more coding is moving to scripting languages. What do you think about this trend given Tcl’s long scripting language heritage? Has Tcl gained from this trend?**

I think this trend makes perfect sense. Scripting languages make it substantially easier to build and maintain certain classes of applications, such as those that do a lot of string processing and those that must integrate a variety of different components and services. For example, most Web applications are built with scripting languages these days.

 **Looking back, is there anything you would change in the language’s development?**

Yes, two things. First, I wish I had known that people would write large programs in Tcl; if I had, I'm sure I would have done some things differently in the design of the language. Second, I wish I had included object-oriented programming facilities in the language. I resisted this for a long time, and in retrospect I was wrong. It would have been easy to incorporate nice object-oriented facilities in Tcl if I had done it early on. Right now there are several Tcl extensions that provide OO facilities but there is not one "standard" that is part of Tcl; this is a weakness relative to other scripting languages.

 **Where do you envisage Tcl’s future lying?**

Tcl is more than 20 years old now (hard to believe!) so it is pretty mature; I don't expect to see any shocking new developments around Tcl or Tk. I'm sure that Tcl and Tk will continue to be used for a variety of applications.

References
*  :ref:`perl <perl>` 

*  :ref:`f# <f#>` 

*  :ref:`haskell <haskell>` 

*  :ref:`awk <awk>` 

*  :ref:`ada <ada>` 

*  :ref:`asp <asp>` 

*  :ref:`bash <bash>` 

*  :ref:`c++ <c++>` 

*  :ref:`forth <forth>` 

*  :ref:`intercal <intercal>` 

*  :ref:`yacc <yacc>` 

*  `http://www.computerworld.com.auhttp://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/ <http://www.techworld.com.au/article/279011/a-z_programming_languages_bourne_shell_sh/>`_ 

*  :ref:`modula-3 <modula-3>` 

*  :ref:`d <d>` 

*  :ref:`javascript <javascript>` 

*  :ref:`c# <c#>` 

*  :ref:`python <python>` 

*  :ref:`lua <lua>` 

*  :ref:`falcon <falcon>` 

*  `http://www.tcl.tk/ <http://www.tcl.tk/>`_ 
